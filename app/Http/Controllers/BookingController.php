<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomerAccountExists;
use App\Helpers\Property;
use App\Http\Requests;
use FullyBooked\Client\Availability\Payload\ListAvailabilityPayload;
use FullyBooked\Client\Booking\Payload\CreateBookingPayload;
use FullyBooked\Client\Booking\Payload\GetBookingByRefPayload;
use FullyBooked\Client\Booking\Payload\GetBookingPayload;
use FullyBooked\Client\BookingGuest\Payload\CreateBookingGuestPayload;
use FullyBooked\Client\BookingLine\Payload\CreateBookingLinePayload;
use FullyBooked\Client\BookingPayment\Payload\CreateBookingPaymentPayload;
use FullyBooked\Client\Country\Payload\GetCountryPayload;
use FullyBooked\Client\Country\Payload\ListCountryPayload;
use FullyBooked\Client\Country\Response\CountryResponse;
use FullyBooked\Client\Customer\Payload\CreateCustomerPayload;
use FullyBooked\Client\Customer\Payload\GetCustomerByEmailPayload;
use FullyBooked\Client\Customer\Payload\GetCustomerPayload;
use FullyBooked\Client\Customer\Payload\UpdateCustomerPayload;
use FullyBooked\Client\Customer\Response\CustomerResponse;
use FullyBooked\Client\CustomValue\Payload\CreateCustomValuePayload;
use FullyBooked\Client\HeardAbout\Payload\ListHeardAboutPayload;
use FullyBooked\Client\OptionalExtra\Payload\ListOptionalExtraPayload;
use FullyBooked\Client\Property\Payload\GetPropertyBySlugPayload;
use FullyBooked\Client\Property\Payload\GetPropertyPayload;
use FullyBooked\Client\Property\Response\PropertyResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{


  public function getForm($slug, Request $request)
  {
    try
    {
      $dateFrom     = new \DateTime($request->get('date'));
      $dateInterval = new \DateInterval('P' . $request->get('nights', 3) . 'D');
      $dateTo       = clone $dateFrom;
      $dateTo->add($dateInterval);
    }
    catch(\Exception $e)
    {
      return redirect()->route('van.show', $slug);
    }

    $van = $this->api()->propertyService()
      ->getPropertyBySlug(
        (new GetPropertyBySlugPayload())->setSlug($slug)
      );
    /** @var PropertyResponse $van */
    $van = Property::createFromResponse($van);

    $optionalExtras = $this->api()->optionalExtraService()
      ->listOptionalExtras(
        (new ListOptionalExtraPayload())->setTag('extra')->setLimit(100)
      );
    $optionalExtras = $optionalExtras->items;

    $insuranceOptions = $this->_getInsurance($request->get('nights'));

    $heardAbouts = $this->api()->heardAboutService()
      ->listHeardAbouts(
        (new ListHeardAboutPayload())
      );
    $heardAbouts = $heardAbouts->items;

    $countries         = [];
    $countriesResponse = $this->api()->countryService()
      ->listCountries(
        (new ListCountryPayload())
      );
    if(!empty($countriesResponse->items))
    {
      foreach($countriesResponse->items as $country)
      {
        $countries[$country->uuid] = $country->name;
      }
    }

    $availability = $this->api()->availabilityService()
      ->listAvailability(
        (new ListAvailabilityPayload())->setPropertyUuid($van->uuid)->setExact(
          1
        )->setDateFrom($dateFrom->format('Y-m-d'))->setDateTo(
          $dateTo->format('Y-m-d')
        )
      );

    if(empty($availability->items))
    {
      return redirect()->route('availability.calendar');
    }
    if(empty($availability->items[$van->uuid]->items))
    {
      return redirect()->route('availability.calendar');
    }

    $price = $availability->items[$van->uuid]->items[0]->price;

    $today    = new \DateTime();
    $daysAway = $today->diff($dateFrom)->format('%a');
    if($daysAway <= 42)
    {
      $depositAllowed = false;
    }
    else
    {
      $depositAllowed = true;
    }

    if(Session::has('customerUuid'))
    {
      $customer = $this->api()->customerService()
        ->getCustomer(
          (new GetCustomerPayload)->setUuid(Session::get('customerUuid'))
        );
    }
    else
    {
      $customer = new CustomerResponse;

      $customer->country       = new CountryResponse;
      $customer->country->uuid = '57ab35d9-3682-43bb-8d1b-89ed1e729eb7';
    }

    return view(
      'booking',
      compact(
        'van',
        'optionalExtras',
        'insuranceOptions',
        'heardAbouts',
        'countries',
        'price',
        'depositAllowed',
        'customer'
      )
    );
  }

  protected function _getInsurance($nights)
  {
    $insuranceOptions = $this->api()
      ->optionalExtraService()->listOptionalExtras(
        (new ListOptionalExtraPayload)
          ->setTag('insurance')->setLimit(100)
      );
    $insuranceOptions = $insuranceOptions->items;
    if(!empty($insuranceOptions))
    {
      foreach($insuranceOptions as &$insuranceOption)
      {
        if($insuranceOption->frequency == 'per_day')
        {
          $insuranceOption->amount = number_format($insuranceOption->amount * $nights, 2, '.', '');
        }
      }
    }

    return $insuranceOptions;
  }

  public function postForm(Request $request)
  {

    $rules = [
      'firstName'       => 'required',
      'lastName'        => 'required',
      'email'           => 'required|email',
      'billingPhone'    => 'required',
      'billingAddress1' => 'required',
      'billingCity'     => 'required',
      'billingState'    => 'required',
      'billingPostcode' => 'required',
      'terms'           => 'required',
      'payment_method'  => 'required',
    ];

    if(!Session::has('customerUuid'))
    {
      $rules['account_password'] = 'required|min:8';
    }

    // Validate customer data
    $this->validate($request, $rules);

    // Calculate data
    $dateFrom     = new \DateTime($request->get('date'));
    $dateInterval = new \DateInterval('P' . $request->get('nights') . 'D');
    $dateTo       = clone $dateFrom;
    $dateTo->add($dateInterval);

    $availability = $this->api()->availabilityService()
      ->listAvailability(
        (new ListAvailabilityPayload())->setPropertyUuid(
          $request->get('property_uuid')
        )->setExact(1)->setDateFrom($dateFrom->format('Y-m-d'))->setDateTo(
          $dateTo->format('Y-m-d')
        )
      );

    $propertyAvailability = $availability->items[$request->get(
      'property_uuid'
    )];
    if(count($propertyAvailability->items) == 0)
    {
      return redirect()->route('availability.vans');
    }
    $price = $propertyAvailability->items[0]->price;

    // Create user account
    /*if ($request->has('account_email')) {
        $data = [
            'email' => $request->get('account_email'),
            'password' => Hash::make($request->get('account_password')),
        ];
        Auth::login(User::create($data));
    }*/

    if(Session::has('customerUuid'))
    {
      $customer = $this->api()->customerService()
        ->getCustomer(
          (new GetCustomerPayload)->setUuid(Session::get('customerUuid'))
        );

      $customerPayload = (new UpdateCustomerPayload)
        ->setUuid($customer->uuid)
        ->setTitle($request->get('title'))
        ->setForename($request->get('firstName'))
        ->setSurname($request->get('lastName'))
        ->setAddress1($request->get('billingAddress1'))
        ->setAddress2($request->get('billingAddress2'))
        ->setTown($request->get('billingCity'))
        ->setCounty($request->get('billingState'))
        ->setPostcode($request->get('billingPostcode'))
        ->setCountryUuid($request->get('country'))
        ->setTelMob($request->get('billingPhone'))
        ->setEmail($request->get('email'));
      $this->api()->customerService()->updateCustomer($customerPayload);

      $customerUuid = $customer->uuid;
    }
    else
    {
      try
      {
        $customerUuid = $this->createCustomer($request);
        // Login customer
        $request->session()->put('customerUuid', $customerUuid);
      }
      catch(CustomerAccountExists $e)
      {
        $property = $this->api()->propertyService()->getProperty(
          (new GetPropertyPayload)
            ->setUuid($request->get('property_uuid'))
            ->setStub(true)
        );

        return redirect()->route(
          'booking',
          [
            'slug'   => $property->slug,
            'date'   => $request->get('date', ''),
            'nights' => $request->get('nights')
          ]
        )->withInput()
          ->withErrors('An account with that email already exists.');
      }
    }

    // Create booking with FullyBooked
    $bookingUuid = $this->api()->bookingService()
      ->createBooking(
        (new CreateBookingPayload)
          ->setCustomerUuid($customerUuid)
          ->setProvisional(true)
          ->setPropertyUuid($request->get('property_uuid'))
          ->setDateFrom($dateFrom->format('Y-m-d'))
          ->setDateTo($dateTo->format('Y-m-d'))
          ->setNumAdults($request->get('adults'))
          ->setNumChildren($request->get('children'))
          ->setPrice($price)
          ->setArrivalTime('14:30')
          ->setDepartureTime('10:00')
          ->setHeardAboutUuid($request->get('referrer'))
          ->setHeardAboutCustom($request->get('referrer_specific'))
      );

    $booking = $this->api()->bookingService()->getBooking(
      (new GetBookingPayload)->setUuid($bookingUuid->message)->setStub(true)
    );

    $totalAmount = $price;

    // Create optional extras as booking lines with FullyBooked
    if($request->has('optional_extras'))
    {
      $i = 1;
      foreach($request->get('optional_extras') as $uuid => $data)
      {
        if(isset($data['qty']))
        {
          if($data['qty'] < 1)
          {
            continue;
          }
          $this->api()->bookingLineService()
            ->createBookingLine(
              (new CreateBookingLinePayload())
                ->setBookingUuid($bookingUuid)
                ->setItem($data['name'])
                ->setOptionalExtraUuid($uuid)
                ->setPricePerItem($data['price'])
                ->setQuantity($data['qty'])
                ->setWeight($i)
            );
          // Increment payment amount
          $totalAmount += ($data['price'] * $data['qty']);
          $i++;
        }
      }
    }

    // Create booking guests with FullyBooked
    $guestCustomValues = Config('fullybooked.customValues.guest');

    $bookingGuestUuid = $this->api()->bookingGuestService()
      ->createBookingGuest(
        (new CreateBookingGuestPayload())
          ->setBookingUuid($bookingUuid)
          ->setType('adult')
          ->setName(
            $request->get('firstName') . ' ' . $request->get('lastName')
          )
      );
    if(!empty($guestCustomValues))
    {
      foreach($guestCustomValues as $guestCustomValue)
      {
        $this->api()->customValueService()
          ->createCustomValue(
            (new CreateCustomValuePayload())
              ->setRemoteType('guest')
              ->setRemoteUuid($bookingGuestUuid)
              ->setFieldType('text')
              ->setKey($guestCustomValue)
          );
      }
    }

    if($request->get('adults') > 1)
    {
      for($i = 2; $i <= $request->get('adults'); $i++)
      {
        $bookingGuestUuid = $this->api()->bookingGuestService()
          ->createBookingGuest(
            (new CreateBookingGuestPayload())
              ->setBookingUuid($bookingUuid)
              ->setType('adult')
          );
        if(!empty($guestCustomValues))
        {
          foreach($guestCustomValues as $guestCustomValue)
          {
            $this->api()->customValueService()
              ->createCustomValue(
                (new CreateCustomValuePayload())
                  ->setRemoteType('guest')
                  ->setRemoteUuid($bookingGuestUuid)
                  ->setFieldType('text')
                  ->setKey($guestCustomValue)
              );
          }
        }
      }
    }

    if($request->get('children') > 0)
    {
      for($i = 1; $i <= $request->get('children'); $i++)
      {
        $bookingGuestUuid = $this->api()->bookingGuestService()
          ->createBookingGuest(
            (new CreateBookingGuestPayload())
              ->setBookingUuid($bookingUuid)
              ->setType('child')
              ->setAge($request->get('child_age_' . $i))
          );
        if(!empty($guestCustomValues))
        {
          foreach($guestCustomValues as $guestCustomValue)
          {
            $this->api()->customValueService()
              ->createCustomValue(
                (new CreateCustomValuePayload())
                  ->setRemoteType('guest')
                  ->setRemoteUuid($bookingGuestUuid)
                  ->setFieldType('text')
                  ->setKey($guestCustomValue)
              );
          }
        }
      }
    }

    if($request->get('payment_method') == 'card')
    {
      $bookingDetails = $request->all();

      $country = $this->api()->countryService()->getCountry(
        (new GetCountryPayload)->setUuid($request->get('country'))
      );

      $bookingDetails['country'] = $country->code2Letter;

      // Send details to payment gateway
      $card        = \Omnipay::CreditCard($request->all());
      $totalAmount = number_format($totalAmount, 2, '.', '');

      if($request->get('payment_amount') == 'deposit')
      {
        $totalAmount = 150.00;
      }

      $params = [
        'card'          => $card,
        'transactionId' => $booking->ref,
        'amount'        => $totalAmount,
        'currency'      => 'GBP',
        'description'   => 'Online Payment',
        'returnUrl'     => action(
          'BookingController@getConfirmation',
          ['ref' => $bookingUuid->message]
        ),
        'cancelUrl'     => $request->get('cancel_url'),
      ];

      $testIps = 
      [
         'Dave Home' => '2a02:c7d:7b01:bb00:ad0e:924a:c8f5:de1b',
	 'Pulse8' => '92.236.60.250',
	 'Pulse8New' => '92.236.60.154'
      ];
      if ( in_array($_SERVER['REMOTE_ADDR'], $testIps) ) {
	$params['testMode'] = 100;
      }

      $response = \Omnipay::purchase($params)->send();

      if($response->isSuccessful())
      {
        // payment was successful: update database
        print_r($response);
      }
      elseif($response->isRedirect())
      {
        // redirect to offsite payment gateway
        return $response->getRedirectResponse();
      }
      else
      {
        // payment failed: display message to customer
        echo $response->getMessage();
      }
    }
    else
    {
      return redirect()->route(
        'booking.confirmation',
        ['ref' => $bookingUuid->message]
      );
    }
  }

  /**
   * @param Request $request
   *
   * @return string
   */
  protected function createCustomer(Request $request)
  {

    $existsPayload = (new GetCustomerByEmailPayload)
      ->setEmail($request->get('email'));

    $accountExists = $this->api()->customerService()
      ->accountExists($existsPayload);

    if($accountExists->result)
    {
      throw new CustomerAccountExists;
    }

    $customerPayload = (new CreateCustomerPayload())
      ->setTitle($request->get('title'))
      ->setForename($request->get('firstName'))
      ->setSurname($request->get('lastName'))
      ->setAddress1($request->get('billingAddress1'))
      ->setAddress2($request->get('billingAddress2'))
      ->setTown($request->get('billingCity'))
      ->setCounty($request->get('billingState'))
      ->setPostcode($request->get('billingPostcode'))
      ->setCountryUuid($request->get('country'))
      ->setTelMob($request->get('billingPhone'))
      ->setEmail($request->get('email'))
      ->setPassword($request->get('account_password'));

    if($request->get('newsletter'))
    {
      $customerPayload
        ->setCommunicationsEmail(true)
        ->setCommunicationsPhone(true)
        ->setCommunicationsSms(true)
        ->setCommunicationsNewsletter(true)
        ->setCommunicationsOffers(true)
        ->setCommunicationsNewsletter(true);
    }

    // Create customer with FullyBooked
    $customerUuid = $this->api()->customerService()
      ->createCustomer($customerPayload);

    return $customerUuid->message;
  }

  public function postCallback(Request $request)
  {
    if($request->has('transStatus'))
    {
      if($request->get('transStatus') == 'Y')
      {

        $callback = $request->get('MC_callback', '');
        $update   = substr($callback, -8) == 'update=1';

        try
        {
          $booking =
            $this->api()->bookingService()
              ->getBookingByRef(
                (new GetBookingByRefPayload)
                  ->setBookingRef($request->get('cartId'))
                  ->setIncludeProvisional(true)
              );

          if(!$update)
          {
            $this->api()->bookingService()->convertBookingByRef(
              (new GetBookingByRefPayload)
                ->setBookingRef($booking->ref)
            );
          }

          // Create booking payment with FullyBooked
          $this->api()->bookingPaymentService()
            ->createBookingPayment(
              (new CreateBookingPaymentPayload())
                ->setBookingUuid($booking->uuid)
                ->setAmount($request->get('authAmount'))
                ->setPaymentSource('WorldPay')
                ->setPaymentType('Deposit')
                ->setTransactionReference($request->get('transId'))
                ->setNotes('')
            );
        }
        catch(\Exception $e)
        {

          $subject = 'DEBUG';
          $message = $e->getMessage() . "\n\n\n" . print_r($_REQUEST, true) . print_r($_SERVER, true);
          mail('dblake@pulse8.co.uk', $subject, $message, "From: noreply@fullybooked.com");
          error_log($message);
          exit;
        }

        if($update)
        {
          return '<meta http-equiv="refresh" content="0;' . route(
            'account.booking.payment.confirm',
            ['ref' => $booking->ref]
          ) . '" />';
        }
        else
        {
          return '<meta http-equiv="refresh" content="0;' . route(
            'booking.confirmation'
          ) . '?ref=' . $booking->ref . '&amp;worldpay=1" />';
        }
      }
    }
  }

  public function getConfirmation(Request $request)
  {
    $hideBacsWarning = $request->has('worldpay');
    if($request->has('ref'))
    {
      $bookingRef = $request->get('ref');

      $booking = $this->api()->bookingService()
        ->getBookingByRef(
          (new GetBookingByRefPayload)
            ->setBookingRef($bookingRef)
        );

      $booking->property = Property::createFromResponseFromStdObject($booking->property);

      return view(
        'booking-confirmation',
        compact('booking', 'hideBacsWarning')
      );
    }

    return redirect()->action('MainController@getHome');
  }

}
