@extends('admin._layout')

@section('content')

<h1 class="page-header">Travel Guides</h1>

@include('flash::message')

<p>
    <a class="btn btn-success" href="{{ URL::route('admin.guide.create') }}"><span class="glyphicon glyphicon-plus"></span> Create Guide</a>
</p>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="100">Published?</th>
            <th>Title</th>
            <th width="30%">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($guides as $guide)
        <tr id="{{ $guide->id }}">
            <td>{{ $guide->published ? 'Yes' : 'No' }}</td>
            <td>
                {{ $guide->title }}<br>
                <small>{{ $guide->location }}</small>
            </td>
            <td>
                <a class="btn btn-small btn-default" href="{{ URL::route('preview.guide.show', array( $guide->slug )) }}" target="_blank"><span class="glyphicon glyphicon-new-window"></span> Preview</a>
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.guide.edit', array( $guide->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Guide</a>
                {!! Form::open(array('route' => array('admin.guide.destroy', $guide->id), 'method' => 'DELETE', 'class' => 'delete-form')) !!}
                    <button type="submit" class="btn btn-small btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@stop