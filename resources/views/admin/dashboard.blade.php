@extends('admin._layout')

@section('content')

<h1 class="page-header">Admin Dashboard</h1>

<div class="row">

    <div class="col-md-6">

        <p>Welcome to the O'Connors Campers admin area.</p>

    </div>

    <div class="col-md-6">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>Recent Enquiries <span class="glyphicon glyphicon-envelope pull-right"></span></h3>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Sent</th>
                    <th>From</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($enquiries as $enquiry)
                    <tr>
                        <td>{{ $enquiry->created_at->diffForHumans() }}</td>
                        <td>
                            <strong>{{ $enquiry->title }} {{ $enquiry->first_name }} {{ $enquiry->last_name }}</strong><br>
                            {{ $enquiry->email }}
                        </td>
                        <td>
                            <a class="btn btn-small btn-info btn-block" href="{{ route('admin.enquiry.show', [ $enquiry->id ]) }}"><span class="glyphicon glyphicon-pencil"></span> Show</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="panel-footer">
                <small>Showing five latest enquiries.</small>
            </div>
        </div>

    </div>

</div>

@stop