@extends('_layout')

@section('content')

<div class="container text-center">

    <div class="row">

        <div class="col-md-offset-3 col-md-6 spacer">

            {!! Form::open(array( 'route'=>'login', 'method'=>'post' )) !!}

            <h3>Request a password reset</h3>

            @include('layout.partials.errors')

            <div class="row">
                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            </div>

            <br><button class="btn btn-lg btn-primary" type="submit">Send Password Reset Link</button>

            {!! Form::close() !!}

        </div>

    </div>

</div>

@endsection