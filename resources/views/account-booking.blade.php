@extends('_layout')

@section('content')

  <div class="container">

    <h2 class="text-center heading">Your Booking / {{ $booking->ref }}</h2>

    <h4 align="center">To order extras and add driver information select 'Add Driver Details and Choose Extras' below.</h4>
    <h4 align="center">Balance / Stage payments can be made at the bottom of this screen</h4><br/>

    @include('flash::message')

    <div class="row">

      <div class="col-md-6">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Booking Details</h4>
          </div>
          <div class="panel-body">
            @if($booking->canEdit)
              <a class="btn btn-sm btn-warning pull-right"
                 href="{{ route('account.booking.edit', $booking->ref) }}">Add Driver Details and Choose Extras</a>
            @endif
            <p>
              <strong>{{ $booking->ref }}</strong><br>
              {{ $booking->property->name }}<br>
              {{ date('l jS F Y', strtotime($booking->dateFrom)) }}
              - {{ date('l jS F Y', strtotime($booking->dateTo)) }}
            </p>
            <p>
              {{ $booking->numAdults }} Adults<br>
              {{ $booking->numChildren }} Children<br>
            </p>
            @if(!empty($booking->guests))
              <p><strong>Guests</strong><br>
                @foreach($booking->guests as $guest)
                  {{ $guest->name }} ({{ucwords($guest->type)}} @if($guest->age){{ ' - ' . $guest->age }}@endif)
                  <br>
                @endforeach
              </p>
            @endif
            @if(!empty($optionalExtras))
              <strong>Optional Extras</strong>
              <table class="table table-striped table-hover">
                <thead>
                <tr>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($optionalExtras as $optionalExtra)
                  <tr>
                    <td>{{ $optionalExtra->item }}</td>
                    <td>{{ $optionalExtra->pricePerItem }}</td>
                    <td>{{ $optionalExtra->quantity }}</td>
                    <td>&pound;{{ $optionalExtra->priceTotal }}</td>
                  </tr>
                @endforeach
                </tbody>

                <tfoot>
                <tr>
                  <th colspan="2"></th>
                  <th>Total</th>
                  <th>&pound;{{$optionalExtrasTotal}}</th>
                </tr>
                </tfoot>
              </table>
            @endif
            @if(!empty($insuranceLoadings))
              <strong>Insurance Loadings</strong>
              <table class="table table-striped table-hover">
                <thead>
                <tr>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($insuranceLoadings as $optionalExtra)
                  <tr>
                    <td>{{ $optionalExtra->item }}</td>
                    <td>{{ $optionalExtra->pricePerItem }}</td>
                    <td>{{ $optionalExtra->quantity }}</td>
                    <td>&pound;{{ $optionalExtra->priceTotal }}</td>
                  </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th colspan="2"></th>
                  <th>Total</th>
                  <th>&pound;{{$insuranceLoadingsTotal}}</th>
                </tr>
                </tfoot>
              </table>
            @endif
          </div>
        </div>

      </div>

      <div class="col-md-6">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Booking Status <span
                  class="label label-info pull-right">{{ $booking->status }}</span>
            </h4>
          </div>
          <div class="panel-body">
            <p>
              Base Price: &pound;{{ $booking->basePrice }}<br>
              Extras Price: &pound;{{ $booking->extrasPrice }}<br>
              <strong>Total Price: &pound;{{ $booking->totalPrice }}</strong>
            </p>
            <p>
              Total Paid: &pound;{{ $booking->totalPaid }}<br>
              <strong>Remaining Balance: &pound;{{ $booking->balance }}</strong>
            </p>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Payment Details</h4>
          </div>
          <div class="panel-body">
            @if(empty($booking->payments))
              <p><em>No payments found</em></p>
            @else
              <table class="table table-striped table-hover">
                <thead>
                <tr>
                  <th>Amount</th>
                  <th>Source</th>
                  <th>Reference</th>
                </tr>
                </thead>
                <tbody>
                @foreach($booking->payments as $payment)
                  <tr>
                    <td>&pound;{{ $payment->amount }}</td>
                    <td>{{ $payment->paymentSource }}</td>
                    <td>{{ $payment->transactionReference }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            @endif
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Make Payment</h4>
          </div>
          <div class="panel-body">

            @if($booking->balance>0)
              <form
                  action="{{ route('account.booking.payment', $booking->ref) }}"
                  method="post">

                <div class="form-group">
                  <label for="amount">Amount</label>
                  <div class="input-group">

                    <div class="input-group-addon">&pound;</div>
                    <input class="form-control" id="amount" name="amount"
                           type="text"
                           value="{{$booking->balance}}">

                  </div>
                </div>

                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Add Payment
                </button>

              </form>
            @else
              <p>Your balance is paid, thank you.</p>
            @endif
          </div>
        </div>

      </div>

    </div>

  </div>

  <div class="container text-center">

    <a href="{{ route('account') }}" class="btn btn-lg btn-primary spacer">Back
                                                                           to My
                                                                           Account</a>

  </div>

@endsection