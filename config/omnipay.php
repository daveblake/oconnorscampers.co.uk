<?php

return array(

  /** The default gateway name */
  'gateway'  => 'WorldPay',

  /** The default settings, applied to all gateways */
  'defaults' => array(
    'testMode' =>
      (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '92.236.60.250')
      || env('PAYMENT_TEST', false),
  ),

  /** Gateway specific parameters */
  'gateways' => array(
    'WorldPay' => array(
      'installationId'   => '1078012',
      'accountId'        => '',
      'secretWord'       => 'occ033p8!$QWasZXerDF',
      'callbackPassword' => 'occ033p8!response',
      'testMode'         => env('PAYMENT_TEST', false),
    ),
  ),

);
