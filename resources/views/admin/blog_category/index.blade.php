@extends('admin._layout')

@section('content')

<h1 class="page-header">Blog Categories</h1>

@if (Session::has('message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('message') }}</div>
@endif

<p>
    <a class="btn btn-success" href="{{ URL::route('admin.blog_category.create') }}"><span class="glyphicon glyphicon-plus"></span> Create Blog Category</a>
    <a class="btn btn-info" href="{{ URL::route('admin.blog_article.index') }}"><span class="glyphicon glyphicon-pencil"></span> Manage Articles</a>
</p>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th><span class="glyphicon glyphicon-resize-vertical"></span></th>
            <th>Name</th>
            <th width="30%">Actions</th>
        </tr>
    </thead>
    <tbody class="order-sortable" data-model="blog_category">
    @foreach($blogCategories as $blogCategory)
        <tr id="{{ $blogCategory->id }}">
            <td width="20px" class="order-handle"><span class="glyphicon glyphicon-resize-vertical"></span></td>
            <td>{{ $blogCategory->name }}</td>
            <td>
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.blog_category.edit', array( $blogCategory->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Category</a>
                {!! Form::open(array('route' => array('admin.blog_category.destroy', $blogCategory->id), 'method' => 'DELETE', 'class' => 'delete-form')) !!}
                    <button type="submit" class="btn btn-small btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@stop