@extends('_layout')

@section('seo')

<meta name="description" content="{{ $guide->seo_description }}">
<title>{{ $guide->seo_title }}</title>

<meta name="twitter:title" content="{{ $guide->seo_title }}">
<meta name="twitter:description" content="{{ $guide->seo_description }}">
<meta name="twitter:image" content="{{ asset($guide->image('small')) }}">

<meta property="og:title" content="{{ $guide->seo_title }}" />
<meta property="og:description" content="{{ $guide->seo_description }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:image" content="{{ asset($guide->image('small')) }}" />

@endsection

@section('content')

<div class="banner ltr top" style="background-image:url('{{ $guide->image('large') }}');">
    <div class="container text-center wow fadeInUp">
        <h2>{{ $guide->title }}</h2>
        <h4>{{ $guide->location }}</h4>
    </div>
</div>

<div class="container text-center">

    <div class="row">

        <div class="col-md-push-2 col-md-8">

            {!! $guide->content !!}

        </div>

    </div>

</div>

@if($guide->guideSections)
<?php $i = 1; ?>
@foreach($guide->guideSections as $guideSection)
    <div class="guide-section {{ ($i%2 != 0) ? 'ltr colored' : '' }}">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-push-2 col-md-8">
                    <h2>{{ $guideSection->heading }}</h2>
                    <h4>{{ $guideSection->subheading }}</h4>

                    <img src="/assets/images/icons/marker.png" height="40" alt="Marker" class="marker" />

                    {!! $guideSection->summary !!}

                    @if($src = $guideSection->image('medium'))
                        <img src="{{$src}}" alt="{{ ($guideSection->image_alt) ? $guideSection->image_alt : $guideSection->heading }}" class="img-responsive" />
                    @endif

                    {!! $guideSection->content !!}
                </div>
            </div>
        </div>
    </div>
    <?php $i++; ?>
@endforeach
@endif

<div class="container text-center">

    <a class="btn btn-lg btn-primary spacer" onclick="javascript:window.print();">Print This Guide</a>

</div>

@if(!$moreGuides->isEmpty())
<div class="container text-center">

    <h2 class="heading">More O'Connors travel guides</h2>

    <div class="row">

        @foreach($moreGuides as $moreGuide)
            <a href="{{ route('guide.show', $moreGuide->slug) }}" class="col-md-4 grid-box">
                <div class="img-container">
                    <img class="img-responsive" src="{{ $moreGuide->image('small') }}" alt="{{ ($moreGuide->image_alt) ? $moreGuide->image_alt : $moreGuide->heading }}" />
                </div>
                <h3>{{ $moreGuide->title }}</h3>
                <h6>From {{ $moreGuide->location }}</h6>
                {!! $moreGuide->summary !!}
            </a>
        @endforeach

    </div>

</div>
@endif

<div class="container text-center">

    <a href="{{ route('guide.index') }}" class="btn btn-lg btn-primary spacer">View All Travel Guides</a>

</div>

@endsection