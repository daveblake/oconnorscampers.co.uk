@extends('admin._layout')

@section('content')

<h1 class="page-header">Settings</h1>

@include('flash::message')

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Value</th>
            <th width="30%" class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($settings as $setting)
        <tr id="{{ $setting->id }}">
            <td>{{ $setting->name }}</td>
            <td>{{ $setting->value }}</td>
            <td class="text-right">
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.setting.edit', array( $setting->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Setting</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $settings->render() !!}

@stop