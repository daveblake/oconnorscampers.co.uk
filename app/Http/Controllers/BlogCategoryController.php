<?php

namespace App\Http\Controllers;

use App\BlogArticle;
use App\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Requests;

class BlogCategoryController extends Controller {

	public function frontendDetail($slug)
	{
		$blogCategories = BlogCategory::orderBy('order')->get();
		$category = BlogCategory::where('slug', $slug)->firstOrFail();
		$blogArticles = BlogArticle::where('blog_category_id', $category->id)->where('published', 1)->orderBy('published_at', 'desc')->paginate(5);
		$recentEntries = BlogArticle::where('published', 1)->orderBy('published_at', 'desc')->take(5)->get();

		return view('blog-category', compact('blogCategories', 'category', 'blogArticles', 'recentEntries'));
	}

	/**
	 * Display a listing of blog categories
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$blogCategories = BlogCategory::orderBy('order')->get();

		return view('admin.blog_category.index', compact('blogCategories'));
	}

	/**
	 * Show the form for creating a new blog category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$blogCategory = new BlogCategory();

		return view('admin.blog_category.create', compact('blogCategory'));
	}

	/**
	 * Store a newly created blog article in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
			'name' => 'required',
			'slug' => 'required|alpha_dash|unique:blog_categories',
		]);

		BlogCategory::create($request->all());

		flash()->success('Blog category created.');

		return redirect()->action('BlogCategoryController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return view('admin.blog_category.show', ['blogCategory' => BlogCategory::findOrFail($id)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$blogCategory = BlogCategory::findOrFail($id);

		return view('admin.blog_category.edit', compact('blogCategory'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$blogCategory = BlogCategory::findOrFail($id);

		$this->validate($request, [
			'name' => 'required',
			'slug' => 'required|alpha_dash|unique:blog_categories,slug,'.$id,
		]);

		$blogCategory->fill($request->all());
		$blogCategory->save();

		flash()->success('Blog category updated.');

		return redirect()->action('BlogCategoryController@index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$blogCategory = BlogCategory::findOrFail($id);
		$blogCategory->delete();

		flash()->success('Blog category deleted.');

		return redirect()->action('BlogCategoryController@index');
	}

}
