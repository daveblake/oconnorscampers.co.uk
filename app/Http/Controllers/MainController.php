<?php

namespace App\Http\Controllers;

use App\Banner;
use App\BlogArticle;
use App\Helpers\Property;
use App\Image;
use App\Page;
use FullyBooked\Client\Availability\Payload\ListAvailabilityPayload;
use FullyBooked\Client\Common\Exceptions\NotFoundException;
use FullyBooked\Client\Property\Payload\GetPropertyBySlugPayload;
use FullyBooked\Client\Property\Payload\ListPropertyPayload;
use App\Http\Requests;
use FullyBooked\Client\Property\Response\PropertyResponse;

class MainController extends Controller
{

  public function getHome()
  {
    $vans = $this->api()->propertyService()
      ->listProperties(
        (new ListPropertyPayload)->setLimit(20)
      );

    $vans = $vans->items;

    /** @var PropertyResponse $van */
    foreach($vans as $key => $van)
    {
      $vans[$key] = Property::createFromResponse($van);
    }

    $blogArticles = BlogArticle::where('published', 1)->orderBy(
      'published_at',
      'desc'
    )->take(3)->get();

    $banners = Banner::all();

    return view('home', compact('vans', 'blogArticles', 'banners'));
  }

  public function getVans()
  {
    $vanCollection = $this->api()->propertyService()
      ->listProperties(
        (new ListPropertyPayload())->setLimit(20)
      );

    $vans       = [];
    $motorhomes = [];

    /** @var PropertyResponse $van */
    foreach($vanCollection->items as $key => $van)
    {
      /** @var Property $van */
      $van = Property::createFromResponse($van);

      
      /*if($van->hasTag('motorhome'))
      {
        $motorhomes[] = $van;
        continue;
      }*/

      $vans[] = $van;
    }

    return view('vans', compact('vans', 'motorhomes'));
  }

  public function getVan($slug)
  {
    try
    {
      $van = $this->api()->propertyService()
        ->getPropertyBySlug(
          (new GetPropertyBySlugPayload())->setSlug($slug)
        );
    }
    catch(NotFoundException $e)
    {
      abort('404');
    }
    /** @var PropertyResponse $van */
    $van = Property::createFromResponse($van);

    $iconDesktop = '/uploads/images/icons/desktop/' . $van->slug . '.jpg';
    $iconMobile  = '/uploads/images/icons/mobile/' . $van->slug . '.jpg';

    $van->icons = [
      'desktop' => file_exists(
        public_path($iconDesktop)
      ) ? $iconDesktop : false,
      'mobile'  => file_exists(public_path($iconMobile)) ? $iconMobile : false,
    ];

    // Van Availability
    $dateFrom = new \DateTime();
    $dateTo   = clone $dateFrom;
    $dateTo->modify('+2 years');

    $vanAvailabilityResponse = $this->api()->availabilityService()
      ->listAvailability(
        (new ListAvailabilityPayload())->setPropertyUuid($van->uuid)
          ->setDateFrom($dateFrom->format('Y-m-d'))
          ->setDateTo($dateTo->format('Y-m-d'))
      );

    if(!empty($vanAvailabilityResponse->items))
    {
      $vanAvailability = [];
      if(!empty($vanAvailabilityResponse->items[$van->uuid]->items))
      {
        foreach($vanAvailabilityResponse->items[$van->uuid]->items as $vanAvailabilityItem)
        {
          $datetimeFrom                                             = new \DateTime(
            $vanAvailabilityItem->dateFrom
          );
          $datetimeTo                                               = new \DateTime(
            $vanAvailabilityItem->dateTo
          );
          $nights                                                   = $datetimeFrom->diff(
            $datetimeTo
          )->format('%a');
          $vanAvailability[$vanAvailabilityItem->dateFrom][$nights] = $vanAvailabilityItem->price;
        }
      }
    }
    else
    {
      $vanAvailability = [];
    }

    // Related Vans
    $relatedVans = $this->api()->propertyService()
      ->listProperties(
        (new ListPropertyPayload())->setMinGuests($van->maxGuests)
          ->setMaxGuests($van->maxGuests)
          ->setLimit(4)
      );
    $relatedVans = $relatedVans->items;
    if(!empty($relatedVans))
    {
      foreach($relatedVans as $key => $relatedVan)
      {
        if($relatedVan->uuid == $van->uuid)
        {
          unset($relatedVans[$key]);
        }

        /** @var PropertyResponse $relatedVan */
        $relatedVans[$key] = Property::createFromResponse($relatedVan);
      }
    }
    $relatedVans = array_slice($relatedVans, 0, 3);

    // Blog Articles
    $blogArticles = BlogArticle::where('published', 1)->where(
      'van_uuid',
      $van->uuid
    )->orderBy('published_at', 'desc')->take(3)->get();

    return view(
      'van',
      compact('van', 'vanAvailability', 'relatedVans', 'blogArticles')
    );
  }

  public function getGallery()
  {
    $page = Page::where('slug', 'gallery')->firstOrFail();

    $images = Image::where('display', 1)->orderBy('order')->get();

    return view('gallery', compact('page', 'images'));
  }

  public function getContact($status = null)
  {
    $page = Page::where('slug', 'contact')->firstOrFail();
    return view('contact', compact('status', 'page'));
  }

}
