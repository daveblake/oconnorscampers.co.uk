@extends('_layout')

@section('content')

<div class="container">

    <h2 class="text-center heading">Your Account</h2>
    <h4 align="center">From here you can manage your bookings and personal details.  </h4>
    <h4 align="center">To add or edit an existing booking please use the link under 'Your Bookings'</h4>
<br /><br />



    @include('flash::message')

    <div class="row">

        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Your Details</h4>
                </div>
                <div class="panel-body">

                    <a class="btn btn-sm btn-warning pull-right" href="{{ route('account.edit') }}">Edit Details</a>

                    <p><strong>{{ $customer->title }} {{ $customer->forename }} {{ $customer->surname }}</strong></p>
                    <p><strong>Address:</strong><br>
                        {{ $customer->address1 }}, {{ $customer->address2 }}<br>
                        {{ $customer->town }}<br>
                        {{ $customer->county }}<br>
                        {{ $customer->postcode }}<br>
                    </p>
                    <p>
                        <strong>Telephone (Home):</strong> {{ $customer->telHome }}<br>
                        <strong>Telephone (Mobile):</strong> {{ $customer->telMob }}<br>
                        <strong>Email:</strong> {{ $customer->email }}
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Useful Documents</h4>
                </div>
                <div class="panel-body">
                    <ul>
                        <li><a href="/assets/documents/Equipment.pdf">Van Equipment</a> </li>
                        <li><a href="/assets/documents/O'Connors Campsite List 2016.pdf">O'Connors Campsite List</a> </li>
                        <li><a href="/assets/documents/T&amp;Cs January 2016.pdf">Terms and Conditions - 2016</a> </li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="col-md-6">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Your Bookings</h4>
                </div>
                <div class="panel-body">
                    @if(empty($bookings))
                        <p><em>No bookings found</em></p>
                    @else
                        @foreach($bookings as $booking)
                            <p><strong><a href="{{ route('account.booking', $booking->ref) }}">{{ $booking->ref }} -  Add Drivers, Extras and Make Payments.</a></strong><br>
                                {{ $booking->property->name }} / &pound;{{ $booking->totalPrice }}<br>
                                {{ date('l jS F Y', strtotime($booking->dateFrom)) }} - {{ date('l jS F Y', strtotime($booking->dateTo)) }}
                            </p>
                            <hr>
                        @endforeach
                    @endif
                </div>
            </div>

        </div>

    </div>

</div>

<div class="container text-center">

    <a href="{{ route('logout') }}" class="btn btn-lg btn-primary spacer">Logout</a>

</div>

@endsection