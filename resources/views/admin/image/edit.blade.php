@extends('admin._layout')

@section('content')

<h1 class="page-header">Edit Image <small>{!! $image->title !!}</small></h1>

@include('layout.partials.errors')

{!! Form::model($image, array('route' => array('admin.image.update', $image->id), 'method' => 'PUT')) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::text('description', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        @if($image->image_path)
            <img src="{{$image->image('small')}}" class="img-responsive" />
        @endif
    </div>

    <div class="form-group">
        {!! Form::label('display', 'Display?') !!}
        {!! Form::checkbox('published', '1', $image->display, array('class' => 'checkbox')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

@stop