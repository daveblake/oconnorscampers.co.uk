@extends('_layout')

@section('content')

<section id="search-bar">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <h3>Search Availability</h3>

            </div>

            <div class="col-sm-6 text-right">

                <div class="view-switcher">
                    <a class="active" href="{{ route('availability.vans') }}">Van View</a><a href="{{ route('availability.calendar') }}">Calendar View</a>
                </div>

            </div>

        </div>

        <form class="row form-inline">

            <div class="col-lg-9 col-md-10">

                <div class="form-group">
                    <label for="nights">Booking</label>
                    {!! Form::select('nights',
                        [
                            '3' => 'Weekend (F-M)',
                            '4' => 'Mid-Week (M-F)',
                            '7' => 'Full Week',
                        ],
                        Input::get('nights'), ['placeholder' => 'Select Period', 'id' => 'nights', 'class' => 'form-control', 'required' => 'required' ]) !!}
                </div>

                <div class="form-group">
                    <label for="date">From</label>
                    <input type="text" id="collection-date-calendar" class="form-control" required="required"
                        @if(Input::has('date'))
                            value="{{ (new \DateTime(Input::get('date')))->format('j F Y') }}"
                        @else
                            value=""
                        @endif />
                    <input type="hidden" id="collection-date" name="date" value="{{ Input::get('date') }}" />
                </div>

                <div class="form-group">
                    <label for="sleeps">Berths</label>
                    {!! Form::select('sleeps',
                        [
                            '2' => '2 or More',
                            '3' => '3 or More',
                            '4' => '4 or More',
                            '5' => '5 or More',
                            '6' => '6 or More',
                        ],
                        Input::get('sleeps'), ['class' => 'form-control', 'required' => 'required' ]) !!}
                </div>

            </div>

            <div class="col-lg-3 col-md-2">
                <button type="submit" class="btn btn-black btn-block">Search</button>
            </div>

        </form>

    </div>

</section>

<div class="container">

    <div class="row text-center">

        @if(!empty($availableVans))
            @foreach($availableVans as $van)
                <div class="col-md-3 col-sm-4 van-box">
                    @if ( $van->hasImageFromTag('side-med') )
                        <a href="{{ route('van.show', $van->slug) }}"><img class="img-responsive" src="{{$van->imageFromTag('side-med')->fullPath}}" alt="{{ $van->name }}" /></a>
                    @else
                        <a href="{{ route('van.show', $van->slug) }}"><img class="img-responsive" src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_med.jpg" alt="{{ $van->name }}" /></a>
                    @endif
                    <h3><a href="{{ route('van.show', $van->slug) }}">{{ $van->name }}</a></h3>
                    <h6>{{ $van->maxGuests }} Berth &bull; {{ $van->customValues->type }}</h6>
                    <p>{{ $van->summary }}</p>
                    @if(isset($van->price))
                        <p class="price">&pound;{{ $van->price }}</p>
                    @endif

                    <div class="indent">
                        @if(isset($van->price))
                            <a href="{{ route('booking', $van->slug) }}?date={{ Input::get('date') }}&nights={{ Input::get('nights') }}" class="btn btn-block btn-default" @if(isset($van->customValues->color))style="color:#ffffff;background:#{{ $van->customValues->color }}"@endif>Book Online</a>
                        @endif
                        <a href="{{ route('van.show', $van->slug) }}" class="btn btn-block btn-default">Van Details</a>
                    </div>
                </div>
            @endforeach
        @endif

        @if(!empty($unavailableVans))
            @foreach($unavailableVans as $van)
                <div class="col-md-3 col-sm-4 van-box unavailable">
                    @if ( $van->hasImageFromTag('side-med') )
                        <img class="img-responsive" src="{{$van->imageFromTag('side-med')->fullPath}}" alt="{{ $van->name }}" />
                        @else
                    <img class="img-responsive" src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_med.jpg" alt="{{ $van->name }}" />
                    @endif
                    <h3>{{ $van->name }}</h3>
                    <h6>{{ $van->maxGuests }} Berth &bull; {{ $van->customValues->type }}</h6>
                    <p>{{ $van->summary }}</p>
                    <p class="price">Unavailable</p>

                    <div class="indent">
                        <a href="{{ route('van.show', $van->slug) }}" class="btn btn-block btn-default">Van Details</a>
                    </div>
                </div>
            @endforeach
        @endif

    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
function filterStartDays() {
    var nights = $('#nights').val();
    $('#collection-date-calendar').datepicker( "option", "beforeShowDay", function(date) {
        var day = date.getDay();
        var enabled = false;
        switch(nights) {
            case "3":
                if (day == 5) enabled = true;
                break;
            case "4":
                if (day == 1) enabled = true;
                break;
            case "7":
                if (day == 5 || day == 1) enabled = true;
                break;
            default:
                if (day == 5 || day == 1) enabled = true;
        }
        return [enabled];
    });
}

$(document).ready(function(){

    // Collection date picker
    $('#collection-date-calendar').datepicker({
        dateFormat: "d MM yy",
        altField: "#collection-date",
        altFormat: "yy-mm-dd",
        minDate: +1,
        maxDate: "+2Y",
        firstDay: 1,
        dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ],
        beforeShowDay: function(date) {
            var day = date.getDay();
            return [day == 5 || day == 1];
        }
    });

    filterStartDays();
    $('#nights').change(function() {
        filterStartDays();
    });

});
</script>
@endsection