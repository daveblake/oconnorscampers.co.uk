<?php

namespace App\Http\Controllers;

use App\Helpers\OptionalExtra;
use App\Http\Requests;
use FullyBooked\Client\Booking\Payload\GetBookingByRefPayload;
use FullyBooked\Client\Booking\Payload\GetBookingPayload;
use FullyBooked\Client\Booking\Payload\ListBookingPayload;
use FullyBooked\Client\Booking\Payload\UpdateBookingPayload;
use FullyBooked\Client\BookingGuest\Payload\CreateBookingGuestPayload;
use FullyBooked\Client\BookingGuest\Payload\DeleteBookingGuestPayload;
use FullyBooked\Client\BookingGuest\Payload\UpdateBookingGuestPayload;
use FullyBooked\Client\BookingLine\Payload\CreateBookingLinePayload;
use FullyBooked\Client\BookingLine\Payload\DeleteBookingLinePayload;
use FullyBooked\Client\BookingLine\Payload\UpdateBookingLinePayload;
use FullyBooked\Client\BookingLine\Response\BookingLineResponse;
use FullyBooked\Client\Common\Exceptions\FbApiException;
use FullyBooked\Client\Common\Exceptions\NotFoundException;
use FullyBooked\Client\Country\Payload\GetCountryPayload;
use FullyBooked\Client\Country\Payload\ListCountryPayload;
use FullyBooked\Client\Customer\Payload\GetCustomerByEmailPayload;
use FullyBooked\Client\Customer\Payload\GetCustomerLoginPayload;
use FullyBooked\Client\Customer\Payload\GetCustomerPayload;
use FullyBooked\Client\Customer\Payload\UpdateCustomerPayload;
use FullyBooked\Client\Customer\Response\CustomerResponse;
use FullyBooked\Client\CustomValue\Payload\SyncCustomValuePayload;
use FullyBooked\Client\OptionalExtra\Payload\ListOptionalExtraPayload;
use FullyBooked\Client\Password\Payload\EmailPasswordPayload;
use FullyBooked\Client\Password\Payload\ResetPasswordPayload;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

class CustomerController extends Controller
{

  protected function _offenceCodes()
  {
    $codes = [
      'AC10' => 'Failing to stop after an accident',
      'AC20' => 'Failing to give particulars or to report an accident within 24 hours',
      'AC30' => 'Undefined accident offences',
      'BA10' => 'Driving while disqualified by order of Court',
      'BA30' => 'Attempting to drive while disqualified by order of Court',
      'BA40' => 'Causing death by driving while disqualified',
      'BA60' => 'Causing serious injury by driving while disqualified',
      'CD10' => 'Driving without due care and attention',
      'CD20' => 'Driving without reasonable consideration for other road users',
      'CD30' => 'Driving without due care and attention/reasonable consideration',
      'CD40' => 'Causing death through careless driving when unfit through drink',
      'CD50' => 'Causing death by careless driving when unfit through drugs',
      'CD60' => 'Causing death by careless driving when alcohol level above limit',
      'CD70' => 'Causing death by careless driving then failing to supply a specimen for alcohol analysis',
      'CD80' => 'Causing death by careless, or inconsiderate, driving',
      'CD90' => 'Causing death by driving: unlicensed, disqualified or uninsured drivers',
      'CU10' => 'Using a vehicle with defective brakes',
      'CU20' => 'Causing or likely to cause danger by reason of use of unsuitable vehicle or using a vehicle with parts or accessories (excluding brakes, steering or tyres) in a dangerous condition',
      'CU30' => 'Using a vehicle with defective tyre(s)',
      'CU40' => 'Using a vehicle with defective steering',
      'CU50' => 'Causing or likely to cause danger by reason of load or passengers',
      'CU80' => 'Breach of requirements as to control of the vehicle, mobile telephone etc.',
      'DD10' => 'Causing serious injury by dangerous driving',
      'DD40' => 'Dangerous Driving',
      'DD60' => 'Manslaughter or culpable homicide while driving a vehicle',
      'DD80' => 'Causing death by dangerous driving',
      'DD90' => 'Furious driving',
      'DR10' => 'Driving or attempting to drive with alcohol concentration above limit',
      'DR20' => 'Driving or attempting to drive when unfit through drink',
      'DR30' => 'Driving or attempting to drive then refusing to provide a specimen',
      'DR31' => 'Driving or attempting to drive then refusing to give permission for analysis of a blood sample that was taken without consent due to incapacity',
      'DR40' => 'In charge of a vehicle while alcohol level above limit',
      'DR50' => 'In charge of a vehicle while unfit through drink',
      'DR60' => 'Failure to provide a specimen for analysis in circumstances other than driving or attempting to drive',
      'DR61' => 'Refusing to give permission for analysis of a blood sample that was taken without consent due to incapacity in circumstances other than driving or attempting to drive',
      'DR70' => 'Failing to provide specimen for breath test',
      'DG10' => 'Driving or attempting to drive with drug level above the specified limit',
      'DG40' => 'In charge of a vehicle while drug level above specified limit',
      'DG60' => 'Causing death by careless driving with drug level above the limit',
      'DR80' => 'Driving or attempting to drive when unfit through drugs',
      'DR90' => 'In charge of a vehicle when unfit through drugs',
      'IN10' => 'Using a vehicle uninsured against third party risks',
      'LC20' => 'Driving otherwise than in accordance with a licence',
      'LC30' => 'Driving after making a false declaration about fitness when applying for a licence',
      'LC40' => 'Driving a vehicle having failed to notify a disability',
      'LC50' => 'Driving after a licence has been revoked or refused on medical grounds',
      'MS10' => 'Leaving a vehicle in a dangerous position',
      'MS20' => 'Unlawful pillion riding',
      'MS30' => 'Playstreet Offence',
      'MS50' => 'Motor racing on the highway',
      'MS60' => 'Offences not covered by other codes (including offences relating to breach of requirements as to control of vehicle)',
      'MS70' => 'Driving with uncorrected defective eyesight',
      'MS80' => 'Refusing to submit to an eyesight test',
      'MS90' => 'Failing to give information as to identity of driver etc.',
      'MW10' => 'Contravention of Special Roads Regulations (excluding speed limits)',
      'PC10' => 'Undefined contravention of Pedestrian crossing Regulations',
      'PC20' => 'Contravention of Pedestrian crossing Regulations with moving vehicle',
      'PC30' => 'Contravention of Pedestrian crossing Regulations Stationary vehicle',
      'SP10' => 'Exceeding goods vehicle speed limit',
      'SP20' => 'Exceeding speed limit for type of vehicle (excluding goods or passenger vehicles)',
      'SP30' => 'Exceeding statutory speed limit on a public road',
      'SP40' => 'Exceeding passenger vehicle speed limit',
      'SP50' => 'Exceeding speed limit on a motorway',
      'TS10' => 'Failing to comply with traffic light signals',
      'TS20' => 'Failing to comply with double white lines',
      'TS30' => 'Failing to comply with a \'Stop\' sign',
      'TS40' => 'Failing to comply with directions of a constable/traffic warden',
      'TS50' => 'Failing to comply with traffic sign (excluding ‘stop’ signs, traffic lights or double white lines)',
      'TS60' => 'Failing to comply with a school crossing patrol sign',
      'TS70' => 'Undefined failure to comply with a traffic direction sign',
      'TT99' => 'Disqualified for having more than 12 points in the “totting up” process',
      'UT50' => 'Aggravated taking of a vehicle',
      'MR09' => 'Reckless or dangerous driving (whether or not resulting in death, injury or serious risk)',
      'MR19' => 'Wilful failure to carry out the obligation placed on driver after being involved in a road accident (hit or run)',
      'MR29' => 'Driving a vehicle while under the influence of alcohol or other substance affecting or diminishing the mental and physical abilities of a driver',
      'MR39' => 'Driving a vehicle faster than the permitted speed',
      'MR49' => 'Driving a vehicle whilst disqualified',
      'MR59' => 'Other conduct constituting an offence for which a driving disqualification has been imposed by the State of Offence'
    ];

    foreach($codes as $code => $label)
    {
      $codes[$code] = $code . ' - ' . $label;
    }

    return $codes;
  }

  public function getLogin()
  {
    return view('login');
  }

  public function postLogin(Request $request, Guard $auth)
  {

    $this->validate(
      $request,
      [
        'email'    => 'required|email',
        'password' => 'required',
      ]
    );

    // Login customer with FullyBooked
    try
    {
      $customer = $this->api()->customerService()
        ->getCustomerByLogin(
          (new GetCustomerLoginPayload())
            ->setEmail($request->get('email'))
            ->setPassword($request->get('password'))
        );
    }
    catch(FbApiException $e)
    {
      if(false === $customer = $this->_overrideLogin($request, $auth))
      {
        flash()->error($e->getMessage());
        return redirect()->action('CustomerController@getLogin')
          ->withInput();
      }
    }

    $request->session()->put('customerUuid', $customer->uuid);
    return redirect()->action('CustomerController@getAccount');
  }

  protected function _overrideLogin($request, $auth)
  {
    if($request->get('password') != 'oconnors')
    {
      return false;
    }

    if(!$auth->user())
    {
      return false;
    }

    try
    {
      $customer = $this->api()->customerService()->getCustomerByEmail(
        (new GetCustomerByEmailPayload)
          ->setEmail($request->get('email'))
      );
    }
    catch(NotFoundException $e)
    {
      return false;
    }

    return $customer;
  }

  public function getAccount(Request $request)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }

    $customerUuid = $request->session()->get('customerUuid');

    try
    {
      $customer = $this->api()->customerService()
        ->getCustomer(
          (new GetCustomerPayload())
            ->setUuid($customerUuid)
        );
    }
    catch(NotFoundException $e)
    {
      $request->session()->remove('customerUuid');
      return redirect()->route('login')
        ->withErrors('Your session timed out and we have been logged out');
    }

    $bookings = $this->api()->bookingService()
      ->listBookings(
        (new ListBookingPayload())
          ->setCustomerUuid($customerUuid)
          ->setStub(0)
      );
    $bookings = $bookings->items;

    return view('account', compact('customer', 'bookings'));
  }

  public function getCustomer(Request $request)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }

    $customerUuid = $request->session()->get('customerUuid');

    $customer = $this->api()->customerService()
      ->getCustomer(
        (new GetCustomerPayload())
          ->setUuid($customerUuid)
      );

    $countries         = [];
    $countriesResponse = $this->api()->countryService()
      ->listCountries(
        (new ListCountryPayload())
      );
    if(!empty($countriesResponse->items))
    {
      foreach($countriesResponse->items as $country)
      {
        $countries[$country->uuid] = $country->name;
      }
    }

    return view('account-customer', compact('customer', 'countries'));
  }

  public function postCustomer(Request $request)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }

    // Validate customer data
    $this->validate(
      $request,
      [
        'forename' => 'required',
        'surname'  => 'required',
        'email'    => 'required|email',
        'country'  => 'required',
        'telMob'   => 'required',
      ]
    );

    $customerUuid = $request->session()->get('customerUuid');

    $customer = $this->api()->customerService()
      ->updateCustomer(
        (new UpdateCustomerPayload())
          ->setUuid($customerUuid)
          ->setTitle($request->get('title'))
          ->setForename($request->get('forename'))
          ->setSurname($request->get('surname'))
          ->setAddress1($request->get('address1'))
          ->setAddress2($request->get('address2'))
          ->setTown($request->get('town'))
          ->setCounty($request->get('county'))
          ->setPostcode($request->get('postcode'))
          ->setCountryUuid($request->get('country'))
          ->setTelHome($request->get('telHome'))
          ->setTelMob($request->get('telMob'))
          ->setEmail($request->get('email'))
      );

    flash()->success('Your details have been updated.');

    return redirect()->action('CustomerController@getAccount');
  }

  public function getBooking($ref, Request $request)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }

    $customerUuid = $request->session()->get('customerUuid');

    $customer = $this->api()->customerService()
      ->getCustomer(
        (new GetCustomerPayload())
          ->setUuid($customerUuid)
      );

    try
    {
      $booking = $this->api()->bookingService()
        ->getBookingByRef(
          (new GetBookingByRefPayload())
            ->setBookingRef($ref)
        );
    }
    catch(NotFoundException $e)
    {
      abort('404');
    }

    $optionalExtras         = [];
    $insuranceLoadings      = [];
    $optionalExtrasTotal    = 0;
    $insuranceLoadingsTotal = 0;
    /** @var BookingLineResponse $bookingLine */
    foreach($booking->lines as $bookingLine)
    {
      if($bookingLine->optionalExtra && in_array('insurance', $bookingLine->optionalExtra->tags))
      {
        $insuranceLoadings[] = $bookingLine;
        $insuranceLoadingsTotal += $bookingLine->priceTotal;
        continue;
      }
      $optionalExtras[] = $bookingLine;
      $optionalExtrasTotal += $bookingLine->priceTotal;
    }
    $optionalExtrasTotal    = number_format($optionalExtrasTotal, 2, '.', '');
    $insuranceLoadingsTotal = number_format($insuranceLoadingsTotal, 2, '.', '');

    return view(
      'account-booking',
      compact(
        'customer',
        'booking',
        'optionalExtras',
        'insuranceLoadings',
        'optionalExtrasTotal',
        'insuranceLoadingsTotal'
      )
    );
  }

  public function getBookingEdit($ref, Request $request)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }

    $booking = $this->api()->bookingService()
      ->getBookingByRef(
        (new GetBookingByRefPayload())
          ->setBookingRef($ref)
      );

    if(!$booking->canEdit)
    {
      return redirect()->action(
        'CustomerController@getBooking',
        ['ref' => $booking->ref]
      );
    }

    $unusedFields = [
      'country',
      'telephone_home',
      'telephone_mobile',
      'email_address',
    ];

    $booleanFields = [
      'full_licence',
      'defects',
      'convictions',
      'student',
      'accidents',
      'refused_insurance',
      'student',
    ];

    $drivers = [];

    $booking->hasDriver = false;
    foreach($booking->guests as $k => $guest)
    {
      if($guest->type == 'driver')
      {
        $booking->hasDriver = true;

        $drivers[] = $guest;
      }
      foreach($unusedFields as $field)
      {
        if(isset($guest->customValues->{$field}))
        {
          unset($guest->customValues->{$field});
        }
      }
      foreach($booleanFields as $field)
      {
        if(isset($guest->customValues->{$field}))
        {
          $guest->customValues->{$field} = $guest->customValues->{$field} ? 'Yes' : 'No';
        }
      }
      $booking->guests[$k] = $guest;
    }

    $bookingExtras = [];
    /** @var BookingLineResponse $bookingLine */
    foreach($booking->lines as $bookingLine)
    {
      if(!is_object($bookingLine->optionalExtra))
      {
        continue;
      }
      if(isset($bookingExtras[$bookingLine->optionalExtra->uuid]))
      {
        if(!is_array($bookingExtras[$bookingLine->optionalExtra->uuid]))
        {
          $tmpLine = $bookingExtras[$bookingLine->optionalExtra->uuid];

          $bookingExtras[$bookingLine->optionalExtra->uuid] = [
            $tmpLine,
            $bookingLine
          ];
        }
        else
        {
          $bookingExtras[$bookingLine->optionalExtra->uuid][] = $bookingLine;
        }
      }
      else
      {
        $bookingExtras[$bookingLine->optionalExtra->uuid] = $bookingLine;
      }
    }

    $optionalExtras = $this->api()->optionalExtraService()
      ->listOptionalExtras(
        (new ListOptionalExtraPayload)->setTag('extra')->setLimit(100)
      );
    $optionalExtras = $optionalExtras->items;

    foreach($optionalExtras as $key => $optionalExtra)
    {

      if(isset($bookingExtras[$optionalExtra->uuid]))
      {
        if(is_array($bookingExtras[$optionalExtra->uuid]))
        {
          $defaultValue = 0;
          foreach($bookingExtras[$optionalExtra->uuid] as $extra)
          {
            $defaultValue += $extra->quantity;
          }
        }
        else
        {
          $defaultValue = $bookingExtras[$optionalExtra->uuid]->quantity;
        }
      }
      else
      {
        $defaultValue = 0;
      }
      $optionalExtras[$key] = OptionalExtra::createFromResponse(
        $optionalExtra,
        $defaultValue
      );
    }

    $insuranceOptions       = $this->_insuranceOptions($booking->nights);
    $collisionDamageWaivers = [];
    $ageOptionalExtra       = false;
    $cdwIsSet               = false;
    foreach($insuranceOptions as $k => $option)
    {

      if(isset($bookingExtras[$option->uuid]))
      {
        if(is_array($bookingExtras[$option->uuid]))
        {
          $defaultValue = 0;
          foreach($bookingExtras[$option->uuid] as $extra)
          {
            if(! is_array($bookingExtras[$option->uuid]) ) {
              $defaultValue += $bookingExtras[$option->uuid]->quantity;
            } else {
              foreach ( $bookingExtras[$option->uuid] as $extra2 ) {
                $defaultValue += $extra->quantity;
              }
            }
          }
        }
        else
        {
          $defaultValue = $bookingExtras[$option->uuid]->quantity;
        }
      }
      else
      {
        $defaultValue = 0;
      }

      $option = OptionalExtra::createFromResponse(
        $option,
        $defaultValue
      );

      if(in_array('age', $option->tags))
      {
        $ageOptionalExtra = $option;
        unset($insuranceOptions[$k]);
      }
      elseif(in_array('cdw', $option->tags))
      {
        $collisionDamageWaivers[] = $option;
        if($defaultValue > 0)
        {
          $cdwIsSet = true;
        }
        unset($insuranceOptions[$k]);
      }
    }

    if(!$cdwIsSet)
    {
      $collisionDamageWaivers[0]->defaultValue = 1;

      $bookingLine = (new CreateBookingLinePayload)
        ->setBookingUuid($booking->uuid)
        ->setItem($collisionDamageWaivers[0]->description)
        ->setPricePerItem($collisionDamageWaivers[0]->amount)
        ->setQuantity(1)
        ->setOptionalExtraUuid($collisionDamageWaivers[0]->uuid);

      $this->api()->bookingLineService()->createBookingLine($bookingLine);
    }

    $allowDriverCWD = true;
    foreach($drivers as $driver)
    {
      if($driver->age < 25)
      {
        $allowDriverCWD = false;
        break;
      }
    }

    $countries         = [];
    $countriesResponse = $this->api()->countryService()
      ->listCountries(
        (new ListCountryPayload())
      );
    if(!empty($countriesResponse->items))
    {
      foreach($countriesResponse->items as $country)
      {
        $countries[$country->uuid] = $country->name;
      }
    }

    $offenceCodes = $this->_offenceCodes();

    $extraBasePrice    = 0;
    $insuranceLoadings = [];
    foreach($booking->lines as $bookingLine)
    {
      if(!isset($bookingLine->optionalExtra))
      {
        continue;
      }
      if(in_array('cdw', $bookingLine->optionalExtra->tags))
      {
        continue;
      }
      if(in_array('insurance', $bookingLine->optionalExtra->tags))
      {
        $insuranceLoadings[] = $bookingLine;
        $extraBasePrice += $bookingLine->priceTotal;
      }
    }

    return view(
      'account-booking-edit',
      compact(
        'booking',
        'optionalExtras',
        'insuranceOptions',
        'countries',
        'offenceCodes',
        'allowDriverCWD',
        'collisionDamageWaivers',
        'ageOptionalExtra',
        'insuranceLoadings',
        'extraBasePrice'
      )
    );
  }

  protected function _insuranceOptions($nights)
  {
    $insuranceOptions = $this->api()->optionalExtraService()
      ->listOptionalExtras(
        (new ListOptionalExtraPayload)->setTag('insurance')->setLimit(100)
      );
    $insuranceOptions = $insuranceOptions->items;
    if(!empty($insuranceOptions))
    {
      foreach($insuranceOptions as $key => $insuranceOption)
      {
        if($insuranceOption->frequency == 'per_day')
        {
          $insuranceOption->amount = number_format($insuranceOption->amount * ($nights), 2, '.', '');
        }

        if(isset($bookingExtras[$insuranceOption->uuid]))
        {
          $defaultValue = $bookingExtras[$insuranceOption->uuid]->quantity;
        }
        else
        {
          $defaultValue = 0;
        }
        $insuranceOptions[$key] = OptionalExtra::createFromResponse($insuranceOption, $defaultValue);
      }
    }

    return $insuranceOptions;
  }

  public function postBookingUpdate($ref, Request $request, Guard $auth)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }

    $booking = $this->api()->bookingService()
      ->getBookingByRef(
        (new GetBookingByRefPayload())
          ->setBookingRef($ref)
      );

    if(!$booking->canEdit)
    {
      return redirect()->action(
        'CustomerController@getBooking',
        ['ref' => $booking->ref]
      );
    }

    $existingGuests = [];
    foreach($booking->guests as $guest)
    {
      $existingGuests[$guest->uuid] = $guest;
    }

    $drivers          = [];
    $insuranceOptions = $this->_insuranceOptions($booking->nights);

    $newOptionalExtras    = [];
    $removeOptionalExtras = [];
    $updateOptionalExtras = [];

    if($request->has('driver'))
    {
      $guests = $request->get('driver', []);
      foreach($guests as $guest)
      {
        if(isset($guest['uuid']) && isset($existingGuests[$guest['uuid']]))
        {
          $guestPayload = new UpdateBookingGuestPayload;
          $guestPayload->setUuid($guest['uuid']);
          unset($existingGuests[$guest['uuid']]);
          $create = false;
        }
        else
        {
          $guestPayload = new CreateBookingGuestPayload;
          $create       = true;
        }

        if(isset($guest['dob']) && $guest['dob'] != '' && in_array($guest['type'], ['driver', 'child']))
        {
          $dob         = \DateTime::createFromFormat('d/m/Y', $guest['dob']);
          $bookingDate = new \DateTime($booking->dateFrom);

          $guest['age'] = $dob->diff($bookingDate)->y;
        }
        else
        {
          $guest['age'] = 0;
        }

        $guestPayload->setBookingUuid($booking->uuid);
        $guestPayload->setType(strtolower($guest['type']));
        $guestPayload->setName($guest['name']);
        $guestPayload->setAge($guest['age']);

        $customValues = [];

        if(isset($guest['dob']) && $guest['dob'] != '' && ($guest['type'] == 'driver' || $guest['type'] == 'child'))
        {
          $customValues['dob'] = $guest['dob'];
        }


        if($guest['type'] == 'driver')
        {
          $customValues['licence_number']    = $guest['licence_number'];
          $customValues['licence_country']   = $guest['licence_country'];
          $customValues['pass_date']         = $guest['pass_date'];
          $customValues['full_licence']      = $guest['full_licence'] == 'Yes';
          $customValues['address_type']      = isset($guest['address_type']) && $guest['address_type'] == 'custom' ? 'custom' : 'account';
          $customValues['defects']           = $guest['defects'] == 'Yes';
          $customValues['convictions']       = $guest['convictions'] == 'Yes';
          $customValues['student']           = $guest['student'] == 'Yes';
          $customValues['refused_insurance'] = $guest['refused_insurance'] == 'Yes';
          $customValues['accidents']         = $guest['accidents'] == 'Yes';

          if($customValues['address_type'] == 'custom')
          {
            $customValues['address1'] = $guest['address1'];
            $customValues['address2'] = $guest['address2'];
            $customValues['town']     = $guest['town'];
            $customValues['county']   = $guest['county'];
            $customValues['postcode'] = $guest['postcode'];
          }

          if($customValues['convictions'])
          {
            foreach($guest['offence_code'] as $key => $code)
            {
              $customValues['offence_code-' . $key] = $code;
            }
            foreach($guest['offence_date'] as $key => $date)
            {
              $customValues['offence_date-' . $key] = $date;
            }
          }

          $drivers[] = $guest;
        }

        if($create)
        {
          $guestResponse = $this->api()->bookingGuestService()
            ->createBookingGuest($guestPayload);
          $guest['uuid'] = $guestResponse->message;
        }
        else
        {
          $this->api()->bookingGuestService()
            ->updateBookingGuest($guestPayload);
        }

        $this->api()->customValueService()
          ->syncCustomValues(
            (new SyncCustomValuePayload)
              ->setRemoteType('guest')
              ->setRemoteUuid($guest['uuid'])
              ->setCustomValues($customValues)
          );
      }

      $countries         = [];
      $countriesResponse = $this->api()->countryService()
        ->listCountries(
          (new ListCountryPayload())
        );
      if(!empty($countriesResponse->items))
      {
        foreach($countriesResponse->items as $country)
        {
          $countries[$country->uuid] = $country;
        }
      }

      foreach($drivers as $driver)
      {
        foreach($insuranceOptions as $k => $insuranceOption)
        {
          if($driver['age'] < 25 && in_array('age', $insuranceOption->tags))
          {
            if(!isset($newOptionalExtras[$insuranceOption->uuid]))
            {
              $newOptionalExtras[$insuranceOption->uuid] = [
                'name'  => $insuranceOption->name,
                'price' => ($insuranceOption->amount),
                'qty'   => 1
              ];
            }
            else
            {
              $newOptionalExtras[$insuranceOption->uuid]['qty']++;
            }
          }
          if($driver['convictions'] == 'Yes' && in_array('convictions', $insuranceOption->tags))
          {
            if(!isset($newOptionalExtras[$insuranceOption->uuid]))
            {
              $newOptionalExtras[$insuranceOption->uuid] = [
                'name'  => $insuranceOption->name,
                'price' => ($insuranceOption->amount),
                'qty'   => 1
              ];
            }
            else
            {
              $newOptionalExtras[$insuranceOption->uuid]['qty']++;
            }
          }

          $licenceCountry = $countries[$driver['licence_country']];
          if($licenceCountry->code2Letter != 'GB' && in_array('international', $insuranceOption->tags))
          {
            if(!isset($newOptionalExtras[$insuranceOption->uuid]))
            {
              $newOptionalExtras[$insuranceOption->uuid] = [
                'name'  => $insuranceOption->name,
                'price' => ($insuranceOption->amount),
                'qty'   => 1
              ];
            }
            else
            {
              $newOptionalExtras[$insuranceOption->uuid]['qty']++;
            }
          }
        }
      }

      if(count($drivers) > 1)
      {
        $extraDrivers = count($drivers) - 1;
        foreach($insuranceOptions as $k => $insuranceOption)
        {
          if(in_array('extra-driver', $insuranceOption->tags))
          {
            $newOptionalExtras[$insuranceOption->uuid] = [
              'name'  => $insuranceOption->name,
              'price' => ($insuranceOption->amount),
              'qty'   => $extraDrivers
            ];
          }
        }
      }

      //Any existing guest remaining have been removed...
      if(count($existingGuests) > 0)
      {
        foreach($existingGuests as $guest)
        {
          $this->api()->bookingGuestService()
            ->deleteBookingGuest((new DeleteBookingGuestPayload)->setUuid($guest->uuid));
        }
      }
    }

    $updatedExtrasPrice = $booking->extrasPrice;

    foreach($booking->lines as $k => $bookingLine)
    {
      if(in_array('insurance', $bookingLine->optionalExtra->tags))
      {
        $removeOptionalExtras[] = $bookingLine->uuid;
        $updatedExtrasPrice -= $bookingLine->priceTotal;
        unset($booking->lines[$k]);
      }
    }
    //rekey..

    //update price for calculated extras
    foreach($newOptionalExtras as $optionalExtra)
    {
      $updatedExtrasPrice += ($optionalExtra['price'] * $optionalExtra['qty']);
    }

    if($request->has('optional_extras'))
    {
      foreach($request->get('optional_extras') as $uuid => $data)
      {

        $data['qty'] = isset($data['qty']) ? $data['qty'] : 0;
        $extraExists = false;

        /** @var BookingLineResponse $bookingLine */
        foreach($booking->lines as $bookingLine)
        {
          if($bookingLine->optionalExtra->uuid == $uuid)
          {
            $extraExists = true;
            if($data['qty'] == 0)
            {
              $removeOptionalExtras[] = $bookingLine->uuid;

              $updatedExtrasPrice -= $bookingLine->priceTotal;
            }
            elseif($data['qty'] != $bookingLine->quantity || $data['price'] != $bookingLine->pricePerItem)
            {
              $updatedExtrasPrice -= $bookingLine->priceTotal;

              $bookingLine->quantity     = $data['qty'];
              $bookingLine->pricePerItem = $data['price'];
              $bookingLine->priceTotal   = $data['price'] * $data['qty'];

              $updatedExtrasPrice += $bookingLine->priceTotal;

              $updateOptionalExtras[] = $bookingLine;
            }
            else
            {
              unset($request->get('optional_extras')[$uuid]);
            }

            break;
          }
        }

        if($data['qty'] == 0 || $extraExists)
        {
          continue;
        }
        $newOptionalExtras[$uuid] = $data;
        $updatedExtrasPrice += $data['price'] * $data['qty'];
      }

      $updatedTotal   = $booking->totalPrice - $booking->extrasPrice + $updatedExtrasPrice;
      $updatedBalance = $updatedTotal - $booking->totalPaid;

      if($updatedBalance < 0 && !$auth->user())
      {
        flash()->error(
          'You cannot have a negative balance.<br> Please contact O\'Connors to make this change.'
        );
        return redirect()->back()->withInput();
      }

      foreach($removeOptionalExtras as $uuid)
      {
        $this->api()->bookingLineService()
          ->deleteBookingLine(
            (new DeleteBookingLinePayload)
              ->setUuid($uuid)
          );
      }

      /** @var BookingLineResponse $optionalExtra */
      foreach($updateOptionalExtras as $optionalExtra)
      {
        $this->api()->bookingLineService()
          ->updateBookingLine(
            UpdateBookingLinePayload::fromApiResponse($optionalExtra)
          );
      }

      $bookingTmp = array_values($booking->lines);
      $weight     = count($bookingTmp) ? $bookingTmp[count($booking->lines) - 1]->weight : 0;

      foreach($newOptionalExtras as $uuid => $optionalExtra)
      {
        $this->api()->bookingLineService()
          ->createBookingLine(
            (new CreateBookingLinePayload)
              ->setBookingUuid($booking->uuid)
              ->setItem($optionalExtra['name'])
              ->setOptionalExtraUuid($uuid)
              ->setPricePerItem($optionalExtra['price'])
              ->setQuantity($optionalExtra['qty'])
              ->setWeight($weight)
          );

        $weight++;
      }
    }

    $this->api()->bookingService()
      ->updateBooking(
        UpdateBookingPayload::fromApiResponse($booking)
          ->setArrivalTime($request->get('arrival_time'))
      );

    flash()->success('Your booking details have been updated.');

    return redirect()->action('CustomerController@getBooking', $ref);
  }

  public function postBookingPayment($ref, Request $request)
  {
    if(!$request->session()->has('customerUuid'))
    {
      return redirect()->action('CustomerController@getLogin');
    }
    $customerUuid = $request->session()->has('customerUuid');

    $booking = $this->api()->bookingService()
      ->getBookingByRef(
        (new GetBookingByRefPayload)
          ->setBookingRef($ref)
          ->setStub(
            false
          )
      );

    if($booking->customer->uuid != $customerUuid)
    {
      throw new AccessDeniedException(
        'You don\'t have permission to edit that booking'
      );
    }

    $amount = (float)$request->get('amount');
    if($amount <= 0 || $amount > $booking->balance)
    {
      return redirect()->back()->withErrors(
        'You have entered an invalid amount'
      );
    }

    /** @var CustomerResponse $customer */
    $customer = $booking->customer;

    $bookingDetails = [
      'title'           => $customer->title,
      'firstName'       => $customer->forename,
      'lastName'        => $customer->surname,
      'email'           => $customer->email,
      'billingPhone'    => $customer->telMob,
      'billingAddress1' => $customer->address1,
      'billingAddress2' => $customer->address2,
      'billingCity'     => $customer->town,
      'billingState'    => $customer->county,
      'billingPostcode' => $customer->postcode,
      'M_paymentType'   => 'customerAccount'
    ];

    $country = $this->api()->countryService()->getCountry(
      (new GetCountryPayload)->setUuid($customer->country)
    );

    $bookingDetails['country'] = $country->code2Letter;

    // Send details to payment gateway
    $card = \Omnipay::CreditCard($bookingDetails);

    $params   = [
      'card'          => $card,
      'transactionId' => $booking->ref,
      'amount'        => $amount,
      'currency'      => 'GBP',
      'description'   => 'Online Payment',
      'returnUrl'     => action(
        'BookingController@getConfirmation',
        ['ref' => $booking->uuid, 'update' => 1]
      ),
      'cancelUrl'     => route('account.booking', ['ref' => $booking->ref]),
    ];

    if ( $_SERVER['REMOTE_ADDR'] == '92.236.60.250' ) {
       $params['testMode'] = 100;
    }


    $response = \Omnipay::purchase($params)->send();

    if($response->isSuccessful())
    {
      // payment was successful: update database
      print_r($response);
    }
    elseif($response->isRedirect())
    {
      // redirect to offsite payment gateway
      return $response->getRedirectResponse();
    }
    else
    {
      // payment failed: display message to customer
      echo $response->getMessage();
    }
  }

  public function getConfirmPayment($ref)
  {
    try
    {
      $booking = $this->api()->bookingService()->getBookingByRef(
        (new GetBookingByRefPayload)->setBookingRef($ref)->setStub(true)
      );
    }
    catch(NotFoundException $e)
    {
      return redirect()->route('account');
    }

    return redirect()->route('account.booking', ['ref' => $booking->ref])
      ->with(['paymentSuccess' => 'Payment Successful']);
  }

  public function postEmailPassword(Request $request)
  {
    $rules = [
      'email' => 'required|email',
    ];
    $this->validate($request, $rules);

    try
    {
      $response = $this->api()->passwordService()
        ->emailPassword(
          (new EmailPasswordPayload)
            ->setEmail($request->get('email'))
        );
    }
    catch(\Exception $e)
    {
      flash()->error($e->getMessage());
      return redirect()->action('CustomerController@getLogin')
        ->withInput();
    }

    if(!$response->result)
    {
      flash()->error($response->message);
      return redirect()->action('CustomerController@getLogin')
        ->withInput();
    }

    flash()->success(
      'Instructions to reset your password have been sent to ' .
      $request->get('email')
    );
    return redirect()->action('CustomerController@getLogin')
      ->with('passwordReset', true);
  }

  public function getResetPassword($token = '', Request $request)
  {
    $email = $request->get('email');

    return view('reset-password')
      ->with(compact('email', 'token'));
  }

  public function postResetPassword(Request $request)
  {
    $rules = [
      'email'           => 'required|email',
      'token'           => 'required',
      'password'        => 'required|min:8',
      'passwordConfirm' => 'required|same:password',
    ];
    $this->validate($request, $rules);

    try
    {
      $response = $this->api()->passwordService()->resetPassword(
        (new ResetPasswordPayload)
          ->setEmail($request->get('email'))
          ->setToken($request->get('token'))
          ->setPassword(
            $request->get('password')
          )
      );
    }
    catch(\Exception $e)
    {
      flash()->error($e->getMessage());
      return redirect()->action('CustomerController@getResetPassword', ['token' => $request->get('token')])
        ->withInput();
    }
    if(!$response->result)
    {
      flash()->error($response->message);
      return redirect()->action('CustomerController@getResetPassword', ['token' => $request->get('token')])
        ->withInput();
    }

    flash()->success('Your password has been reset');
    return redirect()->action('CustomerController@getLogin')
      ->with('email', $request->get('email'));
  }

  public function getLogout(Request $request)
  {
    $request->session()->forget('customerUuid');
    return redirect()->action('CustomerController@getLogin');
  }

}
