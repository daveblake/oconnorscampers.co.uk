<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('blog_category_id');
			$table->string('title');
			$table->string('slug')->unique();
			$table->string('author');
			$table->date('published_at');
			$table->boolean('published');
			$table->text('summary');
			$table->text('content');
			$table->string('seo_title');
			$table->string('seo_description');
			$table->string('image_path');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('blog_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('slug')->unique();
			$table->integer('order');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_articles');
		Schema::drop('blog_categories');
	}

}
