@extends('_layout')

@section('content')

<div class="container text-center">

    <div class="row">

        <div class="col-md-offset-3 col-md-6 spacer">

            {!! Form::open(array( 'route'=>'login', 'method'=>'post' )) !!}

            <h3>Reset password</h3>

            @include('layout.partials.errors')

            <div class="form-group">
                <label>Email Address</label>
                <input name="email" type="email" class="form-control" placeholder="Email address" required autofocus>
            </div>

            <div class="form-group">
                <label>New Password</label>
                <input name="password" type="password" class="form-control" placeholder="New Password" required>
            </div>

            <div class="form-group">
                <label>Confirm Password</label>
                <input name="password_confirmation" type="password" class="form-control" placeholder="Confirm Password" required>
            </div>

            <br><button class="btn btn-lg btn-primary" type="submit">Reset Password</button>

            {!! Form::close() !!}

        </div>

    </div>

</div>

@endsection