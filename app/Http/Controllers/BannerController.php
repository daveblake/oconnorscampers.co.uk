<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Requests;

class BannerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::paginate(10);
        return view('admin.banner.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banner = new Banner();
        return view('admin.banner.create', compact('banner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'heading' => 'required',
        ]);

        $data = $request->all();

        Banner::create($data);

        flash()->success('Banner created.');

        return redirect()->action('BannerController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.banner.show', ['banner' => Banner::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);

        return view('admin.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::findOrFail($id);

        $this->validate($request, [
            'heading' => 'required',
        ]);

        $data = $request->all();

        $banner->fill($data);

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $uploadedImage = $request->file('image');
                $img = \Image::make($uploadedImage->getRealPath());

                $imagePath = 'banner/' . $banner->id . '.' . $uploadedImage->getClientOriginalExtension();

                $img->save(public_path('uploads/' . $imagePath));
                $banner->image_path = $imagePath;
            }
        }

        $banner->save();

        // TODO better cache clearing for images
        \Cache::flush();

        $banner->save();

        flash()->success('Banner updated.');

        return redirect()->action('BannerController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();

        flash()->success('Banner deleted.');

        return redirect()->action('BannerController@index');
    }

}
