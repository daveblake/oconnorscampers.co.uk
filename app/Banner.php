<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Banner extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'image_path',
        'heading',
        'subheading',
        'button_text',
        'button_url',
    ];

    public function image($template = 'original') {
        if ($this->image_path) {
            return '/'.config('imagecache.route').'/'.$template.'/'.$this->image_path;
        }

        return '/assets/images/placeholder.jpg';
    }

}
