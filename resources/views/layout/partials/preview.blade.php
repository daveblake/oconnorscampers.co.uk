@if(Request::is('preview/*'))
<div id="preview-bar">
    <div class="alert alert-danger">
        <p><strong><span class="glyphicon glyphicon-warning-sign"></span> You are viewing a preview of this page from the admin.</strong>
            <br>It might not be published on the site.</p>
    </div>
</div>
@endif