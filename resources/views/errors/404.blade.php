@extends('_layout')

@section('content')

    <div class="content container" style="margin-top:100px;margin-bottom:100px;">

        <h3>404 - Page Not Found</h3>
        <h4>Oh dear...</h4>
        <p>The page you were looking for could not be found. You can <a href="/">return to the home page</a> or use the navigation above to find your way around the site.</p>

    </div>

@endsection