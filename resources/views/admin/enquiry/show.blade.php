@extends('admin._layout')

@section('content')

  <h1 class="page-header">Show Enquiry
    <small>{{ $enquiry->name }}</small>
  </h1>

  @include('layout.partials.errors')

  <div class="row">

    <div class="col-md-4">
      <div class="panel panel-primary">
        <div class="panel-heading"><h3>Customer Details</h3></div>
        <div class="panel-body">
          <p><strong>Name</strong><br>
            {{ $enquiry->title }} {{ $enquiry->first_name }} {{ $enquiry->last_name }}</p>

          <p><strong>Email</strong><br>
            {{ $enquiry->email }}</p>

          <p><strong>Phone</strong><br>
            {{ $enquiry->phone }}</p>

          <p><strong>Referrer</strong><br>
            {{ $enquiry->referrer }}</p>
        </div>
      </div>
    </div>

    <div class="col-md-8">
      <div class="panel panel-primary">
        <div class="panel-heading"><h3>Enquiry</h3></div>
        <div class="panel-body">
          <p>{!! nl2br(e($enquiry->enquiry)) !!}</p>
        </div>
        <div class="panel-footer">
          <small>Sent: {{ $enquiry->created_at->toDayDateTimeString() }}</small>
        </div>
      </div>
    </div>

  </div>

@stop