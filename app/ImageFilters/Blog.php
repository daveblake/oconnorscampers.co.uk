<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Blog implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->fit(600, 400);
    }
}