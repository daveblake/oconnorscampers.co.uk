<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogArticle extends Model {

	use SoftDeletes;
	protected $dates = ['deleted_at', 'published_at'];

	protected $fillable = [
		'blog_category_id',
		'van_uuid',
		'title',
		'slug',
		'author',
		'published_at',
		'published',
		'summary',
		'content',
		'seo_title',
		'seo_description',
		'image_path',
		'image_alt',
	];


	public function blogCategory() {
		return $this->belongsTo('App\BlogCategory');
	}

	public function image($template = 'original') {
		if ($this->image_path) {
			return '/'.config('imagecache.route').'/'.$template.'/'.$this->image_path;
		}

		return '/assets/images/placeholder.jpg';
	}

	public function next(){
		return BlogArticle::where('published', 1)->where('published_at', '>', $this->published_at)->orderBy('published_at','asc')->first();
	}
	public  function previous(){
		return BlogArticle::where('published', 1)->where('published_at', '<', $this->published_at)->orderBy('published_at','desc')->first();
	}

}