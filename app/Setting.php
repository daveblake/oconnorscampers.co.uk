<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Setting extends Model
{

    protected $fillable = [
        'value',
    ];

    public static function getValueByName($name) {
        $setting = Setting::where('name', $name)->firstOrFail();
        return $setting->value;
    }

}
