@extends('_layout')

@section('content')

<div class="banner ltr top" id="home" style="background-image:url('{{ $banners->get(0)->image('original') }}');">
    <div class="container text-center wow fadeInUp">
        <h2>{!! $banners->get(0)->heading !!}</h2>
        <h4>{{ $banners->get(0)->subheading }}</h4>
        @if($banners->get(0)->button_url)<a href="{{ $banners->get(0)->button_url }}" class="btn btn-lg btn-default">{{ $banners->get(0)->button_text }}</a>@endif
    </div>
</div>

<div class="container text-center">

    @if(!empty($vans))

        <h2 class="heading">{{ ucwords(numberToWords(count($vans))) }} Unique Personalities</h2>

        <div class="slider-filter">
            <a href="#" data-type="all" class="active">All Vans</a><a
                    href="#" data-type="splitscreen">Splitscreen</a><a
                    href="#" data-type="bay">Bay</a><a
                    href="#" data-type="california">California</a><a
                    href="#" data-type="motorhome">Motorhome</a>
        </div>

        <div id="home-slider">
        @foreach($vans as $van)

            <div data-type="{{ $van->customValue('type','') }}" data-color="{{ $van->customValue('color','000000') }}">
                @if ( $van->hasImageFromTag('front-med'))
                    <a href="{{ route('van.show', $van->slug) }}"><img src="{{$van->imageFromTag('front-med')->fullPath}}" alt="{{ $van->name }}" class="img-responsive" /></a>
                @else
                <a href="{{ route('van.show', $van->slug) }}"><img src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_front_med.jpg" alt="{{ $van->name }}" class="img-responsive" /></a>
                @endif
                <h3>{{ $van->name }}</h3>
                <h6>{{ $van->maxGuests }} Berth &bull; {{ $van->customValue('type','') }}</h6>
                <p>{{ $van->summary }}</p>
            </div>

        @endforeach
        </div>

    @endif

</div>

<div class="banner ltr" style="background-image:url('{{ $banners->get(1)->image('original') }}');">
    <div class="container text-center wow fadeInLeft">
        <h2>{{ $banners->get(1)->heading }}</h2>
        <h4>{{ $banners->get(1)->subheading }}</h4>
        @if($banners->get(1)->button_url)<a href="{{ $banners->get(1)->button_url }}" class="btn btn-lg btn-default">{{ $banners->get(1)->button_text }}</a>@endif
    </div>
</div>

@if(!$blogArticles->isEmpty())
<div class="container text-center">

    <h2 class="heading">What others did with theirs...</h2>

    <div class="row">

        @foreach($blogArticles as $article)
        <a href="{{ route('blog.show', $article->slug) }}" class="col-sm-6 col-md-4 grid-box">
            <div class="img-container">
                <img class="img-responsive" src="{{ $article->image('blog') }}" alt="{{ $article->title }}" />
            </div>
            <h3>{{ $article->title }}</h3>
            <h6>From {{ $article->author }}</h6>
            {!! $article->summary !!}
        </a>
        @endforeach

    </div>

</div>
@endif

<div class="banner ltr" style="background-image:url('{{ $banners->get(2)->image('original') }}');">
    <div class="container text-center">

        <div class="banner-overlay"></div>

        <div class="row wow fadeInRight">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <h2>{{ $banners->get(2)->heading }}</h2>
                <h4>{{ $banners->get(2)->subheading }}</h4>
                @if($banners->get(2)->button_url)<a href="{{ $banners->get(2)->button_url }}" class="btn btn-lg btn-default">{{ $banners->get(2)->button_text }}</a>@endif
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

    var homeSlider = $('#home-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        swipeToSlide: true,
        dots: true,
        infinite: false,
        customPaging: function(slider, i) {
            var color = slider.$slides[i].dataset.color;
            return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0" style="color:#'+color+';">' + (i + 1) + '</button>';
        },

        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    arrows: false
                }
            }
        ]
    });

    $('a', '.slider-filter').on('click', function(e){
        e.preventDefault();
        $('a', '.slider-filter').removeClass('active');
        $(this).addClass('active');

        var type = $(this).data('type');
        if (type == 'all') {
            $(homeSlider).slick('slickUnfilter');
        } else {
            $(homeSlider).slick('slickUnfilter');
            $(homeSlider).slick('slickFilter','[data-type="'+type+'"]');
        }
    });
});
</script>
@endsection