<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>O'Connors Campers Admin</title>

    <meta name="description" content="O'Connors Campers Admin">
    <meta name="keywords" content="O'Connors Campers Admin">
    <meta name="author" content="Pulse8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="/assets/favicons/favicon-32x32.png" />

    <title>Admin Login</title>

    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d2fea0fc-09d5-4d6a-b2da-67488e412f28.css"/>
    <link rel="stylesheet" href="/assets/stylesheets/admin.css">

    <style type="text/css">
        .form-signin, .alert {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin .checkbox {
            font-weight: normal;
        }
        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::route('admin.dashboard') }}"><img src="/assets/images/oconnors-campers-logo.png" height="20" /></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="{{ URL::route('admin.dashboard') }}">Admin</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    @if (count($errors) > 0)
        <br>
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!!  Form::open(array( 'route'=>'admin.login', 'method'=>'post', 'class'=>'form-signin' )) !!}

    <h3>Please sign in</h3>
    <input name="email" type="email" id="inputEmail" value="{{Input::old('email')}}" class="form-control" placeholder="Email address" required autofocus>
    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

    {!!  Form::close() !!}

</div> <!-- /container -->

</body>

</html>