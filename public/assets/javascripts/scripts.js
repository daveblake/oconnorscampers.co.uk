// Activate wow.js for animation reveals
new WOW({
    offset: 60
}).init();

$(function() {

    // include CSRF token in all AJAX requests
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Smooth scroll all anchor links
    $('a[href*=#]:not([href=#])').not('a[data-toggle="tab"]').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                console.log($('#header-navbar').outerHeight(true));
                $('html,body').animate({
                    scrollTop: target.offset().top - ($('#header-navbar').outerHeight(true) + 50)
                }, 1000);
                return false;
            }
        }
    });

    // Affix columns
    $('.affix').affix({
        offset: {
            top: 0,
            bottom: function () {
                return (this.bottom = $('#footer').outerHeight(true))
            }
        }
    });

    // Tooltips
    $('[data-toggle="tooltip"]').tooltip();

    $('.forgotPasswordTrigger').click(function(){

       $('.forgotPassword').slideToggle();
    });

});