@extends('_layout')

@section('content')

<div class="container text-center">

    <div class="row">

        <div class="col-md-offset-2 col-md-8 spacer">

            {!! Form::open(array( 'route'=>'login', 'method'=>'post' )) !!}

            <h3>Sign in to your O'connors Campers account</h3>

            @include('layout.partials.errors')

            <div class="row">
                <div class="col-md-6">
                    <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                </div>
                <div class="col-md-6">
                    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                </div>
            </div>

            <br><button class="btn btn-lg btn-primary" type="submit">Sign in</button>

            {!! Form::close() !!}

            <div class="spacer"></div>

            <a href="{{ route('password.request') }}">Forgotten your password?</a>

        </div>

    </div>

</div>

@endsection