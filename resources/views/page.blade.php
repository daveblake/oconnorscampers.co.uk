@extends('_layout')

@section('seo')

<meta name="description" content="{{ $page->seo_description }}">
<title>{{ $page->seo_title }}</title>

<meta name="twitter:title" content="{{ $page->seo_title }}">
<meta name="twitter:description" content="{{ $page->seo_description }}">
<meta name="twitter:image" content="">

<meta property="og:title" content="{{ $page->seo_title }}" />
<meta property="og:description" content="{{ $page->seo_description }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:image" content="" />

@endsection

@section('content')

<div class="banner ltr top" id="interior">
    <div class="container text-center wow fadeInUp">
        <h2>{{ $page->heading }}</h2>
        <h4>{{ $page->subheading }}</h4>
    </div>
</div>

<div class="container text-center">

    <div class="row">

        <div class="col-md-push-2 col-md-8">

            {!! $page->content !!}

        </div>

    </div>

</div>

@endsection