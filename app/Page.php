<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Page extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'heading',
        'subheading',
        'slug',
        'content',
        'seo_title',
        'seo_description',
        'published',
        'order',
    ];


    /**
     * Query scopes
     */

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

}
