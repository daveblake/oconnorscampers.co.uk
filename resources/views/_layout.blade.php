<!doctype html>
<html lang="en">
@include('layout.head')
<body>
@include('layout.partials.browsehappy')

@include('layout.header')

@yield('content')

@include('layout.footer')

@include('layout.partials.preview')

@include('layout.scripts')
@yield('scripts')
</body>
</html>