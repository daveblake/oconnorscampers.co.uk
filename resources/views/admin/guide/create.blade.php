@extends('admin._layout')

@section('content')

<h1 class="page-header">Create Guide</h1>

@include('layout.partials.errors')

{!! Form::model($guide, array('route' => 'admin.guide.store', 'method' => 'POST', 'files' => true)) !!}

    <div class="form-group">
        {!! Form::label('published', 'Published?') !!}
        {!! Form::checkbox('published', '1') !!}
    </div>

    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('slug', 'Slug (URL Name)') !!}
        <div class="input-group">
            <div class="input-group-addon">/travel-guides/</div>
            {!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('location', 'Location') !!}
        {!! Form::text('location', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_title', 'SEO Title') !!}
        {!! Form::text('seo_title', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_description', 'SEO Description') !!}
        {!! Form::text('seo_description', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('summary', 'Summary') !!}
        {!! Form::textarea('summary', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('content', 'Content') !!}
        {!! Form::textarea('content', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('image', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image_alt', 'Image alt description') !!}
        {!! Form::text('image_alt', null, array('class' => 'form-control')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function() {
    $('#slug').slugify('#title');
});
</script>
@stop