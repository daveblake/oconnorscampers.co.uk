<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>O'Connors Campers Admin</title>

    <meta name="description" content="O'Connors Campers Admin">
    <meta name="keywords" content="O'Connors Campers Admin">
    <meta name="author" content="Pulse8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="/assets/favicons/favicon-32x32.png" />

    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/d2fea0fc-09d5-4d6a-b2da-67488e412f28.css"/>
    <link rel="stylesheet" href="/assets/stylesheets/admin.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/javascripts/jquery-1.11.3.min.js"><\/script>')</script>
    <script src="/assets/javascripts/jquery-ui-1.11.4.min.js"></script>
    <script src="/assets/javascripts/ckeditor/ckeditor.js"></script>
    <script src="/assets/javascripts/ckfinder/ckfinder.js"></script>
    <script>
        CKFinder.setupCKEditor();
    </script>
    <script src="/assets/javascripts/bootstrap.min.js"></script>
    <script src="/assets/javascripts/speakingurl.js"></script>
    <script src="/assets/javascripts/slugify.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div id="alerts"></div>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('admin.dashboard') }}"><img src="/assets/images/oconnors-campers-logo.png" height="20" /></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="{{ route('admin.dashboard') }}">Admin</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('admin.dashboard') }}"><span class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }}</a></li>
                <li><a href="{{ route('admin.logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="{{ active_class(if_route(['admin.dashboard'])) }}">
                    <a href="{{ route('admin.dashboard') }}"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a>
                </li>
                <li class="{{ active_class(if_route(['admin.setting.index', 'admin.setting.edit'])) }}">
                    <a href="{{ route('admin.setting.index') }}"><span class="glyphicon glyphicon-cog"></span> Settings</a>
                </li>
                <li class="{{ active_class(if_route(['admin.enquiry.index', 'admin.enquiry.show'])) }}">
                    <a href="{{ route('admin.enquiry.index') }}"><span class="glyphicon glyphicon-inbox"></span> Enquiries</a>
                </li>
                <li class="{{ active_class(if_route(['admin.banner.index', 'admin.banner.create', 'admin.banner.edit'])) }}">
                    <a href="{{ route('admin.banner.index') }}"><span class="glyphicon glyphicon-certificate"></span> Home Banners</a>
                </li>
                <li class="{{ active_class(if_route(['admin.page.index', 'admin.page.create', 'admin.page.edit'])) }}">
                    <a href="{{ route('admin.page.index') }}"><span class="glyphicon glyphicon-file"></span> Pages</a>
                </li>
                <li class="{{ active_class(if_route(['admin.image.index', 'admin.image.create', 'admin.image.edit'])) }}">
                    <a href="{{ route('admin.image.index') }}"><span class="glyphicon glyphicon-picture"></span> Gallery Images</a>
                </li>
                <li class="{{ active_class(if_route(['admin.guide.index', 'admin.guide.create', 'admin.guide.edit'])) }}">
                    <a href="{{ route('admin.guide.index') }}"><span class="glyphicon glyphicon-globe"></span> Travel Guides</a>
                </li>
                <li class="{{ active_class(if_route(['admin.blog_article.index', 'admin.blog_article.create', 'admin.blog_article.edit'])) }}">
                    <a href="{{ route('admin.blog_article.index') }}"><span class="glyphicon glyphicon-th-list"></span> Blog Articles</a>
                </li>
                <li class="{{ active_class(if_route(['admin.blog_category.index', 'admin.blog_category.create', 'admin.blog_category.edit'])) }}">
                    <a href="{{ route('admin.blog_category.index') }}"><span class="glyphicon glyphicon-th-large"></span> Blog Categories</a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            @yield('content')
        </div>
    </div>
</div>

<div id="alerts"></div>

<script type="text/javascript">
    $(document).ready(function() {

        // include CSRF token in all AJAX requests
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // confirm all delete forms
        $('.delete-form').submit(function(){
            return confirm('Confirm delete');
        });

        // jquery ui sortable
        $(".order-sortable").sortable({
            axis: "y",
            cursor: "ns-resize",
            handle: ".order-handle",
            opacity: 0.8,
            placeholder: "order-placeholder",
            revert: 200,

            // helper to fix row width
            helper: function(e, tr)
            {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function(index)
                {
                    $(this).width($originals.eq(index).outerWidth())
                });
                return $helper;
            },

            // fix placeholder row height
            start: function(e, ui ){
                ui.placeholder.height(ui.helper.outerHeight());
            },

            // when ordering is changed
            update: function (e, ui)
            {
                var order = $(this).sortable('toArray'); // array of ids
                var model = $(this).data('model'); // snake case of model name

                // ajax post to admin controller
                $.ajax({
                    data: { order: order },
                    type: "POST",
                    url: "/admin/order/"+model
                })
                .done(function() {
                    $('<div class="alert alert-success"><span class="glyphicon glyphicon-ok-sign"></span> Order saved.</div>')
                            .appendTo('#alerts')
                            .hide().fadeIn(500).delay(1500).fadeOut(1000, function(){ $(this).remove() });
                })
                .fail(function() {
                    $('<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Reordering failed.</div>')
                            .appendTo('#alerts')
                            .hide().fadeIn(500).delay(1500).fadeOut(1000, function(){ $(this).remove() });
                });
            }
        });

    });
</script>

</body>
</html>