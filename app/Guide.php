<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guide extends Model {

	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $fillable = [
		'title',
		'slug',
		'location',
		'published',
		'summary',
		'content',
		'seo_title',
		'seo_description',
		'image_path',
		'image_alt',
	];

	public function guideSections()
	{
		return $this->hasMany('App\GuideSection');
	}

	public function image($template = 'original') {
		if ($this->image_path) {
			return '/'.config('imagecache.route').'/'.$template.'/'.$this->image_path;
		}

		return '/assets/images/placeholder.jpg';
	}

}