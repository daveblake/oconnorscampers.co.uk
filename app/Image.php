<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{

    protected $fillable = [
        'title',
        'description',
        'image_path',
        'display',
        'order',
    ];


    /**
     * Query scopes
     */

    public function scopeDisplay($query)
    {
        return $query->where('display', 1);
    }


    public function image($template = 'original') {
        if ($this->image_path) {
            return '/'.config('imagecache.route').'/'.$template.'/'.$this->image_path;
        }

        return '/assets/images/placeholder.jpg';
    }

}
