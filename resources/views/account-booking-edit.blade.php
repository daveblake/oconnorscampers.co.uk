<?php
/**
 * @var \FullyBooked\Client\Booking\Response\BookingResponse $booking
 */

$customer = $booking->customer;
?>
@extends('_layout')

@section('content')

  <form action="{{ route('account.booking.update', ['ref' => $booking->ref]) }}"
        method="post" id="booking-form">
    <div class="container">

      <h2 class="text-center heading">Your Booking / {{ $booking->ref }}</h2>

      @include('flash::message')

      <h4>Update Your Booking Details</h4>

      {{ csrf_field() }}
      <input type="hidden" name="booking_uuid" value="{{ $booking->uuid }}"/>

      <div class="row">

        <div class="col-md-6">

          <h5>Booking Info</h5>

          <div class="row">
            <div class="col-md-12 form-group">
              <h5>Payments</h5>

              <table id="payments-table"
                     class="table table-condensed table-striped">
                <tr>
                  <td>Base Price</td>
                  <td>&pound;<span id="price-booking"
                                   data-price="{{ $booking->basePrice }}">{{$booking->basePrice}}</span>
                  </td>
                </tr>
                <tr>
                  <td>Extras</td>
                  <td>&pound;<span id="price-extras"
                                   data-base="{{ $extraBasePrice }}"
                                   data-extras="{{ $booking->extrasPrice }}">{{$booking->extrasPrice}}</span>
                  </td>
                </tr>
                <tr>
                  <th>Total</th>
                  <th>&pound;<span id="price-total"
                                   data-total="{{ $booking->totalPrice }}">{{$booking->totalPrice}}</span>
                    <br>
                    <small>including VAT at 20%</small>
                  </th>
                </tr>
                <tr>
                  <td>Total Paid</td>
                  <td>&pound;<span id="price-paid"
                                   data-paid="{{ $booking->totalPaid }}">{{$booking->totalPaid}}</span>
                  </td>
                </tr>
                <tr>
                  <th>Balance</th>
                  <th>&pound;<span id="price-balance"
                                   data-balance="{{ $booking->balance }}">{{$booking->balance}}</span>
                  </th>
                </tr>
              </table>

              @if ( count($booking->payments) == 0 )
                <p>You've not made any payments yet.</p>
              @else
                <table class="table table-striped table-hover">
                  <thead>
                  <tr>
                    <th>Amount</th>
                    <th>Source</th>
                    <th>Reference</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($booking->payments as $payment)
                    <tr>
                      <td>&pound;{{ $payment->amount }}</td>
                      <td>{{ $payment->paymentSource }}</td>
                      <td>{{ $payment->transactionReference }}</td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              @endif
            </div>
          </div>

          <?php /*
          <br>

          <div class="row">
            <div class="col-md-6">
              {!! Form::label('arrival_time', 'Arrival Time') !!}
              {!! Form::select('arrival_time',
              [
              '14:00' => '2:00pm',
              '14:30' => '2:30pm',
              '15:00' => '3:00pm',
              '15:30' => '3:30pm',
              '16:00' => '4:00pm',
              ],
              $booking->arrivalTime, ['class' => 'form-control', 'placeholder' => 'Please Select' ]) !!}
            </div>
          </div> */ ?>

          <h5>Guests / Drivers</h5>

          <p>{{$booking->property->name}}
            sleeps {{$booking->property->maxGuests}} {{str_plural('guest',$booking->property->maxGuests )}}</p>

          <div id="driver-warning"
               class="alert alert-danger" <?= $booking->hasDriver ? 'style="display: none;"' : ''; ?>>
            Please add at least one driver
          </div>

          <table id="guest-table" class="table table-condensed" <?= count(
              $booking->guests
              ) == 0 ? 'style="display: none;"' : ''; ?>>
            <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>&nbsp;</th>
              <th width="25%">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $booking->guests as $k => $guest )
              <tr data-guest="{{$k}}">
                <td>
                  {{$guest->name}}

                  <input type="hidden" name="driver[{{$k}}][uuid]" value="{{$guest->uuid}}">
                  <input type="hidden" name="driver[{{$k}}][name]" value="{{$guest->name}}">
                  <input type="hidden" name="driver[{{$k}}][type]" value="{{$guest->type}}">
                  @foreach ( $guest->customValues as $field => $val )
                    <?php
                    $suffix = '';
                    if(in_array(substr($field, 0, 12), ['offence_date', 'offence_code']))
                    {
                    list($field, $suffix) = explode('-', $field);
                    $suffix = "[$suffix]";
                    } ?>
                    <input type="hidden" name="driver[<?= e($k); ?>][<?= e($field); ?>]<?= $suffix; ?>"
                           value="<?= e($val); ?>">
                  @endforeach
                </td>
                <td>{{ ucwords($guest->type) }}</td>
                <td><a class="guest-edit" href="#">Edit</a></td>
                <td><a class="guest-delete" href="#">Delete</a></td>
              </tr>
            @endforeach
            </tbody>
          </table>

          <div class="no-guests alert alert-danger" <?= count($booking->guests) ? 'style="display: none;"' : ''; ?>>
            Add a guest to your booking.
          </div>

          <a href="#" class="guest-add btn btn-md btn-primary">Add Guest / Driver</a>

          <div class="insuranceWrapper">

            <h5>Insurance</h5>

            @if ( $allowDriverCWD )
              <table class="table options">
                @foreach($collisionDamageWaivers as $optionalExtra)
                  <tr>
                    <td width="70%">
                      {{ $optionalExtra->name }}
                      @if($optionalExtra->description) <span
                          class="glyphicon glyphicon-info-sign"
                          data-toggle="tooltip" data-placement="right"
                          title="{{ $optionalExtra->description }}"></span>@endif
                      <input
                          name="optional_extras[{{ $optionalExtra->uuid }}][name]"
                          type="hidden" value="{{ $optionalExtra->name }}">
                    </td>

                    <td class="price">
                      @if($optionalExtra->amount > 0)
                      &pound;{{ number_format($optionalExtra->amount, 2) }}
                      <input
                          name="optional_extras[{{ $optionalExtra->uuid }}][price]"
                          type="hidden"
                          value="{{ number_format($optionalExtra->amount, 2) }}">
                      @else
                        FREE
                        <input
                            name="optional_extras[{{ $optionalExtra->uuid }}][price]"
                            type="hidden" value="0">
                      @endif
                    </td>

                    <td class="input">{!! Form::radio('optional_extras['.$optionalExtra->uuid.'][qty]', '1', old('optional_extras['.$optionalExtra->uuid.'][qty]', $optionalExtra->defaultValue), ['id' => 'optional-extra-'.$optionalExtra->uuid, 'class' => 'optional-extra '.(in_array('cdw', $optionalExtra->tags) ? 'cdw' : ''), 'data-uuid' => $optionalExtra->uuid]) !!}</td>

                  </tr>
                @endforeach
              </table>
            @else
              @if ( $ageOptionalExtra )
                <p>{{$ageOptionalExtra->description}}</p>
              @endif
            @endif

            @if ( count($insuranceLoadings) )
              <h5>Insurance Loadings</h5>
              <table class="table options">
                @foreach ( $insuranceLoadings as $insuranceLoading )
                  <tr>
                    <td width="70%">
                      {{ $insuranceLoading->item }}
                    </td>

                    <td class="price">
                      @if($insuranceLoading->pricePerItem > 0)
                      &pound;{{ number_format($insuranceLoading->pricePerItem, 2) }}
                      @else
                        FREE
                      @endif
                    </td>

                    <td class="qty">x {{$insuranceLoading->quantity}}</td>

                  </tr>
                @endforeach
              </table>
            @endif


          </div>
        </div>

        <div class="col-md-6">
          @if(!empty($optionalExtras))
            <section>

              <h5>Optional Extras</h5>

              <table class="table options">
                @foreach($optionalExtras as $optionalExtra)
                  <tr>
                    <td>
                      {{ $optionalExtra->name }}
                      @if($optionalExtra->description) <span
                          class="glyphicon glyphicon-info-sign"
                          data-toggle="tooltip" data-placement="right"
                          title="{{ $optionalExtra->description }}"></span>@endif
                      <input
                          name="optional_extras[{{ $optionalExtra->uuid }}][name]"
                          type="hidden" value="{{ $optionalExtra->name }}">
                    </td>

                    <td class="price">
                      @if($optionalExtra->amount > 0)
                      &pound;{{ number_format($optionalExtra->amount, 2) }}
                      <input
                          name="optional_extras[{{ $optionalExtra->uuid }}][price]"
                          type="hidden"
                          value="{{ number_format($optionalExtra->amount, 2) }}">
                      @else
                        FREE
                        <input
                            name="optional_extras[{{ $optionalExtra->uuid }}][price]"
                            type="hidden" value="0">
                      @endif
                    </td>

                    @if($optionalExtra->type == 'quantity')
                      <td class="add">QTY</td>
                      <td class="input">{!! Form::number('optional_extras['.$optionalExtra->uuid.'][qty]', old('optional_extras['.$optionalExtra->uuid.'][qty]', $optionalExtra->defaultValue), ['min' => 0, 'class' => 'form-control optional-extra', 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                    @else
                      <td class="add">ADD</td>
                      <td class="input">{!! Form::checkbox('optional_extras['.$optionalExtra->uuid.'][qty]', '1', old('optional_extras['.$optionalExtra->uuid.'][qty]', $optionalExtra->defaultValue), ['class' => 'optional-extra', 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                    @endif
                  </tr>
                @endforeach
              </table>

            </section>
          @endif

          <?php



          /*
        @if(!empty($insuranceOptions))
          <section>

            <h5>Insurance</h5>

            <table class="table options">
              @foreach($insuranceOptions as $optionalExtra)
                <tr>
                  <td>
                    {{ $optionalExtra->name }}
                    @if($optionalExtra->description) <span
                        class="glyphicon glyphicon-info-sign"
                        data-toggle="tooltip" data-placement="right"
                        title="{{ $optionalExtra->description }}"></span>@endif
                    <input
                        name="optional_extras[{{ $optionalExtra->uuid }}][name]"
                        type="hidden" value="{{ $optionalExtra->name }}">
                  </td>

                  <td class="price">
                    @if($optionalExtra->amount > 0)
                    &pound;{{ number_format($optionalExtra->amount, 2) }}
                    <input
                        name="optional_extras[{{ $optionalExtra->uuid }}][price]"
                        type="hidden"
                        value="{{ number_format($optionalExtra->amount, 2) }}">
                    @else
                      FREE
                      <input
                          name="optional_extras[{{ $optionalExtra->uuid }}][price]"
                          type="hidden" value="0">
                    @endif
                  </td>

                  @if($optionalExtra->type == 'quantity')
                    <td class="add">QTY</td>
                    <td class="input">{!! Form::number('optional_extras['.$optionalExtra->uuid.'][qty]', old('optional_extras['.$optionalExtra->uuid.'][qty]', $optionalExtra->defaultValue), ['min' => 0, 'class' => 'form-control optional-extra', 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                  @else
                    <td class="add"><label
                          for="optional-extra-{{ $optionalExtra->uuid }}">ADD</label>
                    </td>
                    <td class="input">{!! Form::checkbox('optional_extras['.$optionalExtra->uuid.'][qty]', '1', old('optional_extras['.$optionalExtra->uuid.'][qty]', $optionalExtra->defaultValue), ['id' => 'optional-extra-'.$optionalExtra->uuid, 'class' => 'optional-extra '.(in_array('cdw', $optionalExtra->tags) ? 'cdw' : ''), 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                  @endif
                </tr>
              @endforeach
            </table>

          </section>
        @endif
*/ ?>
        </div>
      </div>

      @include('layout.partials.errors')

    </div>

    <div class="container text-center">

      <a href="{{ route('account') }}"
         class="btn btn-lg btn-default spacer pull-left">Back</a>
      <input type="submit" class="btn btn-lg btn-primary spacer pull-right"
             value="Update">

    </div>
  </form>

  <div id="modal-guest" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="" method="post">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                  aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Guest / Driver</h4>
          </div>
          <div class="modal-body">
            <div class="row">

              <div class="col-md-6 form-group">
                {!! Form::label('guest-name', 'Name', ['class'=>'control-label']) !!}
                {!! Form::text('guest-name', '', array('class' => 'form-control')) !!}
              </div>

              <div class="col-md-6 form-group">
                {!! Form::label('guest-type', 'Guest Type', ['class'=>'control-label']) !!}

                {!! Form::select('guest-type', array('adult' => 'Adult', 'child' => 'Child', 'driver'=>'Driver'), null,
                array('class'=>'form-control' )) !!}

              </div>


              <div id="dobWrap" class="col-md-6 form-group" style="display: none;">
                {!! Form::label('guest-dob', 'Date of Birth', ['class'=>'control-label']) !!}
                {!! Form::text('guest-dob', null, ['class' => 'form-control datepicker', 'placeholder' =>'dd/mm/yyyy']) !!}
              </div>

            </div>

            <div class="driver-details" style="display:none;">


              <div class="row">


                <div class="col-md-6 form-group">
                  {!! Form::label('guest-occupation', 'Occupation', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-occupation', null, array('class' => 'form-control')) !!}
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-licence_number', 'Driving License Number', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-licence_number', null, array('class' => 'form-control')) !!}
                </div>


                <div class="col-md-6 form-group">
                  {!! Form::label('guest-licence_country', 'Country of Issue', ['class'=>'control-label']) !!}
                  {!! Form::select('guest-licence_country', [''=>'Please Select']+$countries, null, ['class' => 'form-control']) !!}
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-pass_date', 'Pass Date', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-pass_date', null, ['class' => 'form-control datepicker', 'placeholder' =>'dd/mm/yyyy']) !!}
                </div>
              </div>


              <div class="row">
              </div>

              <div class="row">

                <div id="addressTypeWrap" class="col-md-12 form-group">
                  {!! Form::label('guest-address_type', 'Driving Licence Address?', ['class'=>'control-label']) !!}
                  <div id="guest-address_type">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-address_type', 'account', isset($guest->customValues->address_type) ? $guest->customValues->address_type == 'account' : null, array('id' => 'guest-address_type_account')) !!}
                      <label
                          for="guest-address_type_account">Use account address</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-address_type', 'custom', isset($guest->customValues->address_type) ? $guest->customValues->address_type == 'custom' : null, array('id' => 'guest-address_type_custom')) !!}
                      <label
                          for="guest-address_type_custom">Enter custom address</label>
                    </div>
                  </div>
                </div>
              </div>

              <div id="accountAddressWrap" class="row" style="display: none;">
                <div class="col-md-6 form-group">
                  {{ $customer->address1 }}
                  @if ( $customer->address2), {{ $customer->address2 }}<br>@endif
                  {{ $customer->town }}<br>
                  {{ $customer->county }}<br>
                  {{ $customer->postcode }}<br>
                </div>

              </div>

              <div id="customAddressWrap" class="row" style="display: none;">
                <div class="col-md-6 form-group">
                  {!! Form::label('guest-address1', 'Address 1', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-address1', null, array('class' => 'form-control')) !!}
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-address2', 'Address 2', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-address2', null, array('class' => 'form-control')) !!}
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-town', 'Town', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-town', null, ['class' => 'form-control']) !!}
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-county', 'County', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-county', null, ['class' => 'form-control']) !!}
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-postcode', 'Postcode', ['class'=>'control-label']) !!}
                  {!! Form::text('guest-postcode', null, ['class' => 'form-control']) !!}
                </div>
              </div>

              <div class="row">

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-full_licence', 'Full Licence?', ['class'=>'control-label']) !!}
                  <div id="guest-full_licence">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-full_licence', 'Yes', isset($guest->customValues->full_licence) ? $guest->customValues->full_licence : null, array('id' => 'guest-full_licence-yes')) !!}
                      <label
                          for="guest-full_licence-yes">Yes</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-full_licence', 'No', isset($guest->customValues->full_licence) ? $guest->customValues->full_licence : null, array('id' => 'guest-full_licence-no')) !!}
                      <label
                          for="guest-full_licence-no">No</label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-defects', 'Do you suffer from any physical or mental conditions?', ['class'=>'control-label']) !!}
                  <div id="guest-defects">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-defects', 'Yes', isset($guest->customValues->defects) ? $guest->customValues->defects : null, array('id' => 'guest-defects-yes')) !!}
                      <label
                          for="guest-defects-yes">Yes</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-defects', 'No', isset($guest->customValues->defects) ? $guest->customValues->defects : null, array('id' => 'guest-defects-no')) !!}
                      <label
                          for="guest-defects-no">No</label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-accidents', 'Any motoring accidents within the last 3 years?', ['class'=>'control-label']) !!}
                  <div id="guest-accidents">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-accidents', 'Yes', isset($guest->customValues->accidents) ? $guest->customValues->accidents : null, array('id' => 'guest-accidents-yes')) !!}
                      <label
                          for="guest-accidents-yes">Yes</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-accidents', 'No', isset($guest->customValues->accidents) ? $guest->customValues->accidents : null, array('id' => 'guest-accidents-no')) !!}
                      <label
                          for="guest-accidents-no">No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  {!! Form::label('guest-refused_insurance', 'Have you ever been refused motor insurance?', ['class'=>'control-label']) !!}
                  <div id="guest-refused_insurance">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-refused_insurance', 'Yes', isset($guest->customValues->refused_insurance) ? $guest->customValues->refused_insurance : null, array('id' => 'guest-refused_insurance-yes')) !!}
                      <label for="guest-refused_insurance-yes">Yes</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-refused_insurance', 'No', isset($guest->customValues->refused_insurance) ? $guest->customValues->refused_insurance : null, array('id' => 'guest-refused_insurance-no')) !!}
                      <label
                          for="guest-refused_insurance-no">No</label>
                    </div>
                  </div>
                </div>

                <div id="convictionsWrap" class="col-md-6 form-group">
                  {!! Form::label('guest-convictions', 'Any motoring convictions or pending prosecutions?', ['class'=>'control-label']) !!}
                  <div id="guest-convictions">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-convictions', 'Yes', isset($guest->customValues->convictions) ? $guest->customValues->convictions : null, array('id' => 'guest-convictions-yes')) !!}
                      <label for="guest-convictions-yes">Yes</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-convictions', 'No', isset($guest->customValues->convictions) ? $guest->customValues->convictions : null, array('id' => 'guest-convictions-no')) !!}
                      <label for="guest-convictions-no">No</label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  {!! Form::label('guest-student', 'Are you a student?', ['class'=>'control-label']) !!}
                  <div id="guest-student">
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-student', 'Yes', isset($guest->customValues->student) ? $guest->customValues->student : null, array('id' => 'guest-student-yes')) !!}
                      <label for="guest-student-yes">Yes</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      {!! Form::radio('guest-student', 'No', isset($guest->customValues->student) ? $guest->customValues->student : null, array('id' => 'guest-student-no')) !!}
                      <label for="guest-student-no">No</label>
                    </div>
                  </div>
                </div>
              </div>

              <div id="additionalPointsWrapper" style="display: none;">
                <div class="row">
                  <div class="col-md-12">
                    <p>
                      Please give details of convictions or pending prosecutions including conviction codes and dates
                      as shown on your licence using the table below.
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 form-group">
                    {!! Form::label('offence_code', 'Offence Code') !!}
                  </div>

                  <div class="col-xs-5 form-group">
                    {!! Form::label('offence_date', 'Date') !!}
                  </div>
                  <div class="col-xs-1 form-group">
                  </div>
                </div>
                <div class="row offenceWrapper">
                  <div class="col-xs-6 form-group">
                    {!! Form::select('guest-offence_code[0]', [''=>'Please Select']+$offenceCodes, null, ['class' => 'form-control','id'=>'guest-offence_code-0']) !!}
                  </div>

                  <div class="col-xs-5 form-group">
                    {!! Form::text('guest-offence_date[0]',null, ['class' => 'form-control datepicker', 'placeholder'=>'dd/mm/yyyy','id'=>'guest-offence_date-0']) !!}
                  </div>
                  <div class="col-xs-1 form-group">
                    <a href="#" class="remove" style="display: none;"><span
                          class="glyphicon glyphicon-trash"></span></a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <p><a id="add-offence" href="#">Add another offence</a></p>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(
        function ()
        {

          $(".datepicker").datepicker(
              {
                dateFormat: 'dd/mm/yy',
                changeYear: true,
                yearRange: "-100:+0"
              }
          );

          $('body').on(
              "focus", '.datepicker', function ()
              {
                $(this).datepicker(
                    {
                      dateFormat: 'dd/mm/yy',
                      changeYear: true,
                      yearRange: "-100:+0"
                    }
                ).focus();
              }
          ).on(
              'click', '.guest-add', function (e)
              {

                e.preventDefault();

                var modal = $('#modal-guest');
                modal.find("input[type='text'],input[type='number']").val('');
                modal.find(':checked').prop('checked', false);
                modal.find('.has-error').removeClass('has-error');
                modal.find('select').prop('selectedIndex', 0).trigger('change');
                $('.driver-details').hide();
                $('#additionalPointsWrapper').hide();
                $('#accountAddressWrap').hide();
                $('#customAddressWrap').hide();
                $('.offenceWrapper:gt(0)').remove();

                modal
                    .data('edit', false)
                    .modal('show');
              }
          );

          $('#add-offence').click(
              function (e)
              {
                e.preventDefault();
                addOffence();
              }
          );

          $('#additionalPointsWrapper').on(
              'click', '.remove', function (e)
              {
                e.preventDefault();

                var offenceCount = $('.offenceWrapper').size();
                if (offenceCount == 1)
                {
                  return;
                }

                if (offenceCount == 2)
                {
                  $('.offenceWrapper').find('.remove').fadeOut();
                }

                $(this).parents('.offenceWrapper').slideUp(
                    'slow', function ()
                    {
                      $(this).remove();
                    }
                );

              }
          );

          $('#guest-table').on(
              'click', '.guest-edit', function (e)
              {
                e.preventDefault();

                $('.offenceWrapper:gt(0)').remove();
                var offence = $('.offenceWrapper:last');
                offence.find(':input')
                    .val('')
                    .attr('id', 'guest-offence_date-0')
                    .attr('name', 'guest-offence_date[0]');

                offence.find('select')
                    .prop('selectedIndex', 0)
                    .attr('id', 'guest-offence_code-0')
                    .attr('name', 'guest-offence_code[0]');

                var offences = [];

                var inputs = $(this).parents('tr').find('td:first input');
                inputs.each(
                    function ()
                    {
                      // Remove driver[X]
                      var name = $(this).attr('name').split(']');
                      name.shift();
                      name = name.join(']');

                      var offenceId = '';

                      switch (name.substr(0, 14))
                      {
                        case '[offence_code]':
                        case '[offence_date]':
                          offenceId = name.substring(15, name.length - 1);
                          var offenceExists = false;
                          for (var i in offences)
                          {
                            if (offences[i] == offenceId)
                            {
                              offenceExists = true;
                              break;
                            }
                          }
                          if (!offenceExists)
                          {
                            offences[offences.length] = offenceId;
                          }
                          name = name.substr(1, 12);
                          break;
                        default:
                          name = name.substr(1, name.length - 2);
                      }

                      var val = $(this).val();

                      switch (name)
                      {
                        case 'full_licence':
                        case 'address_type':
                        case 'defects':
                        case 'accidents':
                        case 'refused_insurance':
                        case 'convictions':
                        case 'student':
                          if (val == '')
                          {
                            $('#guest-' + name).find(':checked').prop('checked', false);
                          }
                          else
                          {
                            $('#guest-' + name + '-' + val.toLowerCase()).prop('checked', true);
                          }

                          if (name == 'convictions')
                          {
                            if (val == 'Yes')
                            {
                              $('#additionalPointsWrapper').show();
                            }
                            else
                            {
                              $('#additionalPointsWrapper').hide();
                            }
                          }

                          if (name == 'address_type')
                          {
                            if (val == 'account')
                            {
                              $('#accountAddressWrap').show();
                            }
                            else if (val == 'custom')
                            {
                              $('#customAddressWrap').show();
                            }
                          }

                          break;
                        case 'offence_code':
                        case 'offence_date':
                          var offenceIndex;
                          for (var i in offences)
                          {
                            if (offences[i] == offenceId)
                            {
                              offenceIndex = i;
                              break;
                            }
                          }
                          var offence = $('.offenceWrapper:eq(' + offenceIndex + ')');
                          if (offence.size() == 0)
                          {
                            addOffence(true);
                          }
                          $('#guest-' + name + '-' + offenceIndex).val(val);

                          break;
                        default:
                          $('#guest-' + name).val(val);
                      }

                    }
                );

                switch ($('#guest-type').find(':checked').val())
                {
                  case 'driver':
                    $('.driver-details').show();
                    $('#dobWrap').show();
                    break;
                  case 'child':
                    $('#dobWrap').show();
                    $('.driver-details').hide();
                    break;
                  default:
                    $('#dobWrap').hide();
                    $('.driver-details').hide();

                }

                var rowId = $(this).parents('tr').data('guest');

                $('#modal-guest')
                    .data('edit', rowId)
                    .modal('show');
              }
          ).on(
              'click', '.guest-delete', function (e)
              {
                e.preventDefault();

                $(this).parent().html(
                    '<a href="#" class="guest-delete-cancel">Cancel</a> / <a class="guest-delete-confirm" href="#">Confirm</a>'
                );
              }
          )
              .on(
                  'click', '.guest-delete-cancel', function (e)
                  {
                    e.preventDefault();

                    $(this).parent().html('<a href="#" class="guest-delete">Delete</a>');
                  }
              )
              .on(
                  'click', '.guest-delete-confirm', function (e)
                  {
                    e.preventDefault();

                    var table = $(this).parents('table');
                    var rows = table.find('tbody tr').size();
                    var deleteRow = $(this).parents('tr');

                    if (rows == 1)
                    {
                      table.slideUp(
                          function ()
                          {
                            deleteRow.remove();
                          }
                      );

                      $('#driver-warning').slideDown();
                      return;
                    }
                    else
                    {
                      deleteRow.slideUp(
                          function ()
                          {
                            $(this).remove();
                          }
                      );
                    }

                    var hasDriver = false;
                    table.find('tbody tr').each(
                        function ()
                        {
                          if ($(this).find('td:eq(2)').html() == 'Driver')
                          {
                            hasDriver = true;
                          }
                        }
                    );

                    if (!hasDriver)
                    {
                      $('#driver-warning').slideDown();
                    }

                  }
              );

          $('#guest-type').change(
              function ()
              {
                var driverDetails = $(this).closest('.row').next(
                    '.driver-details'
                );

                switch ($(this).val())
                {
                  case 'driver':
                    driverDetails.slideDown();
                    $('#dobWrap').slideDown();
                  break;
                  case 'child':
                    $('#dobWrap').slideDown();
                    driverDetails.slideUp();

                    break;
                  default:

                    driverDetails.slideUp();
                    $('#dobWrap').slideUp();
                }
                if ($(this).val() == 'driver')
                {
                }
                else
                {
                }
              }
          );

          $('#convictionsWrap').find('input').change(
              function ()
              {
                var convictions = $('#convictionsWrap').find('input:checked').val() == 'Yes';
                if (convictions)
                {
                  $('#additionalPointsWrapper').slideDown();
                }
                else
                {
                  $('#additionalPointsWrapper').slideUp();
                }
              }
          );

          $('#addressTypeWrap').find('input').change(
              function ()
              {
                var addressType = $('#addressTypeWrap').find('input:checked').val();
                if (addressType == 'account')
                {
                  $('#accountAddressWrap').slideDown();
                  $('#customAddressWrap').slideUp();
                }
                else
                {
                  $('#customAddressWrap').slideDown();
                  $('#accountAddressWrap').slideUp();
                }
              }
          );

          $('.cdw', '#booking-form').change(
              function ()
              {
                $('.cdw', '#booking-form').prop('checked', false);
                $(this).prop('checked', true);
              }
          );

          $('.optional-extra').change(
              function ()
              {
                updatePricing();
              }
          );
          updatePricing();

          var modal = $('#modal-guest');
          var driverForm = modal.find('form');
          driverForm.submit(
              function (e)
              {
                e.preventDefault();

                $(this).find('.has-error').removeClass('has-error');

                var errors = [];

                var guestName = $('#guest-name').val();
                var guestDob = $('#guest-dob').val();
                var guestType = $('#guest-type').val();

                if (guestName == '')
                {
                  errors[errors.length] = {field: 'guest-name', label: 'Please enter a name'}
                }


                if ( guestType == 'driver' || guestType == 'child' )
                {
                  if (guestDob == '')
                  {
                    errors[errors.length] = {field: 'guest-dob', label: 'Please enter the date of birth'}
                  }
                }
                if (!(guestType == 'adult' || guestType == 'child' || guestType == 'driver'))
                {
                  errors[errors.length] = {field: 'guest-type', label: 'Please select a guest type'};
                }
                else if (guestType == 'driver')
                {
                  errors = errors.concat(validateDriver());
                }

                if (errors.length > 0)
                {
                  showFormErrors(driverForm, errors);
                  return;
                }

                var data = $(this).serializeArray();

                var rowId = modal.data('edit');

                var guestTable = $('#guest-table').find('tbody');

                if (rowId !== false)
                {
                  var maxRowId = rowId;
                }
                else
                {
                  var maxRowId = $(guestTable).find('tr:last').data('guest');
                  maxRowId++;
                }

                var hiddenDrivers = '';
                for (var i in data)
                {
                  //remove 'guest-'
                  var fieldName = data[i].name.substr(6);

                  var fieldSuffix = '';
                  switch (fieldName.substr(0, 12))
                  {
                    case 'offence_code':
                    case 'offence_date':
                      fieldSuffix = fieldName.substr(12);
                      fieldName = fieldName.substr(0, 12);
                      break;
                  }
                  hiddenDrivers += ' <input type="hidden" name="driver[' + maxRowId + '][' + fieldName + ']' + fieldSuffix + '" ' +
                      'value="' + data[i].value + '"> ';
                }

                var totalGuests = guestTable.find('tr').size();

                if (rowId !== false)
                {
                  var tableRow = guestTable.find("tr[data-guest='" + rowId + "']");

                  var tds = tableRow.find('td');

                  tds.eq(0).html(guestName + hiddenDrivers);
                  tds.eq(1).html(ucwords(guestType));

                }
                else
                {
                  var tableRow = $(
                      '<tr data-guest="' + totalGuests + '">' +
                      '<td>' + guestName + hiddenDrivers + '</td>' +
                      '<td>' + ucwords(guestType) + '</td>' +
                      '<td><a class="guest-edit" href="#">Edit</a></td>' +
                      '<td><a class="guest-delete" href="#">Delete</a></td>' +
                      '</tr>'
                  );
                  guestTable.append(tableRow);
                }

                $('#guest-table').slideDown();

                if (guestType == 'driver')
                {
                  $('#driver-warning').slideUp();
                }

                $('#modal-guest')
                    .data('edit', false)
                    .modal('hide');

              }
          );

        }
    );

    function addOffence(fast)
    {
      var lastOffence = $('.offenceWrapper:last');
      var offence = lastOffence.clone(true);
      offence.find('.has-error').removeClass('has-error');

      offence.find('input, select').each(
          function ()
          {
            var idParts = $(this).attr('id').split('-');
            var i = parseInt(idParts[2], 10);
            i++;
            idParts[2] = i;
            $(this).attr('id', idParts.join('-'));
            $(this).attr(
                "name", function (i, name)
                {
                  return name.replace(
                      /\[(\d+)\]/, function (match, number)
                      {
                        return "[" + (+number + 1) + "]";
                      }
                  )
                }
            );
          }
      );

      offence.find(':input').val('');
      offence.find('select').prop('selectedIndex', 0);
      if (fast !== true)
      {
        offence.hide();
      }
      lastOffence.after(offence);

      var datepicker = offence.find(".datepicker");
      datepicker.datepicker('destroy');
      if (fast !== true)
      {
        offence.slideDown();
      }

      if (fast !== true)
      {
        $('.offenceWrapper').find('.remove').show();
      }
      else
      {
        $('.offenceWrapper').find('.remove').fadeIn();
      }
    }

    function showFormErrors(form, errors)
    {
      for (i in errors)
      {
        var field = errors[i].field;
        var label = errors[i].label;

        $('#' + field).parents('.form-group').addClass('has-error');

      }
    }

    function validateDriver()
    {
      var licence_number = $('#guest-licence_number').val();
      var licence_country = $('#guest-licence_country').val();
      var pass_date = $('#guest-pass_date').val();
      var full_licence = $('#guest-full_licence').find('input:checked').val();
      var address_type = $('#guest-address_type').find('input:checked').val();
      var address1 = $('#guest-address1').val();
      var address2 = $('#guest-address2').val();
      var town = $('#guest-town').val();
      var county = $('#guest-county').val();
      var postcode = $('#guest-postcode').val();
      var defects = $('#guest-defects').find('input:checked').val();
      var accidents = $('#guest-accidents').find('input:checked').val();
      var refused_insurance = $('#guest-refused_insurance').find('input:checked').val();
      var convictions = $('#guest-convictions').find('input:checked').val();
      var student = $('#guest-student').find('input:checked').val();

      var errors = [];
      if (licence_number == '')
      {
        errors[errors.length] = {field: 'guest-licence_number', label: 'Please enter your licence number.'}
      }
      if (licence_country == '')
      {
        errors[errors.length] = {field: 'guest-licence_country', label: 'Please select your licence country.'}
      }
      if (pass_date == '')
      {
        errors[errors.length] = {field: 'guest-pass_date', label: 'Please enter your pass date.'}
      }
      if (full_licence == undefined)
      {
        errors[errors.length] = {field: 'guest-full_licence', label: 'Please complete - full licence.'}
      }
      if (address_type == undefined)
      {
        errors[errors.length] = {field: 'guest-address_type', label: 'Please complete - address type.'}
      }
      else if (address_type == 'custom')
      {
        if (address1 == '')
        {
          errors[errors.length] = {field: 'guest-address1', label: 'Please enter your address.'}
        }
        if (town == '')
        {
          errors[errors.length] = {field: 'guest-town', label: 'Please enter your town.'}
        }
        if (county == '')
        {
          errors[errors.length] = {field: 'guest-county', label: 'Please enter your county.'}
        }
        if (postcode == '')
        {
          errors[errors.length] = {field: 'guest-postcode', label: 'Please enter your postcode.'}
        }
      }
      if (defects == undefined)
      {
        errors[errors.length] = {field: 'guest-defects', label: 'Please complete - physical or mental conditions.'}
      }
      if (accidents == undefined)
      {
        errors[errors.length] = {field: 'guest-accidents', label: 'Please complete - accidents'}
      }

      if (refused_insurance == undefined)
      {
        errors[errors.length] = {field: 'guest-refused_insurance', label: 'Please complete - refused insurance'}
      }

      if (convictions == undefined)
      {
        errors[errors.length] = {field: 'guest-convictions', label: 'Please complete - convictions.'}
      }
      else if (convictions == 'Yes')
      {
        var offences = $('.offenceWrapper');
        offences.each(
            function ()
            {
              var code = $(this).find('select');
              var date = $(this).find('input');

              if (code.val() == '')
              {
                errors[errors.length] = {field: $(code).attr('id'), label: 'Please select a code'}
              }
              if (date.val() == '')
              {
                errors[errors.length] = {field: $(date).attr('id'), label: 'Please enter a date'}
              }

            }
        );
      }

      if (student == undefined)
      {
        errors[errors.length] = {field: 'guest-student', label: 'Please complete - student.'}
      }
      return errors;
    }

    function updatePricing()
    {
      var extras = $('#price-extras');
      var bookingPrice = $('#price-booking').data('price');
      var totalPaid = $('#price-paid').data('paid');
      var extrasPrice = extras.data('base');

      $('.optional-extra', '#booking-form').each(
          function ()
          {
            var uuid = ($(this).data('uuid'));
            var price = $(
                'input[name="optional_extras[' + uuid + '][price]"]'
            ).val();
            var qty = $('input[name="optional_extras[' + uuid + '][qty]"]').val();

            if (($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio' ) && $(this).prop(
                    'checked'
                ) == false)
            {
              qty = 0;
            }

            if (parseInt(qty) > 0)
            {
              extrasPrice += (price * qty);
            }
          }
      );

      extras
          .data('price', extrasPrice.toFixed(2))
          .text(extrasPrice.toFixed(2));

      var totalPrice = parseFloat(bookingPrice) + parseFloat(extrasPrice);
      var balance = parseFloat(totalPrice) - parseFloat(totalPaid);

      $('#price-total').data('price', totalPrice.toFixed(2));
      $('#price-total, #price-payment').text(totalPrice.toFixed(2));

      $('#price-balance')
          .data('balance', balance.toFixed(2))
          .text(balance.toFixed(2));

      var balanceWarning = $('#balanceWarning');
      if (balanceWarning.size() == 0)
      {
        $('#payments-table')
            .after(
                '<div style="display:none;" id="balanceWarning" class="alert alert-danger">' +
                'That change gives a negative balance.<br >' +
                'Please contact O\'Connors to make this change.' +
                '</div>'
            );
      }
      if (balance < 0)
      {
        balanceWarning.slideDown();
      }
      else
      {
        balanceWarning.slideUp();
      }

    }

    function ucwords(str)
    {
      return str.toLowerCase().replace(
          /\b[a-z]/g, function (letter)
          {
            return letter.toUpperCase();
          }
      );
    }
  </script>
@endsection