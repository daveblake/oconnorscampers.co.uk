<?php

return [

  'autoConnect' => false,

  'endpoint' => env('FULLYBOOKED_ENDPOINT', 'http://api.fullybooked.com'),

  'credentials' => [
    'grant_type'    => 'client_credentials',
    'client_id'     => 'MjAzMmYxNDg0ZmZjM2E4ZjAyMmE4ZjY0',
    'client_secret' => 'M2YxYzU4ODQxZDg2Y2NhNmE2NGM3OGQ1ZDc1NWY1',
  ],

  'customValues' => [
    'guest' => [
      'address1',
      'address2',
      'town',
      'county',
      'postcode',
      'country',
      'telephone_home',
      'telephone_mobile',
      'email_address',
      'dob',
      'licence_number',
      'uk_licence',
      'licence_country',
      'full_licence',
      'defects',
      'accidents',
      'refused_insurance',
      'convictions',
      'student',
      'points_info',
      'pass_date',
    ]
  ],

];
