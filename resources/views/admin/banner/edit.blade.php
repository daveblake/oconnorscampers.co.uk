@extends('admin._layout')

@section('content')

<h1 class="banner-header">Edit Banner <small>{!! $banner->heading !!}</small></h1>

@include('layout.partials.errors')

{!! Form::model($banner, array('route' => array('admin.banner.update', $banner->id), 'method' => 'PUT', 'files' => true)) !!}

    <div class="form-group">
        {!! Form::label('heading', 'Heading') !!}
        {!! Form::text('heading', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('subheading', 'Sub Heading') !!}
        {!! Form::text('subheading', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('button_text', 'Button Text') !!}
        {!! Form::text('button_text', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('button_url', 'Button URL') !!}
        {!! Form::text('button_url', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        @if($banner->image_path)
            <img src="{{$banner->image('small')}}" class="img-responsive" />
        @endif
        {!! Form::file('image', null, array('class' => 'form-control')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

@stop