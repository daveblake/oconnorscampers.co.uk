@extends('admin._layout')

@section('content')

<h1 class="page-header">Create Image</h1>

@include('layout.partials.errors')

{!! Form::model($image, array('route' => 'admin.image.store', 'method' => 'POST', 'files' => true)) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::text('description', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('image', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('display', 'Display?') !!}
        {!! Form::checkbox('display', '1', true, array('class' => 'checkbox')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

@stop