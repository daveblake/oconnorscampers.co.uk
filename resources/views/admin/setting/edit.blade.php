@extends('admin._layout')

@section('content')

<h1 class="page-header">Edit Setting <small>{!! $setting->name !!}</small></h1>

@include('layout.partials.errors')

{!! Form::model($setting, array('route' => array('admin.setting.update', $setting->id), 'method' => 'PUT')) !!}

    <div class="form-group">
        {!! Form::label('value', 'Value') !!}
        {!! Form::text('value', null, array('class' => 'form-control', 'required')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

@stop