@extends('admin._layout')

@section('content')

<h1 class="banner-header">Banners</h1>

@include('flash::message')

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Heading</th>
            <th>Button</th>
            <th width="30%" class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($banners as $banner)
        <tr id="{{ $banner->id }}">
            <td>
                {{ $banner->heading }}<br>
                <small>{{ $banner->subheading }}</small>
            </td>
            <td>
                {{ $banner->button_text }}<br>
                <small>{{ $banner->button_url }}</small>
            </td>
            <td class="text-right">
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.banner.edit', array( $banner->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Banner</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $banners->render() !!}

@stop