<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use FullyBooked\Client\FullyBooked;

abstract class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  protected $_api;
  protected $_session;

  public function __construct(Request $request)
  {
    $this->_session = $request->session();
  }

  protected function _setupApi()
  {

    $config      = [
      'base_url' => Config('fullybooked.endpoint') ?: FullyBooked::ENDPOINT
    ];
    $credentials = Config('fullybooked.credentials');
    $session     = $this->_session;
    $autoConnect = Config('fullybooked.autoConntect');
    $endpoint    = Config('fullybooked.endpoint') ?: FullyBooked::ENDPOINT;

    return $this->_api = new FullyBooked(
      $config,
      $credentials,
      $session,
      $autoConnect,
      $endpoint
    );
  }

  /**
   * @return FullyBooked
   */
  public function api()
  {
    if(!$this->_api)
    {
      $this->_setupApi();
    }

    return $this->_api;
  }
}
