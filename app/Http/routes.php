<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/

Route::get('/',
    ['as' => 'home', 'uses' => 'MainController@getHome']
);

Route::get('/contact/{status?}',
    ['as' => 'contact', 'uses' => 'MainController@getContact']
);
Route::post('/contact',
    ['as' => 'enquiry.send', 'uses' => 'EnquiryController@sendEnquiry']
);

Route::get('/our-vw-campervans-for-hire/{slug}',
    ['as' => 'van.show', 'uses' => 'MainController@getVan']
);
Route::get('/our-vw-campervans-for-hire',
    ['as' => 'van.index', 'uses' => 'MainController@getVans']
);

Route::get('/gallery',
    ['as' => 'gallery', 'uses' => 'MainController@getGallery']
);

Route::get('/prices-and-availability/vans',
    ['as' => 'availability.vans',  'uses' => 'AvailabilityController@getVans']
);
Route::get('/prices-and-availability/calendar',
    ['as' => 'availability.calendar',  'uses' => 'AvailabilityController@getCalendar']
);

Route::get('/booking/{slug}', ['as' => 'booking',  'uses' => 'BookingController@getForm']);
Route::post('/booking', ['as' => 'booking.send',  'uses' => 'BookingController@postForm']);
Route::get('/booking-confirmation', ['as' => 'booking.confirmation',  'uses' => 'BookingController@getConfirmation']);
Route::any('/booking-callback', ['as' => 'booking.callback',  'uses' => 'BookingController@postCallback']);

Route::get('travel-guides', array('as' => 'guide.index', 'uses' => 'GuideController@frontendList'));
Route::get('travel-guides/{slug}', array('as' => 'guide.show', 'uses' => 'GuideController@frontendDetail'));

Route::get('blog', array('as' => 'blog.index', 'uses' => 'BlogArticleController@frontendList'));
Route::get('blog/category/{slug}', array('as' => 'blog.category.show', 'uses' => 'BlogCategoryController@frontendDetail'));
Route::get('blog/{slug}', array('as' => 'blog.show', 'uses' => 'BlogArticleController@frontendDetail'));

/*
|--------------------------------------------------------------------------
| User Account Routes
|--------------------------------------------------------------------------
*/
Route::get('account', ['as' => 'account', 'uses' => 'CustomerController@getAccount']);
Route::get('account/edit', ['as' => 'account.edit', 'uses' => 'CustomerController@getCustomer']);
Route::post('account/update', ['as' => 'account.update', 'uses' => 'CustomerController@postCustomer']);
Route::get('account/booking/{ref}', ['as' => 'account.booking', 'uses' => 'CustomerController@getBooking']);
Route::get('account/booking/{ref}/edit', ['as' => 'account.booking.edit', 'uses' => 'CustomerController@getBookingEdit']);
Route::post('account/booking/{ref}/update', ['as' => 'account.booking.update', 'uses' => 'CustomerController@postBookingUpdate']);
Route::post('account/booking/{ref}/payment', ['as' => 'account.booking.payment', 'uses' => 'CustomerController@postBookingPayment']);
Route::get('account/booking/payment/confirm/{ref}', ['as' => 'account.booking.payment.confirm', 'uses' => 'CustomerController@getConfirmPayment']);
Route::get('login', ['as' => 'login', 'uses' => 'CustomerController@getLogin']);
Route::post('login', 'CustomerController@postLogin');
Route::post('password/email', ['as'=>'password.email', 'uses'=>'CustomerController@postEmailPassword']);
Route::get('password/reset/{ref?}',  ['as'=>'password.reset', 'uses'=>'CustomerController@getResetPassword']);
Route::post('password/reset', ['as'=>'password.reset', 'uses'=>'CustomerController@postResetPassword']);
Route::get('logout', ['as' => 'logout', 'uses' => 'CustomerController@getLogout']);


/*
|--------------------------------------------------------------------------
| Preview Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'preview', 'middleware' => 'admin'], function()
{
    Route::get('travel-guides/{slug}', array('as' => 'preview.guide.show', 'uses' => 'GuideController@frontendPreview'));
    Route::get('blog/{slug}', array('as' => 'preview.blog.show', 'uses' => 'BlogArticleController@frontendPreview'));
    Route::get('{slug}', array('as' => 'preview.page', 'uses' => 'PageController@showPreview'));
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::get('admin/login', ['as' => 'admin.login', 'uses' => 'Auth\AdminAuthController@getLogin']);
Route::post('admin/login', 'Auth\AdminAuthController@postLogin');
Route::get('admin/logout', ['as' => 'admin.logout', 'uses' => 'Auth\AdminAuthController@getLogout']);

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function()
{
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'AdminController@dashboard']);
    Route::resource('page', 'PageController');
    Route::resource('banner', 'BannerController');
    Route::resource('setting', 'SettingController');
    Route::resource('enquiry', 'EnquiryController');
    Route::resource('guide', 'GuideController');
    Route::resource('image', 'ImageController');
    Route::resource('blog_article', 'BlogArticleController');
    Route::resource('blog_category', 'BlogCategoryController');
});
Route::post('/admin/order/{resource}', array('as' => 'admin.order', 'uses' => 'AdminController@sortOrder'));

/*
|--------------------------------------------------------------------------
| Sitemap Route
|--------------------------------------------------------------------------
*/
Route::get('sitemap.xml', ['as' => 'sitemap', 'uses' => 'SitemapController@getSitemap']);

/*
|--------------------------------------------------------------------------
| Catch All Page Route
|--------------------------------------------------------------------------
*/
Route::get('/{slug}', array('as' => 'page', 'uses' => 'PageController@showFrontend'));