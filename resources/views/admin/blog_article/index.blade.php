@extends('admin._layout')

@section('content')

<h1 class="page-header">Blog Articles</h1>

@if (Session::has('message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('message') }}</div>
@endif

<p>
    <a class="btn btn-success" href="{{ URL::route('admin.blog_article.create') }}"><span class="glyphicon glyphicon-plus"></span> Create Blog Article</a>
    <a class="btn btn-info" href="{{ URL::route('admin.blog_category.index') }}"><span class="glyphicon glyphicon-pencil"></span> Manage Categories</a>
</p>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="100">Published?</th>
            <th>Title</th>
            <th>Category</th>
            <th>Date</th>
            <th width="30%">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($blogArticles as $blogArticle)
        <tr id="{{ $blogArticle->id }}">
            <td>{{ $blogArticle->published ? 'Yes' : 'No' }}</td>
            <td>
                {{ $blogArticle->title }}<br>
                <small>from {{ $blogArticle->author }}</small>
            </td>
            <td>{{ $blogArticle->blogCategory->name or '(None)'}}</td>
            <td>{{ $blogArticle->published_at->toFormattedDateString() }}</td>
            <td>
                <a class="btn btn-small btn-default" href="{{ URL::route('preview.blog.show', array( $blogArticle->slug )) }}" target="_blank"><span class="glyphicon glyphicon-new-window"></span> Preview</a>
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.blog_article.edit', array( $blogArticle->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Article</a>
                {!! Form::open(array('route' => array('admin.blog_article.destroy', $blogArticle->id), 'method' => 'DELETE', 'class' => 'delete-form')) !!}
                    <button type="submit" class="btn btn-small btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="text-center">{!! $blogArticles->render() !!}</div>

@stop