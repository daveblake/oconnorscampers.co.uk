<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/javascripts/jquery-1.11.3.min.js"><\/script>')</script>
<script src="/assets/javascripts/jquery-ui-1.11.4.min.js"></script>
<script src="/assets/javascripts/bootstrap.min.js"></script>
<script src="/assets/javascripts/slick.js"></script>
<script src="/assets/javascripts/lightgallery.min.js"></script>
<script src="/assets/javascripts/lightgallery-fullscreen.min.js"></script>
<script src="/assets/javascripts/lightgallery-thumbnail.min.js"></script>
<script src="/assets/javascripts/jquery.floatThead.min.js"></script>
<script src="/assets/javascripts/wow.mins.js"></script>
<script src="/assets/javascripts/scripts.js"></script>

<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-1529048-1','auto');ga('send','pageview');
</script>