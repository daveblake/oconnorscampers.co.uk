@extends('_layout')

@section('content')

    <div class="content container" style="margin-top:100px;margin-bottom:100px;">

        <h3>403 - Forbidden</h3>
        <h4>Oh dear...</h4>
        <p>You do not have the required permissions to access this page.</p>

    </div>

@endsection