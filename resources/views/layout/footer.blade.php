<footer id="footer">

    <div class="container">



        <a class="social-icon wow fadeInUp" data-wow-delay="0.5s" href="http://twitter.com/VWcamperhire" target="_blank">
            <img src="/assets/images/icons/twitter.png" alt="Twitter" height="40" />
        </a>
        <a class="social-icon wow fadeInUp" data-wow-delay="1s" href="http://www.facebook.com/pages/OConnors-Campers-VW-Campervan-Hire/121197094559112" target="_blank">
            <img src="/assets/images/icons/facebook.png" alt="Facebook" height="40" />
        </a>
        <a class="social-icon wow fadeInUp" data-wow-delay="1.5s" href="http://www.flickr.com/photos/oconnorscampers" target="_blank">
            <img src="/assets/images/icons/flickr.png" alt="Flickr" height="40" />
        </a>

        <nav id="footer-nav">
            <a href="{{ route('blog.category.show', 'press') }}">Press &amp; PR</a><a href="{{ route('page', 'campervan-journals') }}">Campervan Journals</a><a href="{{ route('page', 'terms') }}">Terms &amp; Conditions</a><a href="{{ route('page', 'cookies') }}">Cookie Policy</a><a href="{{ route('contact') }}">Contact Us</a>
        </nav>

        <p><img src="/assets/images/oconnors-campers-logo-black.png" alt="O'connors Campers" height="30" /></p>

        <p>Copyright O'Connors Campers &copy; {{ date('Y') }}</p>



        <p> O’Connors Campers, Highlands, Old Road, High Street, Okehampton. EX20 1SP <br>Registered in England and Wales no. 07804373
            </p>

        <p><a href="http://pulse8.co.uk">Web Design and Development from Pulse8</a></p>

    </div>
</footer>
