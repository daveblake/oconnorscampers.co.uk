@extends('_layout')

@section('content')

  <div class="container text-center">

    <div class="row">

      <div class="col-md-offset-2 col-md-8 spacer">

        {!! Form::open(array( 'route'=>'password.reset', 'method'=>'post' )) !!}


        <input name="token" type="hidden" id="inputToken"
               value="{{Input::old('token', $token)}}" class="form-control"
               placeholder="Token">
        
        <h3>Reset your password</h3>

        @include('layout.partials.errors')
        @include('flash::message')

        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <input name="email" type="email" id="inputEmail"
                   value="{{Input::old('email', $email)}}" class="form-control"
                   placeholder="Email address" required autofocus>
            <br>

            <input name="password" type="password" id="inputPassword"
                   class="form-control" placeholder="Password" required>

            <br>
            <input name="passwordConfirm" type="password"
                   id="inputPasswordConfirm"
                   class="form-control" placeholder="Confirm Password" required>
          </div>
        </div>


        <br>
        <button class="btn btn-lg btn-primary" type="submit">Reset Password
        </button>

        {!! Form::close() !!}

        <div class="spacer"></div>

      </div>

    </div>

  </div>

@endsection