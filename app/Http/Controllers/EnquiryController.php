<?php

namespace App\Http\Controllers;

use App\Enquiry;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use App\Setting;

class EnquiryController extends Controller
{

    public function sendEnquiry(Request $request)
    {
        $messages = array(
            'enquiry.required' => 'The message field is required.',
        );

        $this->validate($request, [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'enquiry' => 'required',
        ], $messages);

        $data = $request->all();

        Enquiry::create($data);

        Mail::send('emails.enquiry', $data, function ($m) use ($data) {
            $enquiriesEmail = Setting::getValueByName('Enquiries Email');
            $m->from($enquiriesEmail, 'O\'connors Campers');
            $m->replyTo($data['email'], $data['first_name'] . ' ' . $data['last_name']);
            $m->to($enquiriesEmail);
            $m->subject('Website Enquiry');
        });

        $status = 'sent';

        return redirect()->route('contact', $status);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enquiries = Enquiry::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.enquiry.index', compact('enquiries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $enquiry = new Enquiry();
        return view('admin.enquiry.create', compact('enquiry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'enquiry' => 'required',
        ]);

        Enquiry::create($request->all());

        flash()->success('Enquiry created.');

        return redirect()->action('EnquiryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.enquiry.show', ['enquiry' => Enquiry::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enquiry = Enquiry::findOrFail($id);

        return view('admin.enquiry.edit', compact('enquiry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $enquiry = Enquiry::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'enquiry' => 'required',
        ]);

        $enquiry->fill($request->all());
        $enquiry->save();

        flash()->success('Enquiry updated.');

        return redirect()->action('EnquiryController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enquiry = Enquiry::findOrFail($id);
        $enquiry->delete();

        flash()->success('Enquiry deleted.');

        return redirect()->action('EnquiryController@index');
    }

}
