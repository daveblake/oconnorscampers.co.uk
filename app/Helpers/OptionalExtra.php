<?php

namespace App\Helpers;

use FullyBooked\Client\OptionalExtra\Response\OptionalExtraResponse;

class OptionalExtra extends OptionalExtraResponse
{
  public $defaultValue;

  public static function createFromResponse(
    OptionalExtraResponse $response, $defaultValue
  )
  {
    $reflection    = new \ReflectionClass($response);
    $optionalExtra = new static;

    foreach($reflection->getProperties() as $property)
    {
      $optionalExtra->{$property->name} = $response->{$property->name};
    }

    $optionalExtra->defaultValue = $defaultValue;

    return $optionalExtra;
  }
}
