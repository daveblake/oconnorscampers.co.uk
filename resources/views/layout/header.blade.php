<nav id="header-navbar" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="/"><img src="/assets/images/oconnors-campers-logo.png" alt="O'connors Campers VW Hire" height="30" /></a></h1>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="{{ active_class(if_route(['van.index', 'van.show'])) }}"><a href="{{ route('van.index') }}">Our Campers</a></li>
                <li class="{{ active_class(if_route(['availability.vans', 'availability.calendar'])) }}"><a href="{{ route('availability.vans') }}">Prices &amp; Availability</a></li>
                <li class="{{ active_class(if_route(['guide.index', 'guide.show'])) }}"><a href="{{ route('guide.index') }}">Travel Guides</a></li>
                <li class="{{ active_class(if_route(['blog.index', 'blog.show'])) }}"><a href="{{ route('blog.index') }}">Blog</a></li>
                <li class="{{ active_class(if_route(['gallery'])) }}"><a href="{{ route('gallery') }}">Gallery</a></li>
                <li class="{{ active_class(if_route_param('slug', 'how-it-works')) }}"><a href="{{ route('page', 'how-it-works') }}">How It Works</a></li>
                <li class="{{ active_class(if_route(['contact'])) }}"><a href="{{ route('contact') }}">Contact Us</a></li>
                @if (Session::has('customerUuid'))
                    <li><a href="{{ route('account') }}">My Account</a></li>
                @else
                    <li class="{{ active_class(if_route(['login'])) }}"><a href="{{ route('login') }}">Login</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
