@extends('_layout')

@section('seo')

<meta name="description" content="{{ $page->seo_description }}">
<title>{{ $page->seo_title }}</title>

<meta name="twitter:title" content="{{ $page->seo_title }}">
<meta name="twitter:description" content="{{ $page->seo_description }}">
<meta name="twitter:image" content="">

<meta property="og:title" content="{{ $page->seo_title }}" />
<meta property="og:description" content="{{ $page->seo_description }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:image" content="" />

@endsection

@section('content')

<div class="banner ltr top" id="sunset">
    <div class="container text-center wow fadeInUp">
        <h2>Travel Guides</h2>
        <h4>Explore This Great Island</h4>
    </div>
</div>

<div class="container text-center">

    <div class="row">

        <div class="col-md-push-2 col-md-8">

            {!! $page->content !!}

            <p>&nbsp;</p>

        </div>

    </div>

    @if(!$guides->isEmpty())
        <div class="row">

            @foreach($guides as $guide)
                <a href="{{ route('guide.show', $guide->slug) }}" class="col-sm-6 col-md-4 grid-box">
                    <div class="img-container">
                        <img class="img-responsive" src="{{ $guide->image('small') }}" alt="{{ $guide->title }}" />
                    </div>
                    <h3>{{ $guide->title }}</h3>
                    <h6>{{ $guide->location }}</h6>
                    {!! $guide->summary !!}
                </a>
            @endforeach

        </div>

        <div class="text-center">{!! $guides->render() !!}</div>
    @endif

</div>

@endsection