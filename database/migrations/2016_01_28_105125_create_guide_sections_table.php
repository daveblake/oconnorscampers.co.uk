<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuideSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guide_sections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('guide_id');
			$table->string('heading');
			$table->string('subheading');
			$table->text('summary');
			$table->text('content');
			$table->string('image_path');
			$table->timestamps();
			$table->softDeletes();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guide_sections');
	}

}
