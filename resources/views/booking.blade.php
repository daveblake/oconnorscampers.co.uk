@extends('_layout')

@section('content')

<form id="booking-form" action="{{ route('booking.send') }}" method="post">

<div class="container">

    <div class="row">

        <div class="col-md-3">

            <div class="booking-van affix">

                <div class="booking-van-header" @if(isset($van->customValues->type))style="background-color:#{{ $van->customValues->color }};"@endif>
                    <h4>{{ $van->name }}<br>
                    <span>{{ $van->maxGuests }} Berth</span></h4>

                    @if(isset($van->customValues->type))<small>{{ $van->customValues->type }}</small>@endif
                </div>

                @if ( $van->hasImageFromTag('side-small') )
                    <img style="margin:15px auto;" class="img-responsive" src="{{$van->imageFromTag('side-small')->fullPath}}" alt="{{ $van->name }}" />
                    @else
                <img style="margin:15px auto;" class="img-responsive" src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_small.jpg" alt="{{ $van->name }}" />
                @endif

                <p>From {{ (new \DateTime(Input::get('date')))->format('l jS F Y') }}<br>
                    for {{ Input::get('nights') }} nights</p>

                <p>Collection from 2.30pm<br>
                Drop-off until 10am</p>

                <p class="price">
                    <small>booking</small> &pound;<span id="price-booking" data-price="{{ $price }}">{{ $price }}</span><br>
                    <small>extras</small> &pound;<span id="price-extras" data-price="0.00">0.00</span><br>
                    <small>total</small> &pound;<span id="price-total" data-price="{{ $price }}">{{ $price }}</span><br>
                    <small>including VAT at 20%</small>
                </p>

            </div>

        </div>

        <div class="col-md-9">

                @include('layout.partials.errors')

                {{ csrf_field() }}

                <input type="hidden" name="property_uuid" value="{{ $van->uuid }}" />
                <input type="hidden" name="cancel_url" value="{{ Request::fullUrl() }}" />
                <input type="hidden" name="nights" value="{{ Input::get('nights') }}" />
                <input type="hidden" name="date" value="{{ Input::get('date') }}" />

                <section>

                    <div class="row">

                        <div class="col-md-4">
                            <h2>Your Details</h2>
                        </div>

                        <div class="col-md-8 text-right">
                            @if ($customer->uuid)
                                <p>Logged in as: {{ $customer->forename . ' ' . $customer->surname}}</p>
                            @else
                                <p>Already registered? <a href="{{ route('login') }}" class="btn btn-primary">Login</a></p>
                            @endif
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-2">
                            <label>Title</label>
                            {!! Form::select('title',
                                [
                                    'Mr' => 'Mr',
                                    'Mrs' => 'Mrs',
                                    'Miss' => 'Miss',
                                    'Ms' => 'Ms',
                                ],
                                old('title', $customer->title), ['class' => 'form-control', 'placeholder' => '' ]) !!}
                        </div>

                        <div class="form-group col-md-5">
                            <label>First Name</label>
                            <input name="firstName" value="{{ old('firstName', $customer->forename ) }}" class="form-control" type="text" required="required">
                        </div>

                        <div class="form-group col-md-5">
                            <label>Surname</label>
                            <input name="lastName" value="{{ old('lastName', $customer->surname) }}" class="form-control" type="text" required="required">
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>Email Address</label>
                            <input name="email" value="{{ old('email',$customer->email) }}" class="form-control" type="email" required="required">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Mobile Number</label>
                            <input name="billingPhone" value="{{ old('billingPhone', $customer->telMob) }}" class="form-control" type="text" required="required">
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-4">
                            <label>Address</label>
                            <input name="billingAddress1" value="{{ old('billingAddress1', $customer->address1) }}" class="form-control" type="text" required="required">
                        </div>

                        <div class="form-group col-md-4">
                            <label>&nbsp;</label>
                            <input name="billingAddress2" value="{{ old('billingAddress2',$customer->address2) }}" class="form-control" type="text">
                        </div>

                        <div class="form-group col-md-4">
                            <label>City</label>
                            <input name="billingCity" value="{{ old('billingCity', $customer->town) }}" class="form-control" type="text" required="required">
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-4">
                            <label>County</label>
                            <input name="billingState" value="{{ old('billingState', $customer->county) }}" class="form-control" type="text" required="required">
                        </div>

                        <div class="form-group col-md-4">
                            <label>Postcode</label>
                            <input name="billingPostcode" value="{{ old('billingPostcode',$customer->postcode) }}" class="form-control" type="text" required="required">
                        </div>

                        <div class="form-group col-md-4">
                            <label>Country</label>
                            {!! Form::select('country',
                                $countries,
                                old('country', $customer->country->uuid), ['class' => 'form-control', 'placeholder' => '' ]) !!}
                        </div>

                    </div>

                    @if(!empty($heardAbouts))
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>Where did you hear about us?</label>
                            <select id="referrer" class="form-control" name="referrer" required="required">
                                <option value=""></option>
                                @foreach($heardAbouts as $heardAbout)
                                    <option value="{{ $heardAbout->uuid }}" @if($heardAbout->option == old('referrer'))selected="selected"@endif data-specify="{{ $heardAbout->pleaseSpecify }}">
                                        {{ $heardAbout->option }}@if($heardAbout->pleaseSpecify) (Please Specify)@endif
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div id="referrer-specific-container" class="form-group col-md-6">
                            <label>Please specify</label>
                            <input name="referrer_specific" id="referrer-specific" class="form-control" type="text" required="required">
                        </div>

                    </div>
                    @endif

                </section>

                <section>

                    <div class="row">

                        <div class="col-md-4">
                            <h2>Who's Travelling?</h2>
                        </div>

                        <div class="col-md-8 text-right">
                            <p>This helps us to understand whether you have the correct van</p>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>Number of Adults</label>
                            {!! Form::selectRange('adults', 1, 4, old('title'), ['class' => 'form-control' ]) !!}
                        </div>

                        <div class="form-group col-md-6">
                            <label>Number of Children Under 16</label>
                            {!! Form::selectRange('children', 0, 4, old('title'), ['id' => 'children', 'class' => 'form-control' ]) !!}
                        </div>

                    </div>

                    <div class="row">

                        @for($i = 1; $i <=4; $i++)
                        <div class="child-age form-group col-md-6">
                            <label>Age Of Child {{$i}}</label>
                            {!! Form::selectRange('child_age_'.$i, 0, 15, old('child_age_'.$i), ['id' => 'child-age-'.$i, 'class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        @endfor

                    </div>

                </section>

                @if(!empty($optionalExtras))
                <section>

                    <h2>Optional Extras</h2>

                    <table class="table options">
                        @foreach($optionalExtras as $optionalExtra)
                            <tr>
                                <td>
                                    {{ $optionalExtra->name }}
                                    @if($optionalExtra->description) <span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" title="{{ $optionalExtra->description }}"></span>@endif
                                    <input name="optional_extras[{{ $optionalExtra->uuid }}][name]" type="hidden" value="{{ $optionalExtra->name }}">
                                </td>

                                <td class="price">
                                @if($optionalExtra->amount > 0)
                                    &pound;{{ number_format($optionalExtra->amount, 2) }}
                                    <input name="optional_extras[{{ $optionalExtra->uuid }}][price]" type="hidden" value="{{ number_format($optionalExtra->amount, 2) }}">
                                @else
                                    FREE
                                    <input name="optional_extras[{{ $optionalExtra->uuid }}][price]" type="hidden" value="0">
                                @endif
                                </td>

                                @if($optionalExtra->type == 'quantity')
                                    <td class="add">QTY</td>
                                    <td class="input">{!! Form::number('optional_extras['.$optionalExtra->uuid.'][qty]', old('optional_extras['.$optionalExtra->uuid.'][qty]'), ['min' => 0, 'class' => 'form-control optional-extra', 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                                @else
                                    <td class="add">ADD</td>
                                    <td class="input">{!! Form::checkbox('optional_extras['.$optionalExtra->uuid.'][qty]', '1', old('optional_extras['.$optionalExtra->uuid.'][qty]'), ['class' => 'optional-extra', 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                                @endif
                            </tr>
                        @endforeach
                    </table>

                </section>
                @endif
<?php /*
                @if(!empty($insuranceOptions))
                <section>

                    <h2>Insurance</h2>

                    <table class="table options">
                        @foreach($insuranceOptions as $optionalExtra)
                            <tr>
                                <td>
                                    {{ $optionalExtra->name }}
                                    @if($optionalExtra->description) <span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" title="{{ $optionalExtra->description }}"></span>@endif
                                    <input name="optional_extras[{{ $optionalExtra->uuid }}][name]" type="hidden" value="{{ $optionalExtra->name }}">
                                </td>

                                <td class="price">
                                    @if($optionalExtra->amount > 0)
                                    &pound;{{ number_format($optionalExtra->amount, 2) }}
                                    <input name="optional_extras[{{ $optionalExtra->uuid }}][price]" type="hidden" value="{{ number_format($optionalExtra->amount, 2) }}">
                                    @else
                                        FREE
                                        <input name="optional_extras[{{ $optionalExtra->uuid }}][price]" type="hidden" value="0">
                                    @endif
                                </td>

                                @if($optionalExtra->type == 'quantity')
                                    <td class="add">QTY</td>
                                    <td class="input">{!! Form::number('optional_extras['.$optionalExtra->uuid.'][qty]', old('optional_extras['.$optionalExtra->uuid.'][qty]'), ['min' => 0, 'class' => 'form-control optional-extra', 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                                @else
                                    <td class="add"><label for="optional-extra-{{ $optionalExtra->uuid }}">ADD</label></td>
                                    <td class="input">{!! Form::checkbox('optional_extras['.$optionalExtra->uuid.'][qty]', '1', old('optional_extras['.$optionalExtra->uuid.'][qty]'), ['id' => 'optional-extra-'.$optionalExtra->uuid, 'class' => 'optional-extra '.(in_array('cdw', $optionalExtra->tags) ? 'cdw' : ''), 'data-uuid' => $optionalExtra->uuid]) !!}</td>
                                @endif
                            </tr>
                        @endforeach
                    </table>

                </section>
                @endif
*/ ?>
                @if (!$customer->uuid)
                <section>

                    <h2>Create an O'Connors account</h2>

                    <p>As a registered user, you will enjoy quicker checkout process, booking history and exclusive offers.</p>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>Password</label>
                            <input name="account_password" class="form-control" type="password" required="required">
                        </div>

                    </div>

                </section>
                @endif

                <div class="checkbox checkbox-success">
                    {!! Form::checkbox('newsletter', '1', old('newsletter'), ['id' => 'newsletter']) !!}
                    <label for="newsletter">Please send me offers and news from O'Connors</label> (We will not share your information with third parties)
                </div>

                <div class="checkbox checkbox-success">
                    {!! Form::checkbox('terms', '1', old('terms'), ['id' => 'terms', 'required' => 'required']) !!}
                    <label for="terms">I'm {{ $van->hasTag('motorhome') ? 23 : 21 }} or older and have read and agree with the <a href="{{ route('page', 'terms') }}" target="_blank">Terms &amp; Conditions</a></label>
                </div>

                <div id="deposit-details" class="text-right">

                    <label>Payment Amount</label>

                    <p>The non-refundable &pound;150 booking deposit secures your van. The balance of the hire fee is payable 42 days in advance of your holiday.</p>

                    <div class="row">

                        <div class="form-group col-md-12">

                            @if($depositAllowed)
                            <div class="radio radio-inline radio-primary">
                                <input type="radio" name="payment_amount" <?= old('payment_amount','full') == 'deposit' ? 'checked':''; ?> id="payment-amount-deposit" value="deposit" required="required">
                                <label for="payment-amount-deposit">
                                    Deposit (&pound;150)
                                </label>
                            </div>
                            @endif

                            <div class="radio radio-inline radio-primary">
                                <input type="radio" name="payment_amount" id="payment-amount-full" value="full" <?= old('payment_amount','full') == 'full' ? 'checked':''; ?>  required="required">
                                <label for="payment-amount-full">
                                    Full (&pound;<span id="price-payment">{{$price}}</span>)
                                </label>
                            </div>

                        </div>

                    </div>

                </div>

                <div id="confirm-booking">

                    <label>Payment Method</label>

                    <div class="row">

                        <div class="col-md-6">
                            <input class="pull-left" type="image" src="/assets/images/worldpay.png" alt="Pay with Worldpay">
                        </div>

                        <div class="col-md-6">
                            <p>Pay online now using your credit or debit card.</p>
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-12">

                            <div class="radio radio-inline radio-primary">
                                <input type="radio" name="payment_method" id="payment-method-card" value="card" checked="checked" required="required">
                                <label for="payment-method-card">
                                    Card
                                </label>
                            </div>


                            <p>If you would prefer to pay by BACS please do contact us (weekdays).<br>
                               Please note: availability can change quite quickly.<br>
                               Reservations cannot be confirmed without your deposit in place</p>
<?php /*
                            <div class="radio radio-inline radio-primary">
                                <input type="radio" name="payment_method" id="payment-method-bacs" value="bacs" required="required">
                                <label for="payment-method-bacs">
                                    BACS
                                </label>
                            </div>*/ ?>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary btn-lg">Pay Now</button>
                </div>

        </div>

    </div>

</div>

</form>

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

    $('#children').change(function() {
        var children = $(this).val();
        $('.child-age').each(function(i){
            if(i+1 <= children) {
                $(this).slideDown();
                $('select', this).prop('disabled', false);
            } else {
                $(this).slideUp();
                $('select', this).prop('disabled', true);
            }
        });
    });

    $('#referrer').change(function() {
        var referrer = $(this).val();

        if($(this).find(':selected').data('specify') == 1) {
            $('#referrer-specific-container').slideDown();
            $('#referrer-specific').prop('disabled', false);
        } else {
            $('#referrer-specific-container').slideUp();
            $('#referrer-specific').prop('disabled', true);
        }

    });

    $('.cdw', '#booking-form').change(function() {
        $('.cdw', '#booking-form').prop('checked', false);
        $(this).prop('checked', true);
    });

    $('.optional-extra').change(function() {
        updatePricing();
    });
    updatePricing();

});

function updatePricing() {
    var bookingPrice = $('#price-booking').data('price');
    var extrasPrice = 0;

    $('.optional-extra', '#booking-form').each(function() {
        var uuid = ($(this).data('uuid'));
        var price = $('input[name="optional_extras['+uuid+'][price]"]').val();
        var qty = $('input[name="optional_extras['+uuid+'][qty]"]').val();
        if ($(this).attr('type') == 'checkbox' && $(this).prop('checked') == false) qty = 0;
        if (parseInt(qty) > 0) {
            extrasPrice += (price * qty);
        }
    });

    $('#price-extras').data('price', extrasPrice.toFixed(2));
    $('#price-extras').text(extrasPrice.toFixed(2));

    var totalPrice = parseFloat(bookingPrice) + parseFloat(extrasPrice);

    $('#price-total').data('price', totalPrice.toFixed(2));
    $('#price-total, #price-payment').text(totalPrice.toFixed(2));
}
</script>
@endsection