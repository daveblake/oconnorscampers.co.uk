@extends('admin._layout')

@section('content')

<h1 class="page-header">Edit Page <small>{!! $page->heading !!}</small></h1>

@include('layout.partials.errors')

{!! Form::model($page, array('route' => array('admin.page.update', $page->id), 'method' => 'PUT')) !!}

    <div class="form-group">
        {!! Form::label('heading', 'Heading') !!}
        {!! Form::text('heading', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('subheading', 'Sub Heading') !!}
        {!! Form::text('subheading', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('slug', 'Slug (URL Name)') !!}
        {!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('content', 'Content') !!}
        {!! Form::textarea('content', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_title', 'SEO Title') !!}
        {!! Form::text('seo_title', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_description', 'SEO Description') !!}
        {!! Form::text('seo_description', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('published', 'Puiblished?') !!}
        {!! Form::checkbox('published', '1', $page->published, array('class' => 'checkbox')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

@stop