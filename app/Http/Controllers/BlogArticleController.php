<?php

namespace App\Http\Controllers;

use App\BlogArticle;
use App\BlogCategory;
use App\Page;
use FullyBooked\Client\Property\Payload\ListPropertyPayload;
use Illuminate\Http\Request;
use App\Http\Requests;

class BlogArticleController extends Controller {

	public function frontendList()
	{
		$blogArticles = BlogArticle::where('published', 1)->orderBy('published_at', 'desc')->paginate(5);
		$blogCategories = BlogCategory::orderBy('order')->get();
		$recentEntries = BlogArticle::where('published', 1)->orderBy('published_at', 'desc')->take(5)->get();

		$page = Page::where('slug', 'blog')->firstOrFail();

		return view('blog-articles', compact('blogArticles', 'blogCategories', 'recentEntries', 'page'));
	}

	public function frontendDetail($slug)
	{
		$blogArticle = BlogArticle::where('published', 1)->where('slug', $slug)->firstOrFail();
		$recentEntries = BlogArticle::where('published', 1)->orderBy('published_at', 'desc')->take(5)->get();
		$blogCategories = BlogCategory::orderBy('order')->get();

		return view('blog-article', compact('blogArticle', 'recentEntries', 'blogCategories'));
	}

	public function frontendPreview($slug)
	{
		$blogArticle = BlogArticle::where('slug', $slug)->firstOrFail();

		return view('blog-article', compact('blogArticle'));
	}

	/**
	 * Display a listing of blog articles
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$blogArticles = BlogArticle::orderBy('published_at', 'desc')->with('blogCategory')->paginate(20);

		return view('admin.blog_article.index', compact('blogArticles'));
	}

	/**
	 * Show the form for creating a new blog article
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$blogArticle = new BlogArticle();

		$response = $this->api()->propertyService()
			->listProperties(
				(new ListPropertyPayload())->setLimit(20)
			);
		$response = $response->items;

		if(!empty($response)) {
			foreach($response as $van) {
				$vans[$van->uuid] = $van->name;
			}
		}

		return view('admin.blog_article.create', compact('blogArticle', 'vans'));
	}

	/**
	 * Store a newly created blog article in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
			'title' => 'required',
			'author' => 'required',
			'published_at' => 'required',
			'slug' => 'required|alpha_dash|unique:blog_articles',
			'image' => 'image',
		]);

		$data = $request->all();
		$data['published'] = ($request->has('published')) ? 1 : 0;

		if (!$data['seo_title']) $data['seo_title'] = $data['title']." | O'Connors VW Campervan Hire";
		if (!$data['seo_description']) $data['seo_description'] = "Award winning VW Camper hire in the heart of the South West. Perfect for short breaks or VW Camper holidays.";

		$blogArticle = BlogArticle::create($data);

		if ($request->hasFile('image')) {
			if ($request->file('image')->isValid()) {
				$uploadedImage = $request->file('image');
				$img = \Image::make($uploadedImage->getRealPath());

				$imagePath = 'blog/' . $blogArticle->slug . '.' . $uploadedImage->getClientOriginalExtension();

				$img->save(public_path('uploads/' . $imagePath));
				$blogArticle->image_path = $imagePath;
				$blogArticle->save();
			}
		}

		flash()->success('Blog article created.');

		return redirect()->action('BlogArticleController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return view('admin.blog_article.show', ['blogArticle' => BlogArticle::findOrFail($id)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$blogArticle = BlogArticle::findOrFail($id);

		$response = $this->api()->propertyService()
			->listProperties(
				(new ListPropertyPayload())->setLimit(20)
			);
		$response = $response->items;

		if(!empty($response)) {
			foreach($response as $van) {
				$vans[$van->uuid] = $van->name;
			}
		}

		return view('admin.blog_article.edit', compact('blogArticle', 'vans'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$blogArticle = BlogArticle::findOrFail($id);

		$this->validate($request, [
			'title' => 'required',
			'author' => 'required',
			'published_at' => 'required',
			'slug' => 'required|alpha_dash|unique:blog_articles,slug,'.$id,
			'image' => 'image',
		]);

		$data = $request->all();
		$data['published'] = ($request->has('published')) ? 1 : 0;

		$blogArticle->fill($data);

		if ($request->hasFile('image')) {
			if ($request->file('image')->isValid()) {
				$uploadedImage = $request->file('image');
				$img = \Image::make($uploadedImage->getRealPath());

				$imagePath = 'blog/' . $blogArticle->slug . '.' . $uploadedImage->getClientOriginalExtension();

				$img->save(public_path('uploads/' . $imagePath));
				$blogArticle->image_path = $imagePath;
			}
		}

		$blogArticle->save();

		// TODO better cache clearing for images
		\Cache::flush();

		flash()->success('Blog article updated.');

		return redirect()->action('BlogArticleController@index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$blogArticle = BlogArticle::findOrFail($id);
		$blogArticle->delete();

		flash()->success('Blog article deleted.');

		return redirect()->action('BlogArticleController@index');
	}

}
