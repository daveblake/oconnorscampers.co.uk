<?php

namespace App\Helpers;

use FullyBooked\Client\Property\Response\PropertyResponse;

class Property extends PropertyResponse
{

  /**
   * @param PropertyResponse $response
   *
   * @return Property
   */
  public static function createFromResponse(
    PropertyResponse $response
  )
  {
    $reflection = new \ReflectionClass($response);
    $property   = new static;

    foreach($reflection->getProperties() as $prop)
    {
      $property->{$prop->name} = $response->{$prop->name};
    }

    return $property;
  }

  /**
   * @param \stdClass $response
   *
   * @return static
   */
  public static function createFromResponseFromStdObject(\stdClass $response)
  {
    $property   = new static;
    $reflection = new \ReflectionClass($property);

    foreach($reflection->getProperties() as $prop)
    {
      if(!property_exists($response, $prop->name))
      {
        continue;
      }
      $property->{$prop->name} = $response->{$prop->name};
    }

    return $property;
  }

  /**
   * @param $tag
   *
   * @return bool
   */
  public function hasTag($tag)
  {
    return in_array($tag, $this->tags);
  }

  public function customValue($item, $default = '')
  {
    return isset($this->customValues->{$item}) ? $this->customValues->{$item} : $default;
  }

  public function hasImageFromTag($tag)
  {
    try
    {
      $this->imageFromTag($tag);
      return true;
    }
    catch(\Exception $e)
    {
      return false;
    }
  }

  public function imageFromTag($tag)
  {
    static $tagCache = [];
    if(isset($tagCache[$tag]))
    {
      return $tagCache[$tag];
    }

    foreach($this->images as $image)
    {
      foreach($image->tags as $imageTag)
      {
        if($imageTag == $tag)
        {
          return $image;
        }
      }
    }

    throw new \Exception("Tag $tag not found");
  }

  public function imagesFromTag($tag)
  {
    $images = [];
    foreach($this->images as $image)
    {
      foreach($image->tags as $imageTag)
      {
        if($imageTag == $tag)
        {
          $images[] = $image;
        }
      }
    }
    if(count($images) == 0)
    {
      throw new \Exception("Tag $tag not found");
    }

    return $images;
  }
}