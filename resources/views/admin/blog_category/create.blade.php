@extends('admin._layout')

@section('content')

<h1 class="page-header">Create Blog Category</h1>

@include('layout.partials.errors')

{!! Form::model($blogCategory, array('route' => 'admin.blog_category.store', 'method' => 'POST')) !!}

    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('slug', 'Slug (URL Name)') !!}
        <div class="input-group">
            <div class="input-group-addon">/blog/category/</div>
            {!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
        </div>
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function() {
        $('#slug').slugify('#name');
    });
</script>

@stop