<?php

namespace App\Http\Controllers;

use App\Guide;
use App\GuideSection;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests;

class GuideController extends Controller {

	public function frontendList()
	{
		$guides = Guide::where('published', 1)->orderBy('updated_at', 'desc')->paginate(6);

		$page = Page::where('slug', 'travel-guides')->firstOrFail();

		return view('guides', compact('guides', 'page'));
	}

	public function frontendDetail($slug)
	{
		$guide = Guide::where('published', 1)->where('slug', $slug)->firstOrFail();
		$moreGuides = Guide::where('published', 1)->where('id', '!=', $guide->id)->orderBy('updated_at', 'desc')->take(3)->get();

		return view('guide', compact('guide', 'moreGuides'));
	}

	public function frontendPreview($slug)
	{
		$guide = Guide::where('slug', $slug)->firstOrFail();

		return view('guide', compact('guide'));
	}

	/**
	 * Display a listing of guides
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$guides = Guide::orderBy('updated_at', 'desc')->get();

		return view('admin.guide.index', compact('guides'));
	}

	/**
	 * Show the form for creating a new guide
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$guide = new Guide();

		return view('admin.guide.create', compact('guide'));
	}

	/**
	 * Store a newly created guide in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
			'title' => 'required',
			'location' => 'required',
			'slug' => 'required|alpha_dash|unique:guides',
			'image' => 'image',
		]);

		$data = $request->all();
		$data['published'] = ($request->has('published')) ? 1 : 0;

		if (!$data['seo_title']) $data['seo_title'] = $data['title']." | O'Connors VW Campervan Hire";
		if (!$data['seo_description']) $data['seo_description'] = "Award winning VW Camper hire in the heart of the South West. Perfect for short breaks or VW Camper holidays.";

		$guide = Guide::create($data);

		if ($request->hasFile('image')) {
			if ($request->file('image')->isValid()) {
				$uploadedImage = $request->file('image');
				$img = \Image::make($uploadedImage->getRealPath());

				$imagePath = 'guides/' . $guide->slug . '.' . $uploadedImage->getClientOriginalExtension();

				$img->save(public_path('uploads/' . $imagePath));
				$guide->image_path = $imagePath;
				$guide->save();
			}
		}

		flash()->success('Guide created.');

		return redirect()->action('GuideController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return view('admin.guide.show', ['guide' => Guide::findOrFail($id)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$guide = Guide::findOrFail($id);

		return view('admin.guide.edit', compact('guide'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$guide = Guide::findOrFail($id);

		$this->validate($request, [
			'title' => 'required',
			'location' => 'required',
			'slug' => 'required|alpha_dash|unique:guides,slug,'.$id,
			'image' => 'image',
		]);

		$data = $request->all();
		$data['published'] = ($request->has('published')) ? 1 : 0;

		$guide->fill($data);

		if ($request->hasFile('image')) {
			if ($request->file('image')->isValid()) {
				$uploadedImage = $request->file('image');
				$img = \Image::make($uploadedImage->getRealPath());

				$imagePath = 'guides/' . $guide->slug . '.' . $uploadedImage->getClientOriginalExtension();

				$img->save(public_path('uploads/' . $imagePath));
				$guide->image_path = $imagePath;
			}
		}

		$guide->save();

		// Create/Update Guide Sections
		if ($request->has('sections')) {
			foreach($request->get('sections') as $id => $sectionData) {
				$this->validate($request, [
					'sections.'.$id.'.guide_id' => 'required',
					'sections.'.$id.'.heading' => 'required',
					'sections.'.$id.'.image' => 'image',
				]);

				$section = GuideSection::findOrNew($id);
				$section->fill($sectionData);
				$section->save();

				if ($request->hasFile('sections.'.$id.'.image')) {
					if ($request->file('sections.'.$id.'.image')->isValid()) {
						$uploadedImage = $request->file('sections.'.$id.'.image');
						$img = \Image::make($uploadedImage->getRealPath());

						$imagePath = 'guides/' . $guide->slug . '_'.$section->id.'.' . $uploadedImage->getClientOriginalExtension();

						$img->save(public_path('uploads/' . $imagePath));
						$section->image_path = $imagePath;
						$section->save();
					}
				}
			}
		}

		// Delete Guide Sections
		if ($request->has('deleteSections')) {
			foreach($request->get('deleteSections') as $id) {
				$section = GuideSection::findOrFail($id);
				$section->delete();
			}
		}

		// TODO better cache clearing for images
		\Cache::flush();

		flash()->success('Guide updated.');

		return redirect()->action('GuideController@index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$guide = Guide::findOrFail($id);
		$guide->delete();

		flash()->success('Guide deleted.');

		return redirect()->action('GuideController@index');
	}

}
