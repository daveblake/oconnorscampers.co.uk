<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Large implements FilterInterface
{
  public function applyFilter(Image $image)
  {
    return $image->orientate()->resize(
      1200,
      600,
      function ($constraint)
      {
        $constraint->aspectRatio();
      }
    );
  }
}