@extends('_layout')

@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-6">
            <h2 style="margin-bottom:0;">That's it! Booking confirmed.</h2>
        </div>

        <div class="col-md-6 text-right">
            <p style="margin-bottom:0;margin-top:20px;line-height:30px;">A confirmation email has been sent to {{ $booking->customer->email }}</p>
        </div>

    </div>

    <hr>

    <div class="row">

        <div id="confirmation-van-info" class="col-md-4">
            <h2>{{ $booking->property->name }}</h2>
            <h6>{{ $booking->property->maxGuests }} Berth {{ $booking->property->customValues->type }} Van</h6>

            <p>
                <strong>Booked by {{ $booking->customer->forename }} {{ $booking->customer->surname }}</strong><br>
                From {{ date('l jS F Y', strtotime($booking->dateFrom)) }}<br>
                To {{ date('l jS F Y', strtotime($booking->dateTo)) }}<br>
                {{ $booking->numAdults }} {{ str_plural('Adult', intval($booking->numAdults)) }} and {{ $booking->numChildren }} {{ str_plural('Child', intval($booking->numChildren)) }}
            </p>

            <p>Your van will be available for Collection from 2.30pm on {{ date('l jS F Y', strtotime($booking->dateFrom)) }}<br>
                and will need to be dropped off by 10am on {{ date('l jS F Y', strtotime($booking->dateTo)) }}</p>

            <a href="#collecting-your-camper" class="btn btn-primary">Collection Info</a>
        </div>

        <div class="col-md-8">

            @if($booking->property->hasImageFromTag('side-roofdown-large'))
                <img class="img-responsive" src="{{$booking->property->imageFromTag('side-roofdown-large')->fullPath}}" alt="{{$booking->property->name}}" />
                @else
            <img class="img-responsive" src="/assets/images/vans/{{ $booking->property->slug }}/{{ $booking->property->slug }}_side_roofdown_large.jpg" alt="{{$booking->property->name}}" />
                @endif
        </div>

    </div>

    <hr>

    <div class="row">

        <div class="col-md-4">
            <h4>Your Details</h4><br>

            <p>
                <strong>{{ $booking->customer->title }} {{ $booking->customer->forename }} {{ $booking->customer->surname }}</strong><br>
                Email: {{ $booking->customer->email }}<br>
                Tel: {{ $booking->customer->telMob }}
            </p>

            <p>
                <strong>Address:</strong><br>
                {{ $booking->customer->address1 }}<br>
                {{ $booking->customer->address2 }}<br>
                {{ $booking->customer->town }}<br>
                {{ $booking->customer->county }}<br>
                {{ $booking->customer->postcode }}
            </p>
        </div>

        <div class="col-md-4">
            <h4>Optional Extras</h4><br>

            <p>
            @if(empty($booking->lines))
                <em>None</em>
            @else
                @foreach($booking->lines as $line)
                    {{ $line->quantity }} x {{ $line->item }} at &pound;{{ $line->pricePerItem }} = &pound;{{ $line->priceTotal }}<br>
                @endforeach
            @endif
            </p>
        </div>

        <div class="col-md-4">
            <?php /*
            <h4>Insurance</h4><br>

            <p>
                Extra insurance<br>
                Extra insurance
            </p><br>*/ ?>

            <h4>Payment Details</h4><br>

            <p>
                @if(empty($booking->payments))
                    <em>None</em>
                @else
                    @foreach($booking->payments as $payment)
                        &pound;{{ $payment->amount }} paid through {{ $payment->paymentSource }} [{{ $payment->transactionReference }}]<br>
                    @endforeach
                @endif
            </p>
        </div>

    </div>

    <hr>

    <?php if (! $hideBacsWarning ): ?>
    <div class="alert alert-warning">
        <p><span class="glyphicon glyphicon-info-sign"></span> If paying by BACS bank transfer, please make the payment using the details emailed to you.</p>
    </div>
    <?php endif; ?>

    <div class="row">

        <div class="col-md-push-8 col-md-4">
            <p id="confirmation-total">
                <span>TOTAL</span> &pound;{{ $booking->totalPrice }}<br>
                <span>including VAT @20%</span>
            </p>

            <hr>
        </div>

    </div>

    <h2>Collecting your camper</h2>

    <div id="collecting-your-camper" class="row">

        <div class="col-md-4">

            <br>
            <p>Your van will be available for Collection from 2.30pm on {{ date('l jS F Y', strtotime($booking->dateFrom)) }}<br>
                and will need to be dropped off by 10am on {{ date('l jS F Y', strtotime($booking->dateFrom)) }}</p>

            <p>Highlands, Old Road,<br>
                High Street, Okehampton,<br>
                EX20 1SP</p>

            <p>t: 01837 659599<br>
                e: hello@oconnorscampers.co.uk</p>
            <br>

        </div>

        <div class="col-md-8">

            <div id="collection-map"></div>

        </div>

    </div>

    <div class="row">

        <div class="col-md-6">

            <h4>How to find us from Exeter direction</h4>

            <p>If you are coming from Exeter direction on the A30, take the first signed exit for Okehampton.
                At the T junction turn right. Travel through the town and after the third set of traffic lights,
                take the second right, called High Street. At this point, on the left of the main road, a brown
                Highways' Tourism sign for O'Connors Campers will help you find the turning into High Street.
                (High Street is no longer the main street in Okehampton, but now a residential road). Travel up
                High Street, turning right into Old Road, where the road bears left (again indicated by a brown
                Highways' Tourism sign for O'Connors Campers). Once you are past the last house and out into the
                countryside, O'Connors Campers can be found after about 500 yards on the right hand side.</p>

        </div>

        <div class="col-md-6">

            <h4>How to find us from Cornwall direction</h4>
            
            <p>If you are coming from Cornwall on the A30, take the first signed exit for Okehampton and turn left at
                the T junction. Travel towards the town and after about 2 to 3 miles you will come to the bottom of a
                hill at the beginning of the town. Once you are past Chapman's Carpets on the left, turn left at the
                New Life Church and proceed up High Street. (High Street is no longer the main street in Okehampton,
                but a residential road.) Turn right into Old Road, where the road bears left (indicated by a brown
                Highways' Tourism sign for O'Connors Campers). Once you are past the last house and out into the
                countryside, O'Connors Campers can be found after about 500 yards on the right hand side.</p>

        </div>

    </div>

</div>

<div class="banner ltr" id="photos">
    <div class="container text-center">
        <h4>Enjoy your campervan adventure and don't forget to</h4>
        <h2>Take Lots Of Photos</h2>
        <h4>#OCONNORSCAMPERS</h4>
    </div>
</div>

<div class="container text-center">

    <h2>Next step</h2>

    <p>Add your driver details and other guest information by logging in to your account.</p>

    <a class="btn btn-lg btn-primary" href="{{ route('account') }}">Go to My Account</a>

</div>

@endsection

@section('scripts')
<script type="text/javascript">

    var map;
    function initMap() {
        var ocLatLng = {lat: 50.735264, lng: -4.009977};

        map = new google.maps.Map(document.getElementById('collection-map'), {
            center: ocLatLng,
            zoom: 11,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scrollwheel: false
        });

        var marker = new google.maps.Marker({
            position: ocLatLng,
            map: map,
            title: "O'Connors Campers"
        });
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6fJmVkYQd4UM7hz8_RTDzM6qFc_xkc7Y&callback=initMap">
</script>
@endsection