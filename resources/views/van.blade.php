@extends('_layout')

@section('seo')

  <meta name="description" content="{{ $van->summary }}">
  <title>{{ $van->name }} | O'Connors VW Campervan Hire</title>

  <meta name="twitter:title" content="{{ $van->name }} | O'Connors VW Campervan Hire">
  <meta name="twitter:description" content="{{ $van->summary }}">
  <meta name="twitter:image" content="">

  <meta property="og:title" content="{{ $van->name }} | O'Connors VW Campervan Hire"/>
  <meta property="og:description" content="{{ $van->summary }}"/>
  <meta property="og:type" content="website"/>
  <meta property="og:url" content="{{ URL::current() }}"/>
  <meta property="og:image" content=""/>

@endsection

@section('content')

  <div id="van-header">
    <div class="container">
      <div class="row">

        <div class="col-md-1 hidden-sm hidden-xs">
          @if ( $van->hasImageFromTag('side-small') )
            <img class="img-responsive" src="{{$van->imageFromTag('side-small')->fullPath}}"
                 alt="{{$van->imageFromTag('side-small')->title}}">
          @else
            <img class="img-responsive" src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_small.jpg"
                 alt="Placeholder"/>
          @endif
        </div>
        <div class="col-sm-4">
          <h4>{{ $van->name }} <span>{{ $van->maxGuests }} Berth</span></h4>
          <h6>{{ $van->customValue('type') }}</h6>
        </div>

        <div class="col-md-7 col-sm-8 text-right">
          <form class="form-inline" action="{{ route('booking', $van->slug) }}">
            <input type="text" id="collection-date-calendar" class="form-control" value="Select Collection Date"/>
            <input type="hidden" id="collection-date" name="date"/>
            <select name="nights" id="nights" class="form-control">
              <option value="">Select Nights</option>
            </select>
            <span id="price"></span>
            <button class="btn btn-primary">Book Online</button>
          </form>
        </div>

      </div>
    </div>
  </div>

  @if ( $van->hasImageFromTag('main-header') )
    <div class="banner ltr top empty"
         style="background-image: url('{{$van->imageFromTag('main-header')->fullPath}}');">
    </div>
  @else
    <div class="banner ltr top empty"
         style="background-image: url('/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_main_header.jpg');">
    </div>
  @endif

  <div class="text-center container">

    <div class="row">

      <div class="col-md-offset-2 col-md-8">

        <h1>{{ $van->name }}</h1>
        <h6>{{ $van->maxGuests }} Berth &bull; {{ $van->customValue('type') }}</h6>

        {!! $van->description !!}

        <h4 class="heading">{{ $van->headline }}</h4>

        @if ( $van->hasImageFromTag('side-roofdown-large') )
          <img class="img-responsive"
               src="{{$van->imageFromTag('side-roofdown-large')->fullPath}}"/>
        @else
          <img class="img-responsive"
               src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_roofdown_large.jpg"/>
        @endif

        <div class="row">
          @if ( $van->icons['desktop'] )
            <div class="col-md-12 text-center hidden-sm hidden-xs">
              <img class="img-responsive" src="{{$van->icons['desktop']}}"/>
            </div>
          @endif

          @if ( $van->icons['mobile'] )
            <div class="col-sm-8 col-sm-offset-2 text-center hidden-md hidden-lg">
              <img src="{{$van->icons['mobile']}}"/>
            </div>
          @endif
        </div>

      </div>

    </div>

  </div>

  @if ( $van->hasImageFromTag('gallery'))
    <div class="banner ltr" style="background-image: url('{{$van->imageFromTag('gallery')->fullPath}}');">
      <div class="container text-center">
        <h2>Take A Tour</h2>
        <button id="launch-gallery" class="btn btn-lg btn-default">View Photo Gallery</button>
      </div>
    </div>
  @else
    <div class="banner ltr" style="background-image: url('/assets/images/banners/battenberg-interior.jpg');">
      <div class="container text-center">
        <h2>Take A Tour</h2>
        <button id="launch-gallery" class="btn btn-lg btn-default">View Photo Gallery</button>
      </div>
    </div>
  @endif

  <div class="text-center container">

    @if(count($blogArticles) > 0)

      <h2 class="heading">{{ $van->name }}'s previous adventures</h2>

      <div class="row">

        @foreach($blogArticles as $blogArticle)

          <div class="col-md-4 grid-box">
            <div class="img-container">
              <a href="{{ route('blog.show', $blogArticle->slug) }}"><img class="img-responsive"
                                                                          src="{{ $blogArticle->image('small') }}"
                                                                          alt="{{ $blogArticle->title }}"/></a>
            </div>
            <h3><a href="{{ route('blog.show', $blogArticle->slug) }}">{{ $blogArticle->title }}</a></h3>
            <h6>From {{ $blogArticle->author }}</h6>
            {!! $blogArticle->summary !!}
          </div>

        @endforeach

      </div>

    @endif

    @if(!empty($relatedVans))
      <h2 class="heading">You may also be interested in...</h2>

      <div class="row">
        @foreach($relatedVans as $relatedVan)
          <div class="col-sm-4 van-box">
            @if ( $relatedVan->hasImageFromTag('side-med'))
              <a href="{{ route('van.show', $relatedVan->slug) }}"><img class="img-responsive"
                                                                        src="{{$relatedVan->imageFromTag('side-med')->fullPath}}"
                                                                        alt="{{ $relatedVan->name }}"/></a>
              @else
            <a href="{{ route('van.show', $relatedVan->slug) }}"><img class="img-responsive"
                                                                      src="/assets/images/vans/{{ $relatedVan->slug }}/{{ $relatedVan->slug }}_side_med.jpg"
                                                                      alt="{{ $relatedVan->name }}"/></a>
            @endif
            <h3>{{ $relatedVan->name }}</h3>
            <h6>{{ $relatedVan->maxGuests }} Berth &bull; {{ $relatedVan->customValue('type') }}</h6>
            <p>{{ $relatedVan->summary }}</p>
          </div>
        @endforeach
      </div>
    @endif

    <div class="container text-center">

      <a class="btn btn-lg btn-primary" href="{{ route('van.index') }}">View All Vans</a>

    </div>

  </div>

@endsection

@section('scripts')
  <script type="text/javascript">

    var galleryData = [];
    @if ( $van->hasImageFromTag('gallery'))
      @foreach ( $van->imagesFromTag('gallery') as $image )
        galleryData[galleryData.length] =
        {
          'src': '{{$image->fullPath}}',
          'thumb': '{{$image->fullPath}}',
          'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
        }
      @endforeach
    @else
        galleryData = [
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_front_large.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_front_large.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_front_large.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_side_roofdown_large.jpg')))
      {
        "src": '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_roofdown_large.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_roofdown_large.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_side_roofup_large.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_roofup_large.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_roofup_large.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_back_large.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_back_large.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_back_large.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_main_header.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_main_header.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_main_header.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_interior_1.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_interior_1.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_interior_1.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_interior_2.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_interior_2.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_interior_2.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      },
        @endif
        @if (file_exists(public_path('assets/images/vans/' . $van->slug . '/' . $van->slug . '_interior_3.jpg')))
      {
        'src': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_interior_3.jpg',
        'thumb': '/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_interior_3.jpg',
        'subHtml': '<h4>{{ $van->name }}</h4><h5>{{ $van->headline }}</h5>',
      }
      @endif
    ]
    @endif

$(document).ready(
        function ()
        {

          var $window = $(window);

          if ($("#header-navbar").is(":visible"))
          {
            var headerHeight = $("#header-navbar").outerHeight();
            var vanHeader = $("#van-header");
            $window.scroll(
                function ()
                {
                  if ($window.scrollTop() >= headerHeight)
                  {
                    vanHeader.css("position", "fixed");
                  }
                  else
                  {
                    vanHeader.css("position", "static");
                  }
                }
            );
          }

          // Collection date picker
          var vanAvailability = {!! json_encode($vanAvailability) !!};

          $('#collection-date-calendar').datepicker(
              {
                dateFormat: "d MM yy",
                altField: "#collection-date",
                altFormat: "yy-mm-dd",
                minDate: +1,
                maxDate: "+2Y",
                firstDay: 1,
                dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
                beforeShowDay: function (date)
                {
                  date.setHours(12); // avoid timezone issues
                  var key = date.toJSON().split('T')[0];
                  var className = 'unavailable';
                  var tooltip = null;
                  if (key in vanAvailability)
                  {
                    className = 'available';
                    tooltip = 'Prices:';
                    $.each(
                        vanAvailability[key], function (nights, price)
                        {
                          if (nights > 7) return;
                          tooltip += '\n' + nights + ' Nights - £' + price;
                        }
                    );
                  }
                  date.setHours(0); // revert for jquery time comparison
                  return [(className == 'available'), className, tooltip];
                },
                onSelect: function ()
                {
                  $("#nights").val('');
                  $('#price').slideUp(400, function () { $(this).text('') });
                  var collectionDate = new Date($(this).datepicker("getDate"));

                  collectionDate.setHours(12); // avoid timezone issues
                  var dateKey = collectionDate.toJSON().split('T')[0];
                  var nightOptions = [
                    {key: 3, label: '3 Nights'},
                    {key: 4, label: '4 Nights'},
                    {key: 7, label: '7 Nights'},
                    {key: 14, label: '14 Nights'}
                  ];
                  var availableNights = [];

                  for (var i in nightOptions)
                  {
                    var night = nightOptions[i];
                    console.log(night);
                    console.log(vanAvailability[dateKey]);
                    if (night.key in vanAvailability[dateKey])
                    {
                      availableNights[availableNights.length] = night;
                    }
                  }
                  console.log(availableNights);
                  var optionHtml = '<option>Select Nights</option>';
                  for (var i in availableNights)
                  {
                    var night = availableNights[i];
                    optionHtml += '<option value=' + night.key + '>' + night.label + '</option>';
                  }

                  $('#nights').html(optionHtml);
                }
              }
          );

          $("#nights").change(
              function ()
              {
                var nights = this.value;
                var collectionDate = new Date($('#collection-date-calendar').datepicker("getDate"));
                collectionDate.setHours(12); // avoid timezone issues
                var dateKey = collectionDate.toJSON().split('T')[0];
                var price = parseInt(vanAvailability[dateKey][nights]);
                $('#price').text('£' + price).slideDown();
              }
          );

          $('#launch-gallery').on(
              'click', function ()
              {

                $(this).lightGallery(
                    {
                      download: false,
                      dynamic: true,
                      dynamicEl: galleryData
                    }
                )

              }
          );

        }
    );
  </script>
@endsection