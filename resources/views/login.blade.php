@extends('_layout')

@section('content')

  <div class="container text-center">

    <div class="row">

      <div class="col-md-offset-2 col-md-8 spacer">

        {!! Form::open(array( 'route'=>'login', 'method'=>'post' )) !!}

        <h3>Sign in to your O'connors Campers account</h3>

        @include('layout.partials.errors')
        @include('flash::message')

        <div class="row">
          <div class="col-md-6">
            <input name="email" type="email" id="inputEmail"
                   value="{{Input::old('email')}}" class="form-control"
                   placeholder="Email address" required autofocus>
          </div>
          <div class="col-md-6">
            <input name="password" type="password" id="inputPassword"
                   class="form-control" placeholder="Password" required>

          </div>
        </div>
        <div class="row">
          <div class="col-md-offset-6 col-md-6 text-left">
            <a class="forgotPasswordTrigger" href="#">Forgot Password</a>
          </div>
        </div>

        <br>
        <button class="btn btn-lg btn-primary" type="submit">Sign in</button>

        {!! Form::close() !!}

        <div class="forgotPassword" style="display: none;">
          {!! Form::open(array( 'route'=>'password.email', 'method'=>'post' )) !!}

          <h3>Forgot Password</h3>

          @include('layout.partials.errors')
          @include('flash::message')

          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-md- text-center">
              <input name="email" type="email" id="inputEmail"
                     value="{{Input::old('email')}}" class="form-control"
                     placeholder="Email address" required autofocus>
            </div>
          </div>

          <br>
          <button class="btn btn-lg btn-primary" type="submit">
            Reset Password
          </button>

          {!! Form::close() !!}
        </div>

        <div class="spacer"></div>

      </div>

    </div>

  </div>

@endsection