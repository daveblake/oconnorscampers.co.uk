@extends('admin._layout')

@section('content')

<h1 class="page-header">Create Blog Article</h1>

@include('layout.partials.errors')

{!! Form::model($blogArticle, array('route' => 'admin.blog_article.store', 'method' => 'POST', 'files' => true)) !!}

    <div class="form-group">
        {!! Form::label('published', 'Published?') !!}
        {!! Form::checkbox('published', '1') !!}
    </div>

    <div class="form-group">
        {!! Form::label('published_at', 'Published Date') !!}
        {!! Form::text('published_at', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('blog_category_id', 'Blog Category') !!}
        {!! Form::select('blog_category_id', $categories = \App\BlogCategory::orderBy('order')->lists('name', 'id'), null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('van_uuid', 'Associated Van') !!}
        {!! Form::select('van_uuid', $vans, null, array('class' => 'form-control', 'placeholder' => '-N/A-')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('slug', 'Slug (URL Name)') !!}
        <div class="input-group">
            <div class="input-group-addon">/blog/</div>
            {!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('author', 'Author') !!}
        {!! Form::text('author', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_title', 'SEO Title') !!}
        {!! Form::text('seo_title', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_description', 'SEO Description') !!}
        {!! Form::text('seo_description', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('summary', 'Summary') !!}
        {!! Form::textarea('summary', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('content', 'Content') !!}
        {!! Form::textarea('content', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('image', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image_alt', 'Image alt description') !!}
        {!! Form::text('image_alt', null, array('class' => 'form-control')) !!}
    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function() {
    $('#slug').slugify('#title');

    $('#published_at').datepicker({
        dateFormat: "yy-mm-dd"
    });
});
</script>
@stop