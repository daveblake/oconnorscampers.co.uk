<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests;

class PageController extends Controller
{

    public function showFrontend($slug)
    {
        $page = Page::where('published', 1)->where('slug', $slug)->firstOrFail();

        return view('page', compact('page'));
    }

    public function showPreview($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();

        return view('page', compact('page'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::paginate(10);
        return view('admin.page.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = new Page();
        return view('admin.page.create', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'heading' => 'required',
            'slug' => 'required|unique:pages',
        ]);

        $data = $request->all();
        $data['published'] = ($request->has('published')) ? 1 : 0;

        if (!$data['seo_title']) $data['seo_title'] = $data['heading']." | O'Connors VW Campervan Hire";
        if (!$data['seo_description']) $data['seo_description'] = "Award winning VW Camper hire in the heart of the South West. Perfect for short breaks or VW Camper holidays.";

        Page::create($data);

        flash()->success('Page created.');

        return redirect()->action('PageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.page.show', ['page' => Page::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);

        return view('admin.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        $this->validate($request, [
            'heading' => 'required',
            'slug' => 'required|unique:pages,slug,'.$id,
        ]);

        $data = $request->all();
        $data['published'] = ($request->has('published')) ? 1 : 0;

        $page->fill($data);
        $page->save();

        flash()->success('Page updated.');

        return redirect()->action('PageController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();

        flash()->success('Page deleted.');

        return redirect()->action('PageController@index');
    }

}
