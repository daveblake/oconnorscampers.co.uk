<?php

namespace App\Http\Controllers;

use App\Helpers\Property;
use App\Http\Requests;
use FullyBooked\Client\Availability\Payload\ListAvailabilityPayload;
use FullyBooked\Client\Availability\Response\AvailabilityResponse;
use FullyBooked\Client\Availability\Response\Group\GroupAvailabilityByGroupCollectionResponse;
use FullyBooked\Client\Availability\Response\Group\PropertyAvailabilityByGroupCollectionResponse;
use FullyBooked\Client\Property\Payload\ListPropertyPayload;
use FullyBooked\Client\Property\Response\PropertyResponse;

class AvailabilityController extends Controller
{


  public function getCalendar()
  {
    $vans = $this->api()->propertyService()
      ->listProperties(
        (new ListPropertyPayload())->setLimit(100)
      );
    $vans = $vans->items;
    /** @var PropertyResponse $van */
    foreach($vans as $key => $van)
    {
      $vans[$key] = Property::createFromResponse($van);
    }

    $currentMonth = (int)date('n');
    $nowDate      = new \DateTime();
    $dateRange    = new \DateInterval('P2Y');
    $maxDate      = new \DateTime();
    $maxDate->add($dateRange);

    if(\Input::has('date'))
    {
      $selectedDate = new \DateTime(\Input::get('date'));
    }
    else
    {
      $selectedDate = clone $nowDate;
    }
    if($selectedDate < $nowDate)
    {
      $selectedDate = $nowDate;
    }
    if($selectedDate > $maxDate)
    {
      $selectedDate = $maxDate;
    }

    $surroundingDays = new \DateInterval('P8D');
    $shownDays       = new \DateInterval('P14D');
    $dateFrom        = clone $selectedDate;
    $dateFrom->sub($surroundingDays);
    $dateTo = clone $selectedDate;
    $dateTo->add($surroundingDays)->add($shownDays);

    $availability = $this->api()->availabilityService()
      ->listAvailabilityByGroup(
        (new ListAvailabilityPayload)->setDateFrom($dateFrom->format('Y-m-d'))->setDateTo($dateTo->format('Y-m-d'))
      );
    $queryDateTo  = clone $selectedDate;
    $queryDateTo->add($shownDays);
    $availability = $this->_prepareAvailability($availability->items, $selectedDate, $queryDateTo);

    return view('availability-calendar', compact('vans', 'availability', 'currentMonth', 'selectedDate'));
  }

  public function getVans()
  {
    $vans = $this->api()->propertyService()
      ->listProperties(
        (new ListPropertyPayload())->setLimit(20)
      );
    $vans = $vans->items;
    /** @var PropertyResponse $van */
    foreach($vans as $key => $van)
    {
      $vans[$key] = Property::createFromResponse($van);
    }

    $availability = null;

    if(\Input::has('date') && \Input::has('nights'))
    {

      $date      = \Input::get('date');
      $minGuests = \Input::get('sleeps');

      $dateFrom = new \DateTime($date);
      $dateTo   = clone $dateFrom;
      $dateTo->modify('+' . \Input::get('nights') . ' days');

      $availability = $this->api()->availabilityService()
        ->listAvailability(
          (new ListAvailabilityPayload())->setMinGuests($minGuests)->setDateFrom($dateFrom->format('Y-m-d'))->setDateTo(
            $dateTo->format('Y-m-d')
          )
        );

      if(!empty($vans))
      {
        foreach($vans as $van)
        {
          if(array_key_exists($van->uuid, $availability->items))
          {
            if(!empty($availability->items[$van->uuid]->items))
            {
              foreach($availability->items[$van->uuid]->items as $availabilityResponse)
              {
                if($availabilityResponse->dateFrom == $dateFrom->format(
                    'Y-m-d'
                  ) && $availabilityResponse->dateTo == $dateTo->format('Y-m-d')
                )
                {
                  $van->price = $availabilityResponse->price;
                }
              }
              if(isset($van->price))
              {
                $availableVans[] = $van;
              }
              else
              {
                $unavailableVans[] = $van;
              }
            }
            else
            {
              $unavailableVans[] = $van;
            }
          }
          else
          {
            $unavailableVans[] = $van;
          }
        }
      }
    }
    else
    {
      $availableVans   = $vans;
      $unavailableVans = null;
    }

    return view('availability-vans', compact('availableVans', 'unavailableVans', 'availability'));
  }

  /**
   * This method finds 'Other' availability that overlaps and splits in into seperate
   * lines so it can be displayed in the frontend calendar.
   *
   * @param PropertyAvailabilityByGroupCollectionResponse[] $availability
   *
   * @return PropertyAvailabilityByGroupCollectionResponse[]
   */
  protected function _prepareAvailability($availability, \DateTime $queryDateFrom, \DateTime $queryDateTo)
  {
    $groupTypes = ['MonFri', 'FriMon', 'MonMon', 'FriFri'];

    foreach($availability as &$propertyAvailability)
    {

      //Remove periods outside our query
      foreach($propertyAvailability->items as $groupName => $group)
      {
        foreach($group->items as $k => $period)
        {
          $testDateFrom = new \DateTime($period->dateFrom);
          $testDateTo   = new \DateTime($period->dateTo);

          if($testDateTo <= $queryDateFrom || $testDateFrom >= $queryDateTo)
          {
            unset($propertyAvailability->items[$groupName]->items[$k]);
          }
        }
        if(count($propertyAvailability->items[$groupName]->items) == 0)
        {
          unset($propertyAvailability->items[$groupName]);
        }
      }

      foreach($groupTypes as $groupKey)
      {
        if(!isset($propertyAvailability->items[$groupKey]))
        {
          $propertyAvailability->items[$groupKey] = new GroupAvailabilityByGroupCollectionResponse;

          $propertyAvailability->items[$groupKey]->name = $groupKey;
        }
      }

      uksort(
        $propertyAvailability->items,
        function ($k, $k2) use ($groupTypes)
        {
          $index1 = array_search($k, $groupTypes);
          $index2 = array_search($k2, $groupTypes);

          //Handle Others that aren't in our $groupTypes array
          if($index1 === false)
          {
            $index1 = count($groupTypes) + 1;
          }
          if($index2 === false)
          {
            $index2 = count($groupTypes) + 1;
          }

          if($index1 < $index2)
          {
            return -1;
          }
          if($index1 > $index2)
          {
            return 1;
          }
          return 0;
        }
      );

      if(!isset($propertyAvailability->items['Others']))
      {
        continue;
      }

      $othersGroup = $propertyAvailability->items['Others'];
      if(count($othersGroup->items) == 1)
      {
        continue;
      }

      $groups = [[]];
      /** @var AvailabilityResponse $period */
      foreach($othersGroup->items as $period)
      {
        foreach($groups as $k => $group)
        {
          foreach($group as $testPeriod)
          {
            $overlap = $this->_availabilityOverlaps($period, $testPeriod);
            if($overlap)
            {
              continue 2;
            }
          }

          //If we get this far no overlaps have been found, add to current group
          $groups[$k][] = $period;
          continue 2;
        }
        //If we get this far we need to add a new group
        $groups[] = [$period];
      }
      //no overlaps - lets just move onto the next property
      if(count($groups) == 1)
      {
        continue;
      }

      //Otherwise lets add new groups to the availability response
      foreach($groups as $k => $group)
      {
        $groupName = 'Others';
        if($k > 0)
        {
          $groupName .= '-' . $k;
        }

        $response = new GroupAvailabilityByGroupCollectionResponse();
        foreach($group as $period)
        {
          $response->items[$period->dateFrom] = $period;
        }

        $propertyAvailability->items[$groupName] = $response;
      }
    }

    return $availability;
  }

  protected function _availabilityOverlaps(AvailabilityResponse $period, AvailabilityResponse $testPeriod)
  {
    $dateFrom     = new \DateTime($period->dateFrom);
    $dateTo       = new \DateTime($period->dateTo);
    $testDateFrom = new \DateTime($testPeriod->dateFrom);
    $testDateTo   = new \DateTime($testPeriod->dateTo);

    if($dateFrom >= $testDateFrom && $dateFrom < $testDateTo)
    {
      return true;
    }

    if($dateTo > $testDateFrom && $dateFrom <= $testDateTo)
    {
      return true;
    }

    if($dateFrom < $testDateFrom && $dateTo > $testDateTo)
    {
      return true;
    }

    return false;
  }

}
