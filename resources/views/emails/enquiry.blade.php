<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Website Enquiry</h2>

		<p>The following information was sent through the website enquiry form:</p>

        <p><strong>Name</strong><br>
        {{ $title }} {{ $first_name }} {{ $last_name }}</p>

        <p><strong>Email</strong><br>
        {{ $email }}</p>

        <p><strong>Telephone</strong><br>
        {{ $phone }}</p>

        <p><strong>Enquiry</strong><br>
        {!! nl2br(e($enquiry)) !!}</p>

        <p><strong>Referrer</strong><br>
        {{ $referrer }}</p>

	</body>
</html>
