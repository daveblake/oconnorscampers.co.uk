<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Requests;

class ImageController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::orderBy('order')->get();
        return view('admin.image.index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $image = new Image();
        return view('admin.image.create', compact('image'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image',
        ]);

        $data = $request->all();
        $data['display'] = ($request->has('display')) ? 1 : 0;

        $image = Image::create($data);

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $uploadedImage = $request->file('image');
                $img = \Image::make($uploadedImage->getRealPath());

                $imagePath = 'images/' . $image->id . '_' . $uploadedImage->getClientOriginalName();

                $img->save(public_path('uploads/' . $imagePath));
                $image->image_path = $imagePath;
                $image->save();
            }
        }

        flash()->success('Image created.');

        return redirect()->action('ImageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.image.show', ['image' => Image::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Image::findOrFail($id);

        return view('admin.image.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = Image::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image',
        ]);

        $data = $request->all();
        $data['display'] = ($request->has('display')) ? 1 : 0;

        $image->fill($data);
        $image->save();

        flash()->success('Image updated.');

        return redirect()->action('ImageController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::findOrFail($id);

        if(\File::exists(public_path('uploads/'.$image->image_path))) \File::delete(public_path('uploads/'.$image->image_path));

        $image->delete();

        flash()->success('Image deleted.');

        return redirect()->action('ImageController@index');
    }

}
