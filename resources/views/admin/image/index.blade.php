@extends('admin._layout')

@section('content')

<h1 class="page-header">Gallery Images</h1>

@include('flash::message')

<p><a class="btn btn-success" href="{{ URL::route('admin.image.create') }}"><span class="glyphicon glyphicon-plus"></span> Create Image</a></p>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th><span class="glyphicon glyphicon-resize-vertical"></span></th>
            <th>Thumbnail</th>
            <th>Display?</th>
            <th>Title</th>
            <th width="30%" class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody class="order-sortable" data-model="image">
    @foreach($images as $image)
        <tr id="{{ $image->id }}">
            <td width="20px" class="order-handle"><span class="glyphicon glyphicon-resize-vertical"></span></td>
            <td><img src="{{ $image->image('thumb') }}" class="img-responsive" /></td>
            <td>{{ $image->display ? 'Yes' : 'No' }}</td>
            <td>{{ $image->title }}</td>
            <td class="text-right">
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.image.edit', array( $image->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Image</a>
                {!! Form::open(array('route' => array('admin.image.destroy', $image->id), 'method' => 'DELETE', 'class' => 'delete-form')) !!}
                <button type="submit" class="btn btn-small btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@stop