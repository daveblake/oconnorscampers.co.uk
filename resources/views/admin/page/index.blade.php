@extends('admin._layout')

@section('content')

<h1 class="page-header">Pages</h1>

@include('flash::message')

<p><a class="btn btn-success" href="{{ URL::route('admin.page.create') }}"><span class="glyphicon glyphicon-plus"></span> Create Page</a></p>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Published?</th>
            <th>Heading</th>
            <th>Slug</th>
            <th width="30%" class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($pages as $page)
        <tr id="{{ $page->id }}">
            <td>{{ $page->published ? 'Yes' : 'No' }}</td>
            <td>
                {{ $page->heading }}<br>
                <small>{{ $page->subheading }}</small>
            </td>
            <td>/{{ $page->slug }}</td>
            <td class="text-right">
                <a class="btn btn-small btn-default" href="{{ URL::route('preview.page', array( $page->slug )) }}" target="_blank"><span class="glyphicon glyphicon-new-window"></span> Preview</a>
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.page.edit', array( $page->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Edit Page</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $pages->render() !!}

@stop