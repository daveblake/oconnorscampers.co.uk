@extends('admin._layout')

@section('content')

<h1 class="page-header">Edit Guide <small>{!! $guide->title !!}</small></h1>

@include('layout.partials.errors')

{!! Form::model($guide, array('route' => array('admin.guide.update', $guide->id), 'method' => 'PUT', 'files' => true)) !!}

    <div class="form-group">
        {!! Form::label('published', 'Published?') !!}
        {!! Form::checkbox('published', '1') !!}
    </div>

    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('slug', 'Slug (URL Name)') !!}
        <div class="input-group">
            <div class="input-group-addon">/travel-guides/</div>
            {!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('location', 'Location') !!}
        {!! Form::text('location', null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_title', 'SEO Title') !!}
        {!! Form::text('seo_title', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('seo_description', 'SEO Description') !!}
        {!! Form::text('seo_description', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('summary', 'Summary') !!}
        {!! Form::textarea('summary', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('content', 'Content') !!}
        {!! Form::textarea('content', null, array('class' => 'ckeditor form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        @if($guide->image_path)
            <img src="{{$guide->image('small')}}" class="img-responsive" />
        @endif
        {!! Form::file('image', null, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image_alt', 'Image alt description') !!}
        {!! Form::text('image_alt', null, array('class' => 'form-control')) !!}
    </div>

    <div class="panel-group" id="sections">

        @if($guide->guideSections)
        @foreach($guide->guideSections as $guideSection)
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#" class="delete-section pull-right" data-id="{{ $guideSection->id }}">Delete</a>
                        <a data-toggle="collapse" data-parent="#sections" href="#section{{ $guideSection->id }}">Guide Section</a> <small>{{ $guideSection->heading }}</small>
                    </h4>
                </div>
                <div id="section{{ $guideSection->id }}" class="panel-collapse collapse">
                    <input type="hidden" name="sections[{{ $guideSection->id }}][guide_id]" value="{{  $guide->id }}" />
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('sections['.$guideSection->id.'][heading]', 'Heading') !!}
                            {!! Form::text('sections['.$guideSection->id.'][heading]', $guideSection->heading, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sections['.$guideSection->id.'][subheading]', 'Sub Heading') !!}
                            {!! Form::text('sections['.$guideSection->id.'][subheading]', $guideSection->subheading, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sections['.$guideSection->id.'][summary]', 'Summary') !!}
                            {!! Form::textarea('sections['.$guideSection->id.'][summary]', $guideSection->summary, array('class' => 'ckeditor form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sections['.$guideSection->id.'][content]', 'Content') !!}
                            {!! Form::textarea('sections['.$guideSection->id.'][content]', $guideSection->content, array('class' => 'ckeditor form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sections['.$guideSection->id.'][image]', 'Image') !!}
                            @if($guideSection->image_path)
                                <img src="{{$guideSection->image('small')}}" class="img-responsive" />
                            @endif
                            {!! Form::file('sections['.$guideSection->id.'][image]', null, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sections['.$guideSection->id.'][image_alt]', 'Image alt description') !!}
                            {!! Form::text('sections['.$guideSection->id.'][image_alt]', $guideSection->image_alt, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif

    </div>

    <p><a href="#" id="add-new-section" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Add New Section</a></p>

    <hr>

    {!! Form::submit('Save', array('class' => 'btn btn-success btn-lg')) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default btn-lg">Cancel</a>

{!! Form::close() !!}

<script type="text/javascript">

    var sectionHtml =
        '<div class="panel panel-info" data-id="0"><div class="panel-heading">'+
        '<h4 class="panel-title"><a data-toggle="collapse" data-parent="#sections" href="#new0">Guide Section</a> <small>New</small></h4></div>'+
        '<div id="new0" class="panel-collapse collapse"><div class="panel-body">'+

        '<input type="hidden" name="sections[new0][guide_id]" value="{{  $guide->id }}" />'+

        '<div class="form-group">'+
        '{!! Form::label('sections[new0][heading]', 'Heading') !!}'+
        '{!! Form::text('sections[new0][heading]', null, array('class' => 'form-control')) !!}'+
        '</div>'+

        '<div class="form-group">'+
        '{!! Form::label('sections[new0][subheading]', 'Sub Heading') !!}'+
        '{!! Form::text('sections[new0][subheading]', null, array('class' => 'form-control')) !!}'+
        '</div>'+

        '<div class="form-group">'+
        '{!! Form::label('sections[new0][summary]', 'Summary') !!}'+
        '{!! Form::textarea('sections[new0][summary]', null, array('class' => 'ckeditor form-control')) !!}'+
        '</div>'+

        '<div class="form-group">'+
        '{!! Form::label('sections[new0][content]', 'Content') !!}'+
        '{!! Form::textarea('sections[new0][content]', null, array('class' => 'ckeditor form-control')) !!}'+
        '</div>'+

        '<div class="form-group">'+
        '{!! Form::label('sections[new0][image]', 'Image') !!}'+
        '{!! Form::file('sections[new0][image]', null, array('class' => 'form-control')) !!}'+
        '</div>'+

        '<div class="form-group">'+
        '{!! Form::label('sections[new0][image_alt]', 'Image alt description') !!}'+
        '{!! Form::text('sections[new0][image_alt]', null, array('class' => 'form-control')) !!}'+
        '</div>'+

        '</div></div></div>';

    var sectionCounter = 0;

    $(document).ready(function() {

        $('#add-new-section').click(function(e) {
            e.preventDefault();

            var newSection = $(sectionHtml).clone();
            var oldId = sectionCounter;
            var newId = (oldId+1);

            $('.panel', newSection).data('id', newId);
            $('.panel-title a', newSection).attr('href', '#new'+newId);
            $('.panel-collapse', newSection).attr('id', 'new'+newId);

            $(newSection).find(':input').each(function(){

                var oldName = $(this).attr('name');
                var newName = oldName.replace('new'+oldId, 'new'+newId);
                $(this).attr('name', newName);
                $(this).attr('id', newName);
                $(this).prev().attr('for', newName);

            });

            $(newSection).appendTo('#sections');

            sectionCounter++;
        });

        $('.delete-section').click(function(e) {
            e.preventDefault();
            var panel = $(this).closest('.panel');
            var deleteInput = '<input type="hidden" name="deleteSections[]" value="'+$(this).data('id')+'" />';
            $(panel).replaceWith(deleteInput);
        });

    });
</script>

@stop