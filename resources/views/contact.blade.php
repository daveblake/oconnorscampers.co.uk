@extends('_layout')

@section('seo')

<meta name="description" content="{{ $page->seo_description }}">
<title>{{ $page->seo_title }}</title>

<meta name="twitter:title" content="{{ $page->seo_title }}">
<meta name="twitter:description" content="{{ $page->seo_description }}">
<meta name="twitter:image" content="">

<meta property="og:title" content="{{ $page->seo_title }}" />
<meta property="og:description" content="{{ $page->seo_description }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:image" content="" />

@endsection

@section('content')

<div class="container">

    <h2 class="heading text-center" id="enquiry-form">{{ $page->subheading }}</h2>

    <div class="row text-center">

        <div class="col-md-push-2 col-md-8">

            {!! $page->content !!}
            <p>&nbsp;</p>

        </div>

    </div>

    @if($status == 'sent')

    <div class="row text-center">
        <div class="col-md-push-2 col-md-8">
            <h4>Thanks!</h4>

            <p>Your enquiry has been sent and we will be in touch with you soon.</p>
        </div>
    </div>

    @else

    <div class="row">
        <div class="col-md-push-2 col-md-8">
            @include('layout.partials.errors')
        </div>
    </div>

    <form action="{{ route('enquiry.send') }}#enquiry-form" method="POST">

        {{ csrf_field() }}

        <div class="row">

            <div class="col-md-6">

                <div class="row">

                    <div class="form-group col-md-4">
                        <label>Title</label>
                        <select class="form-control" name="title" required="required">
                            <option></option>
                            <option>Mr</option>
                            <option>Miss</option>
                            <option>Mrs</option>
                            <option>Ms</option>
                        </select>
                    </div>

                    <div class="form-group col-md-8">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="first_name" required="required">
                    </div>

                </div>

                <div class="form-group">
                    <label>Last Name</label>
                    <input class="form-control" type="text" name="last_name" required="required">
                </div>

                <div class="form-group">
                    <label>Email Address</label>
                    <input class="form-control" type="email" name="email" required="required">
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input class="form-control" type="text" name="phone" required="required">
                </div>

            </div>

            <div class="col-md-6">

                <div class="form-group">
                    <label>Your Message</label>
                    <textarea class="form-control" rows="9" name="enquiry" required="required"></textarea>
                </div>

                <div class="form-group">
                    <label>How did you hear about us?</label>
                    <select class="form-control" name="referrer">
                        <option></option>
                        <option>Search Engine</option>
                        <option>Advertising</option>
                        <option>Word Of Mouth</option>
                        <option>Article</option>
                        <option>Other</option>
                    </select>
                </div>

            </div>

        </div>

        <div class="text-center">

            <div class="form-group">
                <label>Join the O'Connors mailing list? <input type="checkbox" name="subscribe" value="1"/></label>
            </div>

            <button type="submit" class="btn btn-secondary btn-lg">Send Message</button>

        </div>

    </form>

    @endif

</div>

<div class="banner ltr">

    <div id="contact-map"></div>

    <div class="container text-center">

        <div class="banner-overlay red"></div>

        <div class="row wow fadeInRight">
            <div class="col-md-push-9 col-md-3">
                <img class="img-responsive" style="margin:0 auto;" src="/assets/images/oconnors-campers-logo-white.png" alt="O'Connors Campers" />
                <h5>Highlands, Old Road,<br>
                    High Street, Okehampton,<br>
                    EX20 1SP</h5>

                <h5>t: 01837 659599<br>
                    e: hello@oconnorscampers.co.uk</h5>
            </div>
        </div>

    </div>

</div>

<div class="container text-center">
    <a class="btn btn-primary" href="https://www.google.co.uk/maps/place/O'Connors+Campers/@50.735253,-4.009977,15z/" target="_blank">Open Map &amp; Directions <span class="glyphicon glyphicon-new-window"></span></a>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

    var map;
    function initMap() {
        var ocLatLng = {lat: 50.735264, lng: -4.009977};

        map = new google.maps.Map(document.getElementById('contact-map'), {
            center: ocLatLng,
            zoom: 11,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scrollwheel: false
        });

        var marker = new google.maps.Marker({
            position: ocLatLng,
            map: map,
            title: "O'Connors Campers"
        });
    }

</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6fJmVkYQd4UM7hz8_RTDzM6qFc_xkc7Y&callback=initMap">
</script>
@endsection