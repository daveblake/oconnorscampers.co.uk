<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageAltToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_articles', function (Blueprint $table) {
            $table->string('image_alt')->after('image_path');
        });

        Schema::table('guides', function (Blueprint $table) {
            $table->string('image_alt')->after('image_path');
        });

        Schema::table('guide_sections', function (Blueprint $table) {
            $table->string('image_alt')->after('image_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_articles', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });

        Schema::table('guides', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });

        Schema::table('guide_sections', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
    }
}
