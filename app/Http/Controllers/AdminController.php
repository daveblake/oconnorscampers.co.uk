<?php

namespace App\Http\Controllers;

use App\Enquiry;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
        //
    }


    public function sortOrder($model, Request $request) {

        $order = $request->input('order');
        $model = '\App\\'.studly_case($model);

        $i = 1;
        foreach ($order as $id) {
            $object = $model::find($id);
            $object->order = $i;
            $object->save();
            $i++;
        }

    }


    /**
     * Display admin dashboard
     *
     * @return Response
     */
    public function dashboard()
    {

        $enquiries = Enquiry::orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.dashboard', compact('enquiries'));
    }

}
