@extends('admin._layout')

@section('content')

<h1 class="page-header">Enquiries</h1>

@include('flash::message')

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Date</th>
            <th>From</th>
            <th width="30%" class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($enquiries as $enquiry)
        <tr id="{{ $enquiry->id }}">
            <td>
                {{ $enquiry->created_at->format('l jS F, Y') }}<br>
                <small>{{ $enquiry->created_at->format('H:i:s') }}</small>
            </td>
            <td>
                {{ $enquiry->title }} {{ $enquiry->first_name }} {{ $enquiry->last_name }}<br>
                {{ $enquiry->email }}
            </td>
            <td class="text-right">
                <a class="btn btn-small btn-info" href="{{ URL::route('admin.enquiry.show', array( $enquiry->id )) }}"><span class="glyphicon glyphicon-pencil"></span> Show Enquiry</a>
                {!! Form::open(array('route' => array('admin.enquiry.destroy', $enquiry->id), 'method' => 'DELETE', 'class' => 'delete-form')) !!}
                <button type="submit" class="btn btn-small btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $enquiries->render() !!}

@stop