@extends('_layout')

@section('seo')

<meta name="description" content="{{ $blogArticle->seo_description }}">
<title>{{ $blogArticle->seo_title }}</title>

<meta name="twitter:title" content="{{ $blogArticle->seo_title }}">
<meta name="twitter:description" content="{{ $blogArticle->seo_description }}">
<meta name="twitter:image" content="{{ asset($blogArticle->image('small')) }}">

<meta property="og:title" content="{{ $blogArticle->seo_title }}" />
<meta property="og:description" content="{{ $blogArticle->seo_description }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:image" content="{{ asset($blogArticle->image('small')) }}" />

@endsection

@section('content')

<div class="container blog">

    <div class="row">

        <div class="col-md-3 blog-sidebar">
            <div class="affix">
                @if(count($blogCategories) > 0)
                    <h4>Categories</h4>
                    <ul>
                        @foreach($blogCategories as $blogCategory)
                            <li><a href="{{ route('blog.category.show', $blogCategory->slug) }}">{{ $blogCategory->name }} ({{ count($blogCategory->articles) }})</a></li>
                        @endforeach
                    </ul>
                @endif

                @if(count($recentEntries) > 0)
                    <h4>Recent Entries</h4>
                    <ul>
                        @foreach($recentEntries as $recentEntry)
                            <li>
                                <a href="{{ route('blog.show', $recentEntry->slug) }}">{{ $recentEntry->title }}</a><br>
                                <small>{{ $recentEntry->published_at->format('l jS F, Y') }}</small>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>

        <div class="col-md-9">

            <h1>{{ $blogArticle->title }}</h1>
            <h2>by {{ $blogArticle->author }} | {{ $blogArticle->published_at->format('l jS F, Y') }}</h2>

            <img class="img-responsive blog-image" src="{{ $blogArticle->image('large') }}" alt="{{ ($blogArticle->image_alt) ? $blogArticle->image_alt : $blogArticle->title }}" />

            {!! $blogArticle->content !!}

            <hr class="spacer">

            <div class="row">

                <div class="col-sm-6">
                    <h5>Posted in {{ $blogArticle->blogCategory->name }}</h5>
                    <h5>on {{ $blogArticle->published_at->format('l jS F, Y') }}</h5>
                </div>

                <div class="col-sm-6 text-right">
                    <h5>Share This Post</h5>
                    <a class="social-icon" href="https://www.twitter.com/share?url={{ URL::current() }}" target="_blank"><img src="/assets/images/icons/twitter.png" alt="Twitter" height="40" /></a>
                    <a class="social-icon" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}" target="_blank"><img src="/assets/images/icons/facebook.png" alt="Facebook" height="40" /></a>
                </div>

            </div>

            <hr class="spacer">

            <div class="row links">
                <div class="col-sm-6">
                    @if($blogArticle->previous())
                        <a href="{{ route('blog.show', $blogArticle->previous()->slug) }}">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <strong>Previous Post</strong><br>
                            {{ $blogArticle->previous()->title }}
                        </a>
                    @endif
                </div>

                <div class="col-sm-6 text-right">
                    @if($blogArticle->next())
                        <a href="{{ route('blog.show', $blogArticle->next()->slug) }}">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <strong>Next Post</strong><br>
                            {{ $blogArticle->next()->title }}
                        </a>
                    @endif
                </div>
            </div>

        </div>

    </div>

</div>

@endsection