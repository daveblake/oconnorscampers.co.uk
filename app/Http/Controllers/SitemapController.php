<?php

namespace App\Http\Controllers;


use App\BlogArticle;
use App\Guide;
use App\Page;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SitemapController extends Controller
{

    public function getSitemap()
    {
        // Create new sitemap object
        $sitemap = App::make("sitemap");

        // Check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached())
        {
            // Add item to the sitemap (url, date, priority, freq)
            $sitemap->add(URL::to('/'), null, 1);

            // Vans
            $sitemap->add(URL::to('/our-vw-campervans-for-hire'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/rosie'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/uma'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/ordell'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/mr-orange'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/kissinger'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/jackie-brown'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/battenberg'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/barbarossa'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/nice-guy-eddie'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/shiny-norma'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/max-cherry'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/noddy-windsor'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/barbarella'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/i-am-spartacus'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/wolverine'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/vw-california'), null, 1);
            $sitemap->add(URL::to('/our-vw-campervans-for-hire/t6-vw-california'), null, 1);

            $pages = Page::where('published', 1)->get();
            if (!$pages->isEmpty()) {
                foreach ($pages as $page) {
                    $sitemap->add(route('page', $page->slug), $page->updated_at, 1);
                }
            }

            $blogArticles = BlogArticle::where('published', 1)->get();
            if (!$blogArticles->isEmpty()) {
                foreach ($blogArticles as $blogArticle) {
                    $sitemap->add(route('blog.show', $blogArticle->slug), $blogArticle->updated_at, 1);
                }
            }

            $guides = Guide::where('published', 1)->get();
            if (!$guides->isEmpty()) {
                foreach ($guides as $guide) {
                    $sitemap->add(route('guide.show', $guide->slug), $guide->updated_at, 1);
                }
            }
        }

        // Render sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
    }

}
