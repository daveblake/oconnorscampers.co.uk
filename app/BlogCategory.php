<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model {

	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $fillable = [
		'name',
		'slug',
	];


	public function articles() {
		return $this->hasMany('App\BlogArticle')->where('published', 1)->orderBy('published_at', 'desc');
	}

}