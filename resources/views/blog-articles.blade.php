@extends('_layout')

@section('seo')

    <meta name="description" content="O'Connors Campers blog posts">
    <title>O'Connors Campers blog posts</title>

    <meta name="twitter:title" content="O'Connors Campers blog posts">
    <meta name="twitter:description" content="O'Connors Campers blog posts">
    <meta name="twitter:image" content="">

    <meta property="og:title" content="O'Connors Campers blog posts" />
    <meta property="og:description" content="O'Connors Campers blog posts" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ URL::current() }}" />
    <meta property="og:image" content="" />

@endsection

@section('content')

<div class="container blog">

    <div class="row">

        <div class="col-md-3 blog-sidebar">
            <div class="affix">
                @if(count($blogCategories) > 0)
                    <h4>Categories</h4>
                    <ul>
                        @foreach($blogCategories as $blogCategory)
                            <li><a href="{{ route('blog.category.show', $blogCategory->slug) }}">{{ $blogCategory->name }} ({{ count($blogCategory->articles) }})</a></li>
                        @endforeach
                    </ul>
                @endif

                @if(count($recentEntries) > 0)
                    <h4>Recent Entries</h4>
                    <ul>
                        @foreach($recentEntries as $recentEntry)
                            <li>
                                <a href="{{ route('blog.show', $recentEntry->slug) }}">{{ $recentEntry->title }}</a><br>
                                <small>{{ $recentEntry->published_at->format('l jS F, Y') }}</small>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>

        <div class="col-md-9">
            <h1>Blog / Recent</h1>

            @if(!$blogArticles->isEmpty())
                @foreach($blogArticles as $article)
                    <a href="{{ route('blog.show', $article->slug) }}" class="row blog-summary">
                        <div class="col-md-4">
                            <img class="img-responsive" src="{{ $article->image('blog') }}" alt="{{ ($article->image_alt) ? $article->image_alt : $article->title }}" />
                        </div>
                        <div class="col-md-8">
                            <h2>{{ $article->title }}</h2>
                            <h6>by {{ $article->author }} | {{ $article->published_at->format('l jS F, Y') }}</h6>
                            {!! $article->summary !!}
                        </div>
                    </a>
                @endforeach

                <div class="text-center">{!! $blogArticles->render() !!}</div>
            @else
                <p class="spacer">Sorry, no entries found.</p>
            @endif
        </div>

    </div>

</div>

@endsection