@if ( $van->hasImageFromTag('side-med'))
  <a href="{{ route('van.show', $van->slug) }}"><img class="img-responsive" src="{{$van->imageFromTag('side-med')->fullPath}}" alt="{{ $van->name }}" /></a>
@else
  <a href="{{ route('van.show', $van->slug) }}"><img class="img-responsive" src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_med.jpg" alt="{{ $van->name }}" /></a>
@endif
<h3><a href="{{ route('van.show', $van->slug) }}">{{ $van->name }}</a></h3>
<h6>{{ $van->maxGuests }} Berth &bull; {{ $van->customValue('type') }}</h6>
<p>{{ $van->summary }}</p>

<div class="indent">
  <a href="{{ route('van.show', $van->slug) }}" style="color:#ffffff;background:#{{ $van->customValue('color','000000') }}" class="btn btn-block btn-default">Van Details</a>
</div>