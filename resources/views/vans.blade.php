@extends('_layout')

@section('content')

  <div class="container">

    <h2 class="heading text-center">Our VW Campers</h2>

    <div class="row text-center">

      @if(!empty($vans))
        @foreach($vans as $van)
          <div class="col-md-3 col-sm-4 van-box">
            @include('layout.partials.van')
          </div>
        @endforeach
      @endif

    </div>

    @if(!empty($motorhomes))

      <div class="row text-center">
        @foreach($motorhomes as $van)
          <div class="col-md-6 col-sm-6 van-box motorhome">
            @include('layout.partials.van')
          </div>
        @endforeach
      </div>
    @endif


  </div>

@endsection