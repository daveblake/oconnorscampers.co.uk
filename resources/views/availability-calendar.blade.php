@extends('_layout')

@section('content')

  <section id="search-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <h3>Search Availability</h3>
        </div>
        <div class="col-md-3 text-right">
          <div class="view-switcher">
            <a href="{{ route('availability.vans') }}">Van View</a><a class="active"
                                                                      href="{{ route('availability.calendar') }}">Calendar
                                                                                                                  View</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php
  $groupNames = [
      'MonFri' => ['name' => 'Mid Week', 'nights' => 4],
      'FriMon' => ['name' => 'Weekend', 'nights' => 3],
      'MonMon' => ['name' => 'Week (M-M)', 'nights' => 7],
      'FriFri' => ['name' => 'Week (F-F)', 'nights' => 7],
      'Others' => ['name' => 'Specials', 'nights' => 7],
  ];
  ?>

  <div class="container">

    <table id="pricing-table" class="pricing table hidden-xs hidden-sm">
      <thead>
      <tr>
        <th class="month">
          <form method="get" action="">
            <img height="20" src="/assets/images/icons/calendar.png" alt="Calendar"/>
            <select class="month-selector" name="date">
              <?php
              for($i = $currentMonth; $i < $currentMonth + 25; $i++)
              {
                $optionMonth = mktime(0, 0, 0, $i, 1);
                echo '<option ' . ($selectedDate->format('Y-m') == date(
                        'Y-m',
                        $optionMonth
                    ) ? 'selected="selected"' : '') . 'value="' . date('Y-m-d', $optionMonth) . '">' . date(
                        'F Y',
                        $optionMonth
                    ) . '</option>';
              }
              ?>
            </select>
          </form>
        </th>
        <th class="stay">&nbsp;</th>
        <?php
        $calendarDates = clone $selectedDate;
        $previousFriday = clone $selectedDate; $previousFriday->modify('previous Friday');
        for($i = 1; $i <= 14; $i++)
        {
          echo '<th><small>' . $calendarDates->format('D') . '</small><br>' . $calendarDates->format('j') . '</th>';
          $calendarDates->modify('+1 day');
        }
        ?>
        <th class="arrows">
          <?php
          $prevDate = clone $selectedDate; $prevDate->modify('-14 days');
          $nextDate = clone $selectedDate; $nextDate->modify('+14 days');
          ?>
          <a href="?date={{ $prevDate->format('Y-m-d') }}"><span class="glyphicon glyphicon-chevron-left"></span></a>
          <a href="?date={{ $nextDate->format('Y-m-d') }}"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </th>
      </tr>
      </thead>
      <tbody>
      @if(!empty($vans))
        @foreach($vans as $van)
          <?php
          $vanAvailability = isset($availability[$van->uuid]) ? $availability[$van->uuid]->items : [];
          $groupCount = count($vanAvailability);
          $currentRow = 0;
          ?>
          <?php
          foreach($vanAvailability as $groupKey => $availabilityGroup):
          $currentRow++;
          ?>
          <tr @if($groupCount == $currentRow)class="bottom" @endif data-group="<?=$currentRow; ?>:<?=$groupCount; ?>">
            @if($currentRow==1)
              <th class="van" rowspan="<?= $groupCount; ?>">
                @if ( $van->hasImageFromTag('side-small') )
                  <img width="100" src="{{$van->imageFromTag('side-small')->fullPath}}"
                       alt="{{ $van->name }}"/>
                  @else
                <img width="100" src="/assets/images/vans/{{ $van->slug }}/{{ $van->slug }}_side_small.png"
                     alt="{{ $van->name }}"/>
                @endif
                <h3>{{ $van->name }}</h3>
                <small>{{ $van->maxGuests }} Berth &bull; {{ $van->customValues->type }}</small>
              </th>
            @endif
            <th>{{ isset($groupNames[$groupKey]) ? $groupNames[$groupKey]['name'] : '&nbsp;' }}</th>
            <?php
            /** @var \DateTime $calendarDates */
            $calendarDates = clone $selectedDate;
            reset($availabilityGroup->items);
            $firstDate = new \DateTime(key($availabilityGroup->items));


            $daysToDisplay = 14;

            for ($i = 1; $i <= $daysToDisplay; $i++) {
            $offsetDays = 0; //Used to calculate an offset when a period starts before first calendar day

            if($firstDate !== false && $firstDate < $calendarDates
                && isset($availabilityGroup->items[$firstDate->format('Y-m-d')])
            )
            {

              $data     = $availabilityGroup->items[$firstDate->format('Y-m-d')];
              $interval = $calendarDates->diff($firstDate);

              $offsetDays = $interval->format('%a');
            }
            elseif(isset($availabilityGroup->items[$calendarDates->format('Y-m-d')]))
            {
              $data = $availabilityGroup->items[$calendarDates->format('Y-m-d')];
            }
            else
            {
              $data = false;
            }

            $firstDate = false; //don't want to run this again

            //Handle dates that start before but end within this period
            if($data) {
            $colNights = $data->nights - ($offsetDays);
            $colspan = ($i + $colNights > 14) ? 15 - $i : $colNights;

            ?>
            <td class="available" colspan="{{ $colspan }}">
              <a href="{{ route('booking', $van->slug) }}?date={{ $data->dateFrom }}&nights={{ $data->nights }}">&pound;{{ $data->price }}</a>
            </td>
            <?php
            $calendarDates->modify('+' . $colNights . ' days');
            $i += ($colNights - 1);
            }
            else
            {
            ?>
            <td class="unavailable"></td><?php
            $calendarDates->modify('+1 day');
            }
            }
            ?>
            <td></td>
          </tr>
          <?php endforeach; ?>
        @endforeach
      @endif

      <tr class="ie-sizing-fix">
        <?php for($i = 1; $i <= 17; $i++)
        {
          echo '<td>&nbsp;</td>';
        } ?>
      </tr>

      </tbody>
    </table>

    <div class="text-center hidden-md hidden-lg">

      <form method="get" action="">
        <img height="20" src="/assets/images/icons/calendar.png" alt="Calendar"/>
        <select class="month-selector" name="date">
          <?php
          for($i = $currentMonth; $i < $currentMonth + 25; $i++)
          {
            $optionMonth = mktime(0, 0, 0, $i, 1);
            echo '<option ' . ($selectedDate->format('Y-m') == date(
                    'Y-m',
                    $optionMonth
                ) ? 'selected="selected"' : '') . 'value="' . date('Y-m-d', $optionMonth) . '">' . date(
                    'F Y',
                    $optionMonth
                ) . '</option>';
          }
          ?>
        </select>
      </form>

      <div id="m-van-calendar-slick">

        @foreach($vans as $van)
          <div id="m-van-calendar-container">

            <div class="m-van-calendar-header"><h3>{{ $van->name }}</h3></div>

            <table class="m-pricing table">
              <tbody>
              @foreach($groupNames as $key => $availabilityGroup)
                <tr>
                  <th colspan="2">{{ $availabilityGroup['name'] }}</th>
                </tr>
                @if(isset($availability[$van->uuid]->items[$key]) && !empty($availability[$van->uuid]->items[$key]->items))
                  @foreach($availability[$van->uuid]->items[$key]->items as $data)
                    <tr>
                      <td class="date">{{ (new \DateTime($data->dateFrom))->format('jS F') }}</td>
                      <td class="price"><a
                            href="{{ route('booking', $van->slug) }}?date={{ $data->dateFrom }}&nights=4">&pound;{{ $data->price }}</a>
                      </td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="2">No Availability</td>
                  </tr>
                @endif
              @endforeach
              </tbody>
            </table>

          </div>
        @endforeach

      </div>

    </div>

  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(
        function ()
        {

          $('.month-selector').change(
              function ()
              {
                $(this).closest('form').trigger('submit');
              }
          );

          $('#pricing-table').floatThead(
              {
                top:          67,
                getSizingRow: function ($table)
                              { // this is only called when using IE
                                return $table.find('tbody tr.ie-sizing-fix:visible:first>*');
                              }
              }
          );

          $('#m-van-calendar-slick').slick();

        }
    );
  </script>
@endsection