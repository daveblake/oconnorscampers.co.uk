<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuideSection extends Model {

	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $fillable = [
		'guide_id',
		'heading',
		'subheading',
		'summary',
		'content',
		'image_path',
		'image_alt',
	];

	public function guide() {
		return $this->belongsTo('App\Guide');
	}


	public function image($template = 'original') {
		if ($this->image_path) {
			return '/'.config('imagecache.route').'/'.$template.'/'.$this->image_path;
		}

		return false;
	}

}