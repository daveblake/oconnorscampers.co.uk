@extends('_layout')

@section('content')

<form action="{{ route('account.update') }}" method="post">
<div class="container">

    <h2 class="text-center heading">Your Account</h2>

    <h4>Update Your Details</h4>

    {{ csrf_field() }}

    <div class="row">

        <div class="col-md-2 form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', $customer->title, array('class' => 'form-control', 'maxlength' => '10')) !!}
        </div>

        <div class="col-md-5 form-group">
            {!! Form::label('forename', 'Forename') !!}
            {!! Form::text('forename', $customer->forename, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="col-md-5 form-group">
            {!! Form::label('surname', 'Surname') !!}
            {!! Form::text('surname', $customer->surname, array('class' => 'form-control', 'required')) !!}
        </div>

    </div>

    <div class="row">

        <div class="col-md-4 form-group">
            {!! Form::label('address1', 'Address 1') !!}
            {!! Form::text('address1', $customer->address1, array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-4 form-group">
            {!! Form::label('address2', 'Address 2') !!}
            {!! Form::text('address2', $customer->address2, array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-4 form-group">
            {!! Form::label('town', 'Town') !!}
            {!! Form::text('town', $customer->town, array('class' => 'form-control')) !!}
        </div>

    </div>

    <div class="row">

        <div class="col-md-4 form-group">
            {!! Form::label('county', 'County') !!}
            {!! Form::text('county', $customer->county, array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-4 form-group">
            {!! Form::label('postcode', 'Postcode') !!}
            {!! Form::text('postcode', $customer->postcode, array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-4 form-group">
            {!! Form::label('country', 'Country') !!}
            {!! Form::select('country',
                $countries,
                $customer->country->uuid, ['class' => 'form-control', 'placeholder' => '' ]) !!}
        </div>

    </div>

    <div class="row">

        <div class="col-md-4 form-group">
            {!! Form::label('telHome', 'Telephone (Home)') !!}
            {!! Form::text('telHome', $customer->telHome, array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-4 form-group">
            {!! Form::label('telMob', 'Telephone (Mob)') !!}
            {!! Form::text('telMob', $customer->telMob, array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-4 form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::text('email', $customer->email, array('class' => 'form-control')) !!}
        </div>

    </div>

    @include('layout.partials.errors')

</div>

<div class="container text-center">

    <a href="{{ route('account') }}" class="btn btn-lg btn-default spacer pull-left">Back</a>
    <input type="submit" class="btn btn-lg btn-primary spacer pull-right" value="Update">

</div>
</form>

@endsection